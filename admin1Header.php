<div class="headerline"></div>
<div class="width100 same-padding header-div">
	<div class="logo"><img src="img/gic.png" class="logo-size"></div>
	<div class="right-menu">
    	<div class="dropdown hover1">
        	<a  class="menu-right-margin dropdown-btn">
            	<img src="img/invoice.png" class="gic-menu-icon hover1a" alt="Loan Status" title="Loan Status">
            	<img src="img/invoice2.png" class="gic-menu-icon hover1b" alt="Loan Status" title="Loan Status">
            </a>
            <div class="dropdown-content yellow-dropdown-content">
            	<p class="dropdown-p"><a href="admin1Product.php"  class="dropdown-a">Loan Status Form</a></p>
            	<p class="dropdown-p"><a href="adminAddNewProduct.php"  class="dropdown-a">Add New Loan</a></p>
            </div>
    	</div>
    	<a href="logout.php" class="hover1">
        	<img src="img/logout.png" class="hover1a gic-menu-icon" alt="logout" title="logout">
            <img src="img/logout2.png" class="hover1b gic-menu-icon" alt="logout" title="logout">
        </a>
    </div>
</div>
<div class="clear"></div>