<?php
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
// require_once dirname(__FILE__) . '/classes/ProductOrders.php';
// require_once dirname(__FILE__) . '/classes/SignUpProduct.php';
require_once dirname(__FILE__) . '/classes/User.php';
// require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

//for count
// $userSignUpProduct = getSignUpProduct($conn);
$memberAmount = getUser($conn," WHERE user_type = ? ",array("user_type"),array(1),"i");
$products = getProduct($conn);

// $shippingRequest = getOrders($conn," WHERE shipping_status = ? ",array("shipping_status"),array('PENDING'),"s");
// $withdrawalRequest = getWithdrawReq($conn," WHERE withdrawal_status = ? ",array("withdrawal_status"),array('PENDING'),"s");
// $withdrawalAmount = getWithdrawReq($conn," WHERE withdrawal_amount = ? AND withdrawal_status = APPROVED ",array("withdrawal_amount"),array('APPROVED'),"s");

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),"s");
$userDetails = $userRows[0];

// $currentOrders = getOrders($conn," WHERE shipping_status = 'SHIPPED' ",array("shipping_status"),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminDashboard.php" />
    <meta property="og:title" content="Admin Dashboard | DCK Supreme" />
    <title>Admin Dashboard | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminDashboard.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

 <!-- <h1 class="h1-title h1-before-border">Dashboard</h1> -->
    <h1 class="h1-title h1-before-border"><?php echo _MAINJS_ADMDASH_DASHBOARD ?></h1>
    <div class="border-top100 four-div-container admin-dash">

        <?php
        if($userSignUpProduct)
        {
            $aaaa = count($userSignUpProduct);
        }
        else
        {   $aaaa = 0;  }
        ?>

        <?php   $revenue = $aaaa * 300  ?>

        <?php
        if($memberAmount)
        {
            $totalMember = count($memberAmount);
        }
        else
        {   $totalMember = 0;   }
        ?>

        <?php
        if($products)
        {
            $totalProduct = count($products);
        }
        else
        {   $totalProduct = 0;   }
        ?>

        <?php
        if($shippingRequest)
        {
            $totalShippingRequest = count($shippingRequest);
        }
        else
        {   $totalShippingRequest = 0;   }
        ?>

        <?php
        if($withdrawalRequest)
        {
            $totalWithdrawalRequest = count($withdrawalRequest);
        }
        else
        {   $totalWithdrawalRequest = 0;   }
        ?>



    	<a href="#" class="black-text">
            <div class="four-white-div hover1 white-div-yellow">
                <img src="img/sales.png" class="four-img hover1a" alt="Products" title="Products">
                <img src="img/sales2.png" class="four-img hover1b" alt="Products" title="Products">

                <p class="four-div-p four-div-p1"><b><?php echo _MAINJS_ADMDASH_NO_OF_SALES ?> </b></p>
                <!-- Just call out the number will do, please remove all the html table etc-->
                <!-- <p class="four-div-p four-div-p2"><b>10</b></p> -->
                <p class="four-div-p four-div-p2"><b><?php echo $aaaa;?></b></p>
            </div>
        </a>
        <!-- <a href="adminShpping.php" class="black-text"> -->
        <a href="#" class="black-text">
            <div class="four-white-div hover1 four-middle-div1 white-div-yellow">
                <img src="img/revenue1.png" class="four-img hover1a" alt="Shipping" title="Shipping">
                <img src="img/revenue2.png" class="four-img hover1b" alt="Shipping" title="Shipping">

                <p class="four-div-p four-div-p1"><b><?php echo _MAINJS_ADMDASH_REVENUE ?></b></p>
                <!-- <p class="four-div-p four-div-p2"><b>10</b></p> -->
                <p class="four-div-p four-div-p2"><b>RM <?php echo $revenue;?></b></p>
            </div>
        </a>
        <a href="adminMember.php" class="black-text">
            <div class="four-white-div hover1 four-middle-div2 white-div-yellow">
                <img src="img/member1.png" class="four-img hover1a" alt="New Joined Members" title="New Joined Members">
                <img src="img/member2.png" class="four-img hover1b" alt="New Joined Members" title="New Joined Members">

                <p class="four-div-p four-div-p1"><b><?php echo _MAINJS_ADMDASH_TOTAL_MEMBER ?></b></p>
                <!-- <p class="four-div-p four-div-p2"><b>10</b></p> -->
                <p class="four-div-p four-div-p2"><b><?php echo $totalMember;?></b></p>
            </div>
        </a>
        <a href="adminSalesProduct.php" class="black-text">
            <div class="four-white-div hover1 white-div-yellow">
                <img src="img/product1.png" class="four-img hover1a" alt="Products" title="Products">
                <img src="img/product2.png" class="four-img hover1b" alt="Products" title="Products">

                <p class="four-div-p four-div-p1"><b><?php echo _MAINJS_ADMDASH_TOTAL_PRODUCT ?></b></p>
                <!-- <p class="four-div-p four-div-p2"><b>10</b></p> -->
                <p class="four-div-p four-div-p2"><b><?php echo $totalProduct;?></b></p>
            </div>
        </a>
    </div>
    <div class="clear"></div>
    <div class="four-div-container extra-margin-top admin-dash">
    	<a href="#" class="black-text">
            <div class="four-white-div hover1 white-div-yellow">
                <img src="img/payout1.png" class="four-img hover1a" alt="Products" title="Products">
                <img src="img/payout2.png" class="four-img hover1b" alt="Products" title="Products">

                <p class="four-div-p four-div-p1"><b><?php echo _MAINJS_ADMDASH_PAYOUT ?></b></p>
                <p class="four-div-p four-div-p2"><b>10</b></p>
            </div>
        </a>
        <!-- <a href="adminShpping.php" class="black-text"> -->
        <a href="adminWithdrawalComp.php" class="black-text">
            <div class="four-white-div hover1 four-middle-div1 white-div-yellow">
                <img src="img/withdrawal1.png" class="four-img hover1a" alt="Shipping" title="Shipping">
                <img src="img/withdrawal2.png" class="four-img hover1b" alt="Shipping" title="Shipping">

                <p class="four-div-p four-div-p1"><b><?php echo _MAINJS_ADMDASH_WITHDRAWAL ?></b></p>
                <!-- <p class="four-div-p four-div-p2"><b>10</b></p> -->
                <p class="four-div-p four-div-p2"><b><?php echo $totalQuantity;?></b></p>
            </div>
        </a>
        <a href="adminWithdrawal.php" class="black-text">
            <div class="four-white-div hover1 four-middle-div2 white-div-yellow">
                <img src="img/withdraw1.png" class="four-img hover1a" alt="Withdrawal" title="Withdrawal">
                <img src="img/withdraw2.png" class="four-img hover1b" alt="Withdrawal" title="Withdrawal">

                <p class="four-div-p four-div-p1"><b><?php echo _MAINJS_ADMDASH_WITHDRAWAL_REQUEST ?></b></p>
                <!-- <p class="four-div-p four-div-p2"><b>10</b></p> -->
                <p class="four-div-p four-div-p2"><b><?php echo $totalWithdrawalRequest;?></b></p>
            </div>
        </a>
        <a href="adminShipping.php" class="black-text">
            <div class="four-white-div hover1 white-div-yellow">
                <img src="img/shipping1.png" class="four-img hover1a" alt="Shipping" title="Shipping">
                <img src="img/shipping2.png" class="four-img hover1b" alt="Shipping" title="Shipping">

                <p class="four-div-p four-div-p1"><b><?php echo _MAINJS_ADMDASH_SHIPPING_REQUEST ?></b></p>
                <!-- <p class="four-div-p four-div-p2"><b>10</b></p> -->
                <p class="four-div-p four-div-p2"><b><?php echo $totalShippingRequest;?></b></p>
            </div>
        </a>
    </div>

    <div class="clear"></div>

</div>

<?php
function totalQuantity()
{
    $conn = connDB();
    // $result1 = mysqli_query($conn,"SELECT sum(buy_stock) AS productno1 FROM `product`");
    $result1 = mysqli_query($conn,"SELECT sum(withdrawal_amount) AS productno1 FROM `withdrawal`");

    if (mysqli_num_rows($result1) > 0)
    {
    ?>
        <?php
        $i=0;
        while($row = mysqli_fetch_array($result1))
        {
        ?>

        <?php echo $row["productno1"]; ?>
        <?php
            $i++;
        }
        ?>

    <?php
    }
    else
    {   echo "No result found"; }
}
?>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
