<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$loanUidRows = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
$invoiceDetails = getInvoice($conn);
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Loan Status Invoice</h1>
    <div class="short-red-border"></div>
    <!-- This is a filter for the table result -->


    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->

    <!-- End of Filter -->
    <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th class="th">NO.</th>
                        <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("PURCHASER NAME",10,"</br>\n");?></th>
                        <!-- <th>STOCK</th> -->
                        <th class="th">IC</th>
                        <th class="th">CONTACT</th>
                        <th class="th">E-MAIL</th>
                        <th class="th"><?php echo wordwrap("BOOKING DATE",10,"</br>\n");?></th>
                        <th class="th">SQ FT</th>
                        <th class="th"><?php echo wordwrap("SPA PRICE",8,"</br>\n");?></th>
                        <th class="th">PACKAGE</th>
                        <th class="th">DISCOUNT</th>
                        <th class="th">REBATE</th>
                        <th class="th"><?php echo wordwrap("EXTRA REBATE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("NETT PRICE",8,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("TOTAL DEVELOPER COMMISSION",10,"</br>\n");?></th>
                        <th class="th">AGENT</th>
                        <th class="th"><?php echo wordwrap("LOAN STATUS",10,"</br>\n");?></th>

                        <th class="th">REMARK</th>
                        <th class="th"><?php echo wordwrap("FORM COLLECTED",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("PAYMENT METHOD",10,"</br>\n");?></th>
                        <th class="th">LAWYER</th>
                        <th class="th"><?php echo wordwrap("PENDING APPROVAL STATUS",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("BANK APPROVED",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("LO SIGNED DATE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("LA SIGNED DATE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("SPA SIGNED DATE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("FULLSET COMPLETED",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("CANCELLED BOOKING",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("CASE STATUS",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("EVENT PERSONAL",10,"</br>\n");?></th>
                        <th class="th">RATE</th>
                        <th class="th"><?php echo wordwrap("AGENT COMMISSION",10,"</br>\n");?></th>
                        <th class="th">UPLINE</th>
                        <th class="th">UP-UPLINE</th>
                        <th class="th"><?php echo wordwrap("PL NAME",5,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("HOS NAME",7,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("LISTER NAME",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UL OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UUL OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("PL OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("HOS OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("LISTER OVERRIDE",10,"</br>\n");?></th>

                        <th class="th"><?php echo wordwrap("ADMIN1 OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("ADMIN2 OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("GIC PROFIT",8,"</br>\n");?></th>
                          <th class="th"><?php echo wordwrap("TOTAL CLAIMED DEV AMT",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("TOTAL BALANCED DEV AMT",10,"</br>\n");?></th>
                        <!-- <th class="th">BANK APPROVED</th>
                        <th class="th">LO SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th> -->

                        <th>ACTION</th>
                        <!-- <th>INVOICE</th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                    // {
                        $orderDetails = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
                        if($orderDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                            {?>
                            <tr>
                                <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                <td class="td"><?php echo $orderDetails[$cntAA]->getId();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php echo wordwrap($orderDetails[$cntAA]->getPurchaserName(),15,"</br>\n");?></td>
                                <!-- <td><?php //echo $orderDetails[$cntAA]->getStock();?></td> -->
                                <td class="td"><?php echo $orderDetails[$cntAA]->getIc();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getContact();?></td>

                                <td class="td"><?php echo $orderDetails[$cntAA]->getEmail();?></td>
                                <td class="td"><?php echo date('d-m-Y', strtotime($orderDetails[$cntAA]->getBookingDate()));?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getSqFt();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getSpaPrice();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getPackage();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getRebate();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getDiscount();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getExtraRebate();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getNettPrice();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getTotalDeveloperComm();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getAgent();?></td>
                                <td class="td"><?php echo wordwrap($orderDetails[$cntAA]->getLoanStatus(),50,"</br>\n");?></td>

                                <td class="td"><?php echo $orderDetails[$cntAA]->getRemark();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getBFormCollected();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getPaymentMethod();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getLawyer();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getPendingApprovalStatus();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getBankApproved();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getLoSignedDate();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getLaSignedDate();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getSpaSignedDate();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getFullsetCompleted();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getCancelledBooking();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getCaseStatus();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getEventPersonal();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getRate();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getAgentComm();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUpline1();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUpline2();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getPlName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getHosName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getListerName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUlOverride();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUulOverride();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getPlOverride();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getHosOverride();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getListerOverride();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getAdmin1Override();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getAdmin2Override();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getGicProfit();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getTotalClaimDevAmt();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getTotalBalUnclaimAmt();?></td>


                                <td class="td">
                                    <form action="editInvoice.php" method="POST">
                                        <button class="clean edit-anc-btn hover1" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">
                                            <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product">
                                            <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product">
                                        </button>
                                    </form>
                                </td>
                                
                            </tr>
                            <?php
                            }
                        }
                    //}
                    ?>
                </tbody>
            </table><br>


    </div>

    <!-- <div class="three-btn-container">
    <!-- <a href="adminRestoreProduct.php" class="add-a"><button name="restore_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side two-button-side1">Restore</button></a> -->
      <!-- <a href="adminAddNewProduct.php" class="add-a"><button name="add_new_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side  two-button-side2">Add</button></a> -->
    <!-- </div> -->
    <?php $conn->close();?>

</div>




<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
</body>
</html>
