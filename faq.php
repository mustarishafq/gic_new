<?php
if (session_id() == ""){
    session_start();
}

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';


function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://dcksupreme.asia/faq.php" />
<meta property="og:title" content="FAQ | DCK Supreme" />
<title>FAQ | DCK Supreme</title>
<meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="keywords" content="DCK, DCK Supreme,supreme, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
noisiness and temperature, dry cold start,etc">
<link rel="canonical" href="https://dcksupreme.asia/faq.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">
<!--
<div id="overlay">
 <div class="center-food"><img src="img/loading-gif.gif" class="food-gif"></div>
 <div id="progstat"></div>
 <div id="progress"></div>
</div>-->

    <!-- Start Menu -->
    <?php include 'header-sherry.php'; ?>
<div class="faq-div width100 same-padding text-center">
	<img src="img/faq.png" class="faq-img"  data-animate-scroll='{"x": "0","y": "-100", "alpha": "0", "duration": "0.5"}'>
	<h1 class="title-h1"  data-animate-scroll='{"x": "0","y": "-100", "alpha": "0", "duration": "0.9"}'>Frequently Asked Questions</h1>
    <p class="border-p"  data-animate-scroll='{"x": "0","y": "-100", "alpha": "0", "duration": "1.3"}'></p>
    <h1 class="big-h1"  data-animate-scroll='{"x": "0","y": "-100", "alpha": "0", "duration": "1.8"}'>DCK Engine Oil Booster</h1>
    <div class="faq-container"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "2.5"}'>
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Suitable for all types of engine oil?" title="Suitable for all types of engine oil?">
            </div>
            <div class="middle-faq-div">
                <b>Is DCK Engine Oil Booster suitable for all types of engine oil? Can I apply a higher dosage?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer1()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer1" style="display:none;"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2.5"}'>
            <p class="answer-p">Yes, it is suitable for ALL types of gasoline and diesel engine oil. Below is the suggested dosages for the respective engine oils, a higher dosage is recommended for engines with high engine oil consumption.</p>
            <table class="answer1-table">
                <thead>
                    <tr>
                        <th>Grade of Engine Oil</th>
                        <th>Recommended Dosage</th>
                    </tr>
                </thead>
                <tr>
                    <td>
                        0W20
                    </td>
                    <td>
                        3% - 3.3%
                    </td>                
                </tr>
                <tr>
                    <td>
                        0W30/40，5W30/40，10W30/40，15W40/50，20W50，25W50
                    </td>
                    <td>
                        3% - 6%
                    </td>                
                </tr>
                <tr>
                    <td>
                        0W50/ 60
                    </td>
                    <td>
                        3% - 8%
                    </td>                
                </tr>                        
            </table>
        </div>
    </div>
    <!-- Q2 -->
    <div class="faq-container"   data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="What is the mixing ratio?" title="What is the mixing ratio?">
            </div>
            <div class="middle-faq-div">
                <b>Could DCK  Engine Oil Booster be added onto Transmission Fluid and Hydraulic Fluid? What is the mixing ratio?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer2()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer2" style="display:none;">
            <div class="answer2 answer-div-css">
                <p class="answer-p">It can be added on both Transmission and Hydraulic Fluid:</p>
                <p class="answer-p"><b>Transmission Fluid</b> – 3% is recommended for all types of manual transmission fluid and up to 5- speed auto transmission fluid. It is not suitable for all types of CVT transmission fluid.</p>
                <p class="answer-p"><b>Hydraulic Fluid</b> – 3% is recommended whilst 4% may be applicable on more demanding situations.</p>
            </div>
        </div>
    </div>
    <!-- Q3 -->
    <div class="faq-container"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Any negative effects?" title="Any negative effects?">
            </div>
            <div class="middle-faq-div">
                <b>After using <b>DCK </b> Engine Oil Booster, will discontinuation of the product usage bring any negative effects?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer3()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer3" style="display:none;">
            <div class="answer3 answer-div-css">
                <p class="answer-p">Definitely no negative effects, the engine performance will restore to the state before using <b>DCK </b> Engine Oil Booster.</p>
            </div>
        </div>
    </div>    

    <!-- Q4 -->
    <div class="faq-container"   data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="I am currently using other brand" title="I am currently using other brand">
            </div>
            <div class="middle-faq-div">
                <b>I am currently using other brands of engine oil additive and is planning to convert to <b>DCK</b> Engine Oil Booster, what should I do?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer4()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer4" style="display:none;">
            <div class="answer3 answer-div-css">
                <p class="answer-p">It is recommended to use <b>DCK</b> Engine Oil Booster during oil change due to uncertainty of the quality and types of additives used.</p>
            </div>
        </div>
    </div> 

    <!-- Q5 -->
    <div class="faq-container"  data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="I am currently using other brand" title="I am currently using other brand">
            </div>
            <div class="middle-faq-div">
                <b>Currently I’m not using any engine oil additives and the engine oil have been in use for quite some time since the last oil change, could I add <b>DCK</b> Engine Oil Booster immediately?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer5()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer5" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">It is recommended to add <b>DCK</b> Engine Oil Booster for immediate protection.</p>
            </div>
        </div>
    </div> 

    <!-- Q6 -->
    <div class="faq-container"   data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Why do I need to add on DCK Engine Oil Booster?" title="Why do I need to add on DCK Engine Oil Booster?">
            </div>
            <div class="middle-faq-div">
                <b>Currently I am using the best imported quality brand of fully synthetic engine oil. Do I still need to use <b>DCK</b> Engine Oil Booster?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer6()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer6" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p"><b>DCK</b> Engine Oil Booster’s most innovative Xagtec technology and formula is designed for the latest engine and engine oil technology. Xagtec polarized technology ensures the maximum reduction of friction between metal to metal contact during extreme working conditions and weather climate.</p>
            	<p class="answer-p"><b>DCK</b> Engine Oil Booster provides boundary protective coating on metal surfaces to reduce friction on metal to metal contact and therefore reduce wear and tear, especially during “Dry Cold Starts”.</p>
            	<p class="answer-p">After the engine stops, the engine oil will naturally drain down to the bottom of the engine oil pan which will leave the engine parts dry. When the engine is restarts, friction between the unlubricated parts occur before the oil has a chance to circulate to lubricate the engine parts.</p>
            	<p class="answer-p"><b>DCK</b> Engine Oil Booster’s polarized technology will form a thin layer of protective film covering all metal parts provides boundary protection for Excellent ‘Dry Cold Starts’, particularly 60% of engine wear and tear may occur at this time.</p>
            	<p class="answer-p">In addition, <b>DCK</b> Engine Oil Booster’s innovative polarized technology will ensure more engine oil booster to be attracted to positively charged metal surfaces when extreme friction occurs as friction produces static electric. Metal to metal contact will be reduced.</p>
            </div>
        </div>
    </div>    
 
 
    <!-- Q7 -->
    <div class="faq-container"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="What is the meaning of Restore Wear and Tear Parts?" title="What is the meaning of Restore Wear and Tear Parts?">
            </div>
            <div class="middle-faq-div">
                <b>What is the meaning of Restore Wear and Tear Parts?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer7()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer7" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">There are many tiny holes and uneven surfaces on the metal parts which could not be seen with the naked eyes unless it is obviously worn. <b>DCK</b> Engine Oil Booster’s unique Xagtec technology will fill up these tiny holes and smoothen the uneven surfaces, and provides boundary protective coating on metal surfaces to reduce friction on metal to metal contact preventing further damages.</p>
            	<p class="answer-p">Engine Oil Booster’s unique Xagtec technology will restores wear & tear parts (peak and valley) ,restoring the lost power and extending the engine life span.</p>
            </div>
        </div>
    </div>
 
    <!-- Q8 -->
    <div class="faq-container"   data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Could the oil change interval have extended to 20,000km?" title="Could the oil change interval have extended to 20,000km?">
            </div>
            <div class="middle-faq-div">
                <b>I am currently using Fully Synthetic Engine Oil, the label indicates that the oil change interval is 10,000 km, could the oil change interval have extended to 20,000km after adding <b>DCK</b> Engine Oil Booster?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer8()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer8" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">The indicated oil change interval is based on ideal driving conditions;</p>
            	<p class="answer-p">For example:</p>
                <p class="answer-p">a. Adequate engine horsepower, well-maintained engine, and no overloading.</p>
                <p class="answer-p">b. Optimum weather condition.</p>
                <p class="answer-p">c. Optimum and constant speed on a non-congested highway.</p>
                <p class="answer-p">d. Using high quality fuel that produces less carbon and ashes. Excessive carbon and ashes produced in the engine combustion chamber will shorten engine oil life span.</p>
            	<p class="answer-p">The extended oil change intervals should be based on actual oil change intervals. For example: 8,000km, oil change intervals extended by 100%, which means 16,000km, however, oil filter is recommended to be replaced at every 10,000km.</p>
            </div>
        </div>
    </div> 
 
    <!-- Q9 -->
    <div class="faq-container"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Is there any method to verify the engine oil’s quality?" title="Is there any method to verify the engine oil’s quality?">
            </div>
            <div class="middle-faq-div">
                <b>Is there any method to verify the engine oil’s quality after oil change intervals extended for 50% or 100% after using <b>DCK</b> Engine Oil Booster?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer9()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer9" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">Could be verified based on the following method (Assume that the normal driving mileage is 5000km):</p>
            	<p class="answer-p">a. Identified 4 vehicles (car or truck) which is using the same brand and model of engine oil, add <b>DCK</b> Engine Oil Booster into two of them.</p>
                <p class="answer-p">b. After driving for 5,000km, extract 100ml* of engine oil from each vehicle (total 4) and place them into 4 separate bottles, send for lab test immediately.</p>
                <p class="answer-p">c. After driving for 7,500km, extract 100ml* engine oil (two which added the booster), place them into two separate bottles, send for lab test immediately.</p>
                <p class="answer-p">d. After driving for 10,000km, extract motor oil (two which added the booster), place them into two separate bottles, send for lab test immediately.</p>
            </div>
        </div>
    </div>     
 
    <!-- Q10 -->
    <div class="faq-container"   data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Prolongs engine lifespan" title="Prolongs engine lifespan">
            </div>
            <div class="middle-faq-div">
                <b>Engine Oil Booster – How much benefits could be generated by “Prolongs engine lifespan”?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer10()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer10" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">Assuming: Each truck having a life expectancy of around 1,000,000 km, the average travel time for each truck is 12 hr/day, 60km/h, 24 days per month (taking into account of breakdown and maintenance time), the service lifespan would be around 5 years.</p>
            	<p class="answer-p">Each 50-ton truck cost USD100,000, depreciated of USD20,000 per year.</p>
                <p class="answer-p">If the service lifespan is extended by 50%, which is 2.5 years, the usage value is increased by USD50,000.</p>
            	<p class="answer-p">1000 trucks is equivalent to USD50 million, which means that USD50 million could be saved in 5 years time, average USD10 million a year.</p>
            	<p class="answer-p">2nd benefit – An increment of service lifespan of 2.5 years, it would defer the capital expenditure of USD100 million for replacement of 1000 trucks by 2.5 years.</p>
            	<p class="answer-p">Company’s profit and cash flow will be improved significantly, business expansion could be accelerated, and Government’s tax revenue would also increase accordingly.</p>
            </div>
        </div>
    </div>      

 
    <!-- Q11 -->
    <div class="faq-container"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Saves fuel 3% to 8%" title="Saves fuel 3% to 8%">
            </div>
            <div class="middle-faq-div">
                <b>Engine Oil Booster – “Saves fuel 3% to 8%”, how much in monetary value?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer11()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer11" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">Assumption: A 50-ton truck with 30 liter engine oil capacity, requires 900ml (3%) of engine oil booster.</p>
            	<p class="answer-p">After enhancing with <b>DCK</b> Engine Oil Booster, oil change interval extended to 10,000km (usual is 5,000 km), for every 100 km consumes 40 liters of diesel, 10,000 km consumes 4,000 liters of diesel, the price of diesel is USD0.55/liter = USD2,200.</p>
            	<p class="answer-p">Fuel savings of 3% (Most Conservative Estimation)= USD66.</p>
            </div>
        </div>
    </div>      
 
    <!-- Q12 -->
    <div class="faq-container"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Improves mechanical and lifting speed (8% to 15%)" title="Improves mechanical and lifting speed (8% to 15%)">
            </div>
            <div class="middle-faq-div">
                <b>Engine Oil Booster – "Improves mechanical and lifting speed (8% to 15%)" will generate how much benefits?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer12()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer12" style="display:none;"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2.5"}'>
            <div class="answer-div-css">
                <p class="answer-p">Applying both Engine Oil Booster and Fuel Booster simultaneously, mechanical and lifting speed could increase by a minimum of 10%. We have used the following machinery to Illustrate:</p>
            	<p class="answer-p"><b>a. Forklift</b></p>
                <p class="answer-p">Forklift moves very slowly due to its heavy body weight. Assuming a driver is paid USD25 per shift daily (Incl. fringe benefits), 3 shifts per day,  all 3 of each forklift's drivers’ wages would be USD75 daily.  If the speed increases by 10%, it will translate into the savings of USD7.5 per day in wages, USD225/month per forklift.</p>
            	<p class="answer-p">20 forklifts equal to saving USD4,500/month, USD54,000/year.</p>
                <p class="answer-p">If the efficiency increases by 10%, which also means the quantity of the forklift could be reduced by 10%, if the cost of the forklift is USD25,000, it would be equivalent to USD50,000 (2 forklifts).</p>
            	<p class="answer-p"><b>b. Fishing Boats</b></p>
                <p class="answer-p">Likewise, if speed increases by 10%, the operation time would be reduced by 10% too. Assuming per operation requires 20 days, speed increases by 10%, only 18 days is required, fishing boats will be able to make 10% more fishing trips.</p>
            	<p class="answer-p">Another main benefit is the trawl range would be increased by 10% too. For instance, before enhancing with DCK Engine Oil Booster and Fuel Booster, trawling speed is 10 nautical miles an hour; after enhancing with DCK Engine Oil Booster and Fuel Booster, trawling speed increases to 11 nautical miles an hour, which means the trawl range increases by 10%, theoretically the catches is 10% more than before.</p>
            	<p class="answer-p"><b>c. Lorry and Bus</b></p>
                <p class="answer-p">Lorry and busses have insufficient power especially driving on hilly road. <b>DCK</b> Engine Oil Booster and Fuel Booster will boost the engine power to reduce travelling time.</p>
            </div>
        </div>
    </div> 
    
 
    <!-- Q13 -->
    <div class="faq-container"   data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="How much can I save?" title="How much can I save?">
            </div>
            <div class="middle-faq-div">
                <b>Engine Oil Booster – How much can I save if the oil change interval extended to 100%?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer13()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer13" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">Assuming: Each truck requires 4 oil change a month, and each oil change consumes 30 L engine oil with cost of USD100.</p>
            	<p class="answer-p">After applying <b>DCK</b> Engine Oil Booster, oil change reduced from 4 times to 2 times a month. Saving of 2 oil change, equivalent to saving of USD200 a month.</p>
            	<p class="answer-p">Assuming each oil change requires half a day, saving of 1 day wages per month (half a day X 2times), says, a day wage is USD25.</p>
            	<p class="answer-p">Assuming each truck could generate net income of USD75 a day, company’s profit would be increased by USD900 per annum per truck.</p>
            	<p class="answer-p">100 trucks would contribute USD90,000 to the bottom line.</p>
            </div>
        </div>
    </div>     
    
    <h1 class="big-h1"  data-animate-scroll='{"x": "0","y": "-100", "alpha": "0", "duration": "2.5"}'>DCK Fuel Booster</h1>    
	<h3  data-animate-scroll='{"x": "0","y": "-100", "alpha": "0", "duration": "2.5"}'>(Suitable for all types of petrol, diesel and marine fuel)</h3> 
 
    <!-- Q14 -->
    <div class="faq-container"   data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Would it damage my engine?" title="Would it damage my engine?">
            </div>
            <div class="middle-faq-div">
                <b>Would <b>DCK</b> Fuel Booster damage my engine?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer14()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer14" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">No, <b>DCK</b> Fuel Booster is not only non-corrosive, but is also non-irritative to human skin. Instead, <b>DCK</b> Fuel Booster contains unique lubricating agent which protects engine parts.</p>
            </div>
        </div>
    </div> 

 
    <!-- Q15 -->
    <div class="faq-container"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="How to apply?" title="How to apply?">
            </div>
            <div class="middle-faq-div">
                <b>How to apply DCK Fuel Booster?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer15()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer15" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">The mixing ratio is as follows:</p>
                <p class="answer-p">1ml: 11 liters of petrol</p>
                <p class="answer-p">1ml: 9 liters of diesel (Euro II – Euro III)</p>
                <p class="answer-p">1ml: 10 liters of diesel (Euro IV)</p>
                <p class="answer-p">1ml: 15 liters of diesel (Euro V – Euro VI)</p>
                <p class="answer-p">First application: Additional 50% dosage on petrol engine</p>
                <p class="answer-p">First application: Additional 20% dosage on diesel engine</p>
            </div>
        </div>
    </div> 

 
    <!-- Q16 -->
    <div class="faq-container"   data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="How much fuel can be saved?" title="How much fuel can be saved?">
            </div>
            <div class="middle-faq-div">
                <b>How long does it take to show effect after applying the fuel booster? How much fuel can be saved?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer16()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer16" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">In normal scenarios, the effect could be seen on the first tank of fuel.</p>
                <table class="answer1-table">
                    <thead>
                        <tr>
                        	<th></th>
                            <th></th>
                            <th>Urban Driving</th>
                            <th>Highway Driving</th>
                        </tr>
                    </thead>
                    <tr>
                    	<td>
                        	Fuel savings
                        </td>
                        <td>
                        	Fuel engine
                        </td>	
                        <td>
                            10% - 15%
                        </td>
                        <td>
                            15% - 25%
                        </td>                
                    </tr>
                    <tr>
                    	<td>
                        
                        </td>
                        <td>
                        	Diesel engine
                        </td>	
                        <td>
                            10% - 20%
                        </td>
                        <td>
                            20% - 30%
                        </td>                 
                    </tr>                        
                </table>                
            </div>
        </div>
    </div> 
 
    <!-- Q17 -->
    <div class="faq-container"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="What should I do if there is no improvement?" title="What should I do if there is no improvement?">
            </div>
            <div class="middle-faq-div">
                <b>What should I do if there is no improvement after applying the product?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer17()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer17" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">1. Make sure that the mixing ratio is accurate or adjust the mixing ratio. If the engine power is very powerful, reduce <b>DCK</b> Fuel Booster dosage. If the engine has no significant improvement in power increase dosage.</p>
            	<p class="answer-p">2. Clean or replace the air filter.</p>
                <p class="answer-p">3. Replace the oil filter.</p>
                <p class="answer-p">4. Clean or replace the spark plug.</p>
            </div>
        </div>
    </div> 

 
    <!-- Q18 -->
    <div class="faq-container"   data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="How long does it takes for the carbon deposits and black smoke to be cleaned?" title="How long does it takes for the carbon deposits and black smoke to be cleaned?">
            </div>
            <div class="middle-faq-div">
                <b>How long does it take for the carbon deposits and black smoke to be cleaned after applying the product?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer18()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer18" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">If <b>DCK</b> Fuel Booster is used continuously, most of the carbon deposits could be emitted after driving around 3,000km for petrol engine and 5,000km for diesel engine, black smoke would be eliminated after driving for 60 minutes.</p>
            </div>
        </div>
    </div> 

 
    <!-- Q19 -->
    <div class="faq-container"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Is cleansing the injector and the whole fuel system generating great benefits?" title="Is cleansing the injector and the whole fuel system generating great benefits?">
            </div>
            <div class="middle-faq-div">
                <b>Does cleansing the injector and the whole fuel system generate great benefits?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer19()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer19" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">The fuel would be atomized completely for better combustion, thus, better fuel efficiency and more power could be expected.</p>
            	<p class="answer-p">In addition, machinery breakdown and maintenance cost of injector would be reduced tremendously.</p>
            </div>
        </div>
    </div> 
 
    <!-- Q20 -->
    <div class="faq-container"   data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Why DCK Fuel Booster?" title="Why DCK Fuel Booster?">
            </div>
            <div class="middle-faq-div">
                <b>I have been using premium grade gasoline and diesel, is there any reason to use <b>DCK</b> Fuel Booster besides fuel savings?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer20()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer20" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">Fuel often contains ash, gum, and other deposits. By using <b>DCK</b> Fuel Booster, these impurities could be cleansed and removed, optimizing engine performance at all times.</p>
            </div>
        </div>
    </div>

 
    <!-- Q21 -->
    <div class="faq-container"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Could DCK Fuel Booster be used during winter?" title="Could DCK Fuel Booster be used during winter?">
            </div>
            <div class="middle-faq-div">
                <b>Could <b>DCK</b> Fuel Booster be used during winter?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer21()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer21" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p"><b>DCK</b> Fuel Booster’s low pour point of -24°C is suitable for winter and it helps to “Cold Start” and prevent engine stalling during cold weather </p>
            </div>
        </div>
    </div>
 
    <!-- Q22 -->
    <div class="faq-container"   data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Could DCK Fuel Booster be applied on turbo engine?" title="Could DCK Fuel Booster be applied on turbo engine?">
            </div>
            <div class="middle-faq-div">
                <b>Could <b>DCK</b> Fuel Booster be applied on turbo engine?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer22()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer22" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">It is ideal to add <b>DCK</b> Fuel Booster onto turbo engine by increasing the dosage by 50%, ie. 1ml fuel booster for every 6 – 7 L gasoline. Besides improved fuel efficiency, it would drastically reduce turbo lag problem.</p>
            </div>
        </div>
    </div>
 
    <!-- Q23 -->
    <div class="faq-container"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Would overdose DCK Fuel Booster cause any damages?" title="Would overdose DCK Fuel Booster cause any damages?">
            </div>
            <div class="middle-faq-div">
                <b>Would overdose of <b>DCK</b> Fuel Booster cause any damage?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer23()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer23" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">Overdose would not damage the engine, power will increase significantly, but fuel efficiency may not be achieved. Overdose would make the fuel much easier to combust, natural air intake would not be sufficient to combust the fuel thoroughly, unburned fuel would be emitted and wasted. Air induction turbo engine would be more suitable for over dosage.</p>
            </div>
        </div>
    </div>
 
    <!-- Q24 -->
    <div class="faq-container"   data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Does applying DCK Fuel Booster have any benefits to engine oil?" title="Does applying DCK Fuel Booster have any benefits to engine oil?">
            </div>
            <div class="middle-faq-div">
                <b>Does applying <b>DCK</b> Fuel Booster have any benefits to engine oil?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer24()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer24" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">By applying <b>DCK</b> Fuel Booster, the combustion chamber could be maintained at a high level of cleanliness, minimizing ashes and carbon deposits falling from the combustion chamber into the engine oil pan, which would pollute and shorten the lifespan of the engine oil.</p>
            </div>
        </div>
    </div>

 
    <!-- Q25 -->
    <div class="faq-container"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2.5"}'>    
        <div class="faq-grey-div">
            <div class="left-faq-icon-div">
                <img src="img/faq2.png" class="little-faq-icon" alt="Could DCK Fuel Booster increases the octane rating?" title="Could DCK Fuel Booster increases the octane rating?">
            </div>
            <div class="middle-faq-div">
                <b>Could <b>DCK</b> Fuel Booster increase the octane rating?</b>
            </div>
            <div class="right-faq-div">
                <button class="clean show-button arrowtoggle" onclick="showAnswer25()">
                    <img src="img/plus.png" class="plus-icon" alt="Show" title="Show"  >
                    <img src="img/minus.png" class="plus-icon  minus-icon up"  alt="Hide" title="Hide"  style="display: none;">
                </button>
                
            </div>
        </div>
        <div class="answer-div answer-div1" id="answer25" style="display:none;">
            <div class="answer-div-css">
                <p class="answer-p">No, as octane rating indicates the anti-knock properties of a fuel, it is irrelevant, as <b>DCK</b> Fuel Booster’s principal function is fuel saving, cleansing, lubricating and improving engine power.</p>
            </div>
        </div>
    </div>

               
</div>

<script>
function showAnswer1() {
  var x = document.getElementById("answer1");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer2() {
  var x = document.getElementById("answer2");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer3() {
  var x = document.getElementById("answer3");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer4() {
  var x = document.getElementById("answer4");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer5() {
  var x = document.getElementById("answer5");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer6() {
  var x = document.getElementById("answer6");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer7() {
  var x = document.getElementById("answer7");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer8() {
  var x = document.getElementById("answer8");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer9() {
  var x = document.getElementById("answer9");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer10() {
  var x = document.getElementById("answer10");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer11() {
  var x = document.getElementById("answer11");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer12() {
  var x = document.getElementById("answer12");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer13() {
  var x = document.getElementById("answer13");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer14() {
  var x = document.getElementById("answer14");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer15() {
  var x = document.getElementById("answer15");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer16() {
  var x = document.getElementById("answer16");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer17() {
  var x = document.getElementById("answer17");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer18() {
  var x = document.getElementById("answer18");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer19() {
  var x = document.getElementById("answer19");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer20() {
  var x = document.getElementById("answer20");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer21() {
  var x = document.getElementById("answer21");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer22() {
  var x = document.getElementById("answer22");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer23() {
  var x = document.getElementById("answer23");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer24() {
  var x = document.getElementById("answer24");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
function showAnswer25() {
  var x = document.getElementById("answer25");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
	(function($) { 
	
		$('.arrowtoggle').click(function(event) {
			event.preventDefault();
			$('img:visible', this).hide().siblings().show();
			
		});
	
	})(jQuery);
</script>

<script type="text/javascript" src="js/main.js?version=1.0.1"></script>
<?php include 'js.php'; ?>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_GET['type'] == 1)
    {
        $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
    }
    else if($_GET['type'] == 2)
    {
        $messageType = "There are no referrer with this email ! Please register again";
    }
    else if($_GET['type'] == 3)
    {
        $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
    }
    else if($_GET['type'] == 4)
    {
        $messageType = "User password must be more than 5 !";
    }
    else if($_GET['type'] == 5)
    {
        $messageType = "User password does not match";
    }
    else if($_GET['type'] == 6)
    {
        $messageType = "Wrong email format.";
    }
    else if($_GET['type'] == 7)
    {
        $messageType = "There are no user with this email ! Please try again.";
    }
    else if($_GET['type'] == 8)
    {
        $messageType = "Successfully reset your password! Please check your email.";
    }
    else if($_GET['type'] == 9)
    {
        $messageType = "Successfully reset your password! ";
    }
    echo '
    <script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");
    </script>
    ';   
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");
    </script>
    ';   
}
?>
</body>
</html>