<!-- Fizo Here 1 --->
<!-- Notice Modal -->
<div id="notice-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css notice-modal-content">
    <span class="close-css close-notice">&times;</span>
    <h1 class="menu-h1">Title Here</h1>
	<div class="menu-link-container">Message Here</div>
  </div>

</div>
<!--- End part 1--->


<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>    
<script src="js/headroom.js"></script>

<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
    
<script>
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>


<!--- Modal Box --->
<script>
var loginmodal = document.getElementById("login-modal");
var menumodal = document.getElementById("menu-modal");
var registermodal = document.getElementById("register-modal");
var forgotmodal = document.getElementById("forgot-modal");
<!-- Fizo Here 2 --->
var noticemodal = document.getElementById("notice-modal");
<!---- End part 2--->
var openlogin = document.getElementsByClassName("open-login")[0];
var openlogin1 = document.getElementsByClassName("open-login")[1];
var openlogin2 = document.getElementsByClassName("open-login")[2];
var openlogin3 = document.getElementsByClassName("open-login")[3];
var openlogin4 = document.getElementsByClassName("open-login")[4];
var openmenu = document.getElementsByClassName("open-menu")[0];
var openregister = document.getElementsByClassName("open-register")[0];
var openregister1 = document.getElementsByClassName("open-register")[1];
var openregister2 = document.getElementsByClassName("open-register")[2];
var openforgot = document.getElementsByClassName("open-forgot")[0];
<!-- Fizo Here 3 --->
var opennotice = document.getElementsByClassName("open-notice")[0];
<!--- End Part 3 ---->
var closelogin = document.getElementsByClassName("close-login")[0];
var closemenu = document.getElementsByClassName("close-menu")[0];
var closemenu1 = document.getElementsByClassName("close-menu")[1];
var closemenu2 = document.getElementsByClassName("close-menu")[2];
var closemenu3 = document.getElementsByClassName("close-menu")[3];
var closeregister = document.getElementsByClassName("close-register")[0];
var closeforgot = document.getElementsByClassName("close-forgot")[0];
<!-- Fizo Here 4 --->
var closenotice = document.getElementsByClassName("close-notice")[0];
<!-- End Part 4 --->
openlogin.onclick = function() {
  loginmodal.style.display = "block";
}
openlogin1.onclick = function() {
  loginmodal.style.display = "block";
  menumodal.style.display = "none";
}
openlogin2.onclick = function() {
  loginmodal.style.display = "block";
  registermodal.style.display = "none";
}
openlogin3.onclick = function() {
  loginmodal.style.display = "block";
}
openlogin4.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
}
openmenu.onclick = function() {
  menumodal.style.display = "block";
}
openregister.onclick = function() {
  registermodal.style.display = "block";}
openregister1.onclick = function() {
  registermodal.style.display = "block";
  loginmodal.style.display = "none";
}
openregister2.onclick = function() {
  registermodal.style.display = "block";
  menumodal.style.display = "none";
}
openforgot.onclick = function() {
  forgotmodal.style.display = "block";
  loginmodal.style.display = "none";
}
<!-- Fizo Here 5 --->
opennotice.onclick = function() {
  noticemodal.style.display = "block";
}
<!---- End part 5 --->


closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
closemenu.onclick = function() {
  menumodal.style.display = "none";
}
closemenu.onclick = function() {
  menumodal.style.display = "none";
}
closemenu1.onclick = function() {
  menumodal.style.display = "none";
}
closemenu2.onclick = function() {
  menumodal.style.display = "none";
}
closemenu3.onclick = function() {
  menumodal.style.display = "none";
}
closeregister.onclick = function() {
  registermodal.style.display = "none";
}
closeforgot.onclick = function() {
  forgotmodal.style.display = "none";
}
<!-- Fizo Here 6 --->
closenotice.onclick = function() {
  noticemodal.style.display = "none";
}
<!--- End Part 6 --->
window.onclick = function(event) {
  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }
  if (event.target == menumodal) {
    menumodal.style.display = "none";
  }
  if (event.target == registermodal) {
    registermodal.style.display = "none";
  }  
  if (event.target == forgotmodal) {
    forgotmodal.style.display = "none";
  } 
  <!-- Fizo Here 7 --->
  if (event.target == noticemodal) {
    noticemodal.style.display = "none";
  } 
  <!--- End Part 7 --->  
}
</script>
