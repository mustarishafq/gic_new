<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$invoiceDetails = getInvoice($conn, "WHERE id =?",array("id"),array($_POST['invoice']), "s");
$invoiceNo = getInvoice($conn, "WHERE id =? ORDER BY id DESC LIMIT 1",array("id"),array($_POST['invoice']), "s");
$amount5 = 0;
$amount4 = 0;
$amount3 = 0;
$amount2 = 0;
$amount = 0;


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Invoice | GIC" />
    <title>Invoice | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Invoice</h1>
    <div class="short-red-border"></div>
    <!-- This is a filter for the table result -->


    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->

    <!-- End of Filter -->
    <div class="clear"></div>

    <div class="width100 print-div">
 		<div class="text-center">
            <img src="img/gic-logo.png" class="invoice-logo" alt="GIC Consultancy Sdn. Bhd." title="GIC Consultancy Sdn. Bhd.">
            <p class="invoice-address company-name"><b>GIC Consultancy Sdn. Bhd.</b></p>
            <p class="invoice-small-p">1189044-D (SST No : P11-1808-31038566)</p>
            <p class="invoice-address">1-2-42, Elit Avenue, Jalan Mayang Pasir 3, Bayan Lepas 11950 Penang.</p>
            <p class="invoice-address">Email: gicpenang@gmail.com  Tel:04-6374698</p>
        </div>
		<h1 class="invoice-title">INVOICE</h1>
        <div class="invoice-width50 top-invoice-w50">
        	<p class="invoice-p">
            	<b>Attn: <?php echo $invoiceDetails[0]->getPurchaserName(); ?></b>
            </p>
        </div>
        <div class="invoice-width50 top-invoice-w50">
			<table class="invoice-top-small-table">
            	<tr>
                	<td><b>Invoice No</b></td>
                    <td><b>:</b></td>
                    <td><b><?php echo date('Ymd', strtotime($invoiceNo[0]->getDateCreated())).$invoiceNo[0]->getID() ?></b></td>
                </tr>
                <tr>
                	<td>Project</td>
                    <td>:</td>
                    <td><?php echo $invoiceNo[0]->getProject()  ?></td>
                </tr>
                <tr>
                	<td>Status</td>
                    <td>:</td>
                    <td>Forfeit</td>
                </tr>
                <tr>
                	<td>Date</td>
                    <td>:</td>
                    <td><?php echo date('d/m/Y', strtotime($invoiceNo[0]->getDateCreated())) ?></td>
                </tr>
            </table>
        </div>
        <div class="clear"></div>
        <table class="invoice-printing-table">
        	<thead>
                    <tr>
                    	<th >No.</th>
                        <th >Items</th>
                        <th >Amount (RM)</th>
                    </tr>
            </thead>

                  <?php if ($invoiceDetails[0]->getItem()) {
                    $total = 0;
                    // for ($cnt=0; $cnt <count($invoiceDetails) ; $cnt++) {
                      ?><tr>
                      	<td class="td">1.</td>
                          <td><?php echo $invoiceDetails[0]->getItem() ?></td>
                        <td class="td"><?php echo $amount = $invoiceDetails[0]->getAmount() ?></td>
                    </tr>
                  <?php } ?>
                  <?php if ($invoiceDetails[0]->getItem2()) {
                    $total = 0;
                    // for ($cnt=0; $cnt <count($invoiceDetails) ; $cnt++) {
                      ?><tr>
                        <td class="td">2.</td>
                          <td><?php echo $invoiceDetails[0]->getItem2() ?></td>
                        <td class="td"><?php echo $amount2 = $invoiceDetails[0]->getAmount2() ?></td>
                    </tr>
                  <?php }else {
                    ?><tr>
                      <td ></td>
                        <td></td>
                      <td></td>
                  </tr> <?php
                  } ?>

                  <?php if ($invoiceDetails[0]->getItem3()) {
                    $total = 0;
                    // for ($cnt=0; $cnt <count($invoiceDetails) ; $cnt++) {
                      ?><tr>
                        <td class="td">3.</td>
                          <td><?php echo $invoiceDetails[0]->getItem3() ?></td>
                        <td class="td"><?php echo $amount3 = $invoiceDetails[0]->getAmount3() ?></td>
                    </tr>
                  <?php }else {
                    ?><tr>
                      <td ></td>
                        <td></td>
                      <td></td>
                  </tr> <?php
                  } ?>

                  <?php if ($invoiceDetails[0]->getItem4()) {
                    $total = 0;
                    // for ($cnt=0; $cnt <count($invoiceDetails) ; $cnt++) {
                      ?><tr>
                        <td class="td">4.</td>
                          <td><?php echo $invoiceDetails[0]->getItem4() ?></td>
                        <td class="td"><?php echo $amount4 = $invoiceDetails[0]->getAmount4() ?></td>
                    </tr>
                  <?php }else {
                    ?><tr>
                      <td ></td>
                        <td></td>
                      <td></td>
                  </tr> <?php
                  } ?>

                  <?php if ($invoiceDetails[0]->getItem5()) {
                    $total = 0;
                    // for ($cnt=0; $cnt <count($invoiceDetails) ; $cnt++) {
                      ?><tr>
                        <td class="td">5.</td>
                          <td><?php echo $invoiceDetails[0]->getItem5() ?></td>
                        <td class="td"><?php echo $amount5 = $invoiceDetails[0]->getAmount5() ?></td>
                    </tr>
                  <?php }else {
                    ?><tr>
                      <td ></td>
                        <td></td>
                      <td></td>
                  </tr> <?php
                  } ?>





        </table>
		<div class="clear"></div>
        <div class="invoice-width50 right-w50">
			<table class="invoice-bottom-small-table">
            	<tr>
                	<td>Sum Amount (excluding Service Tax)</td>
                    <td>:</td>
                    <td><?php echo ($amount5 + $amount4 + $amount3 + $amount2 + $amount); ?></td>
                </tr>
                <tr>
                	<td>Service Tax 6%</td>
                    <td>:</td>
                    <?php
                      if ($invoiceDetails[0]->getCharges() == 'YES') {
                        ?><td><?php echo $invoiceDetails[0]->getFinalAmount() - ($amount5 + $amount4 + $amount3 + $amount2 + $amount) ?></td><?php
                      }

                  ?>
                </tr>
                <tr>
                	<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                	<td><b>Total Amount</b></td>
                    <td>:</td>
                    <td class="bottom-3rd-td border-td"><b><?php echo $invoiceDetails[0]->getFinalAmount() ?></b></td>
                </tr>
            </table>

        </div>

        <div class="invoice-width50 left-w50">
			<table class="invoice-small-table">
            	<tr>
                	<td><b><u>Payee Details:</u></b></td>
                    <td><b>:</b></td>
                    <td></td>
                </tr>
                <tr>
                	<td>Name</td>
                    <td><b>:</b></td>
                    <td><b><?php echo $invoiceNo[0]->getBankAccountHolder(); ?></b></td>
                </tr>
                <tr>
                	<td>Bank</td>
                    <td><b>:</b></td>
                    <td><b><?php echo $invoiceNo[0]->getBankName(); ?></b></td>
                </tr>
                <tr>
                	<td>Account No.</td>
                    <td><b>:</b></td>
                    <td><b><?php echo $invoiceNo[0]->getBankAccountNo(); ?></b></td>
                </tr>
            </table>
        </div>
        <div class="invoice-print-spacing"></div>
        <div class="signature-div">
        	<div class="signature-border"></div>
            <p class="invoice-p"><b>GIC Consultancy Sdn Bhd</b></p>
            <p class="invoice-p">Eddie Song</p>
        </div>
    </div>
	<div class="clear"></div>
    <div class="dual-button-div width100 same-padding">
    	<a href="#">
            <div class="left-button white-red-line-btn">
                Edit
            </div>
        </a>
    	<a href="#">
            <button class="right-button red-btn clean"  onclick="window.print()">
                Print
            </button>
        </a>
    </div>


</div>




<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
</body>
</html>
