<?php
class Invoice {
    /* Member variables */
    var $id, $purchaserName, $item, $remark,$amount,$project,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,$dateCreated,$dateUpdated,$loanUid,$charges,$item2,$item3,$item4,$item5,$amount2,$amount3,$amount4,$amount5;

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setID($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPurchaserName()
    {
        return $this->purchaser_name;
    }

    /**
     * @param mixed $id
     */
    public function setPurchaserName($purchaserName)
    {
        $this->purchaser_name = $purchaserName;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $id
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return mixed
     */
    public function getItem2()
    {
        return $this->item2;
    }

    /**
     * @param mixed $id
     */
    public function setItem2($item2)
    {
        $this->item2 = $item2;
    }

    /**
     * @return mixed
     */
    public function getItem3()
    {
        return $this->item3;
    }

    /**
     * @param mixed $id
     */
    public function setItem3($item3)
    {
        $this->item3 = $item3;
    }

    /**
     * @return mixed
     */
    public function getItem4()
    {
        return $this->item4;
    }

    /**
     * @param mixed $id
     */
    public function setItem4($item4)
    {
        $this->item4 = $item4;
    }

    /**
     * @return mixed
     */
    public function getItem5()
    {
        return $this->item5;
    }

    /**
     * @param mixed $id
     */
    public function setItem5($item5)
    {
        $this->item5 = $item5;
    }

    /**
     * @return mixed
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param mixed $id
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $id
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getAmount2()
    {
        return $this->amount2;
    }

    /**
     * @param mixed $id
     */
    public function setAmount2($amount2)
    {
        $this->amount2 = $amount2;
    }

    /**
     * @return mixed
     */
    public function getAmount3()
    {
        return $this->amount3;
    }

    /**
     * @param mixed $id
     */
    public function setAmount3($amount3)
    {
        $this->amount3 = $amount3;
    }

    /**
     * @return mixed
     */
    public function getAmount4()
    {
        return $this->amount4;
    }

    /**
     * @param mixed $id
     */
    public function setAmount4($amount4)
    {
        $this->amount4 = $amount4;
    }

    /**
     * @return mixed
     */
    public function getAmount5()
    {
        return $this->amount5;
    }

    /**
     * @param mixed $id
     */
    public function setAmount5($amount5)
    {
        $this->amount5 = $amount5;
    }

    /**
     * @return mixed
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param mixed $id
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return mixed
     */
    public function getFinalAmount()
    {
        return $this->final_amount;
    }

    /**
     * @param mixed $id
     */
    public function setFinalAmount($finalAmount)
    {
        $this->final_amount = $finalAmount;
    }

    /**
     * @return mixed
     */
    public function getBankAccountHolder()
    {
        return $this->bank_account_holder;
    }

    /**
     * @param mixed $id
     */
    public function setBankAccountHolder($bankAccountHolder)
    {
        $this->bank_account_holder = $bankAccountHolder;
    }

    /**
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bank_name;
    }

    /**
     * @param mixed $id
     */
    public function setBankName($bankName)
    {
        $this->bank_name = $bankName;
    }

    /**
     * @return mixed
     */
    public function getBankAccountNo()
    {
        return $this->bank_account_no;
    }

    /**
     * @param mixed $id
     */
    public function setBankAccountNo($bankAccountNo)
    {
        $this->bank_account_no = $bankAccountNo;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getLoanUid()
    {
        return $this->loan_uid;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setLoanUid($loanUid)
    {
        $this->loan_uid = $loanUid;
    }

    /**
     * @return mixed
     */
    public function getCharges()
    {
        return $this->charges;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setCharges($charges)
    {
        $this->charges = $charges;
    }
}

function getInvoice($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","purchaser_name", "item", "remark", "amount", "project", "final_amount","bank_account_holder","bank_name","bank_account_no","date_created","date_updated","loan_uid","charges","item2","item3","item4","item5","amount2","amount3","amount4","amount5");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"invoice");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $purchaserName, $item, $remark,$amount,$project,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,$dateCreated,$dateUpdated,$loanUid,$charges,$item2,$item3,$item4,$item5,$amount2,$amount3,$amount4,$amount5);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Invoice();
            $class->setID($id);
            $class->setPurchaserName($purchaserName);
            $class->setItem($item);
            $class->setItem2($item2);
            $class->setItem3($item3);
            $class->setItem4($item4);
            $class->setItem5($item5);
            $class->setRemark($remark);
            $class->setAmount($amount);
            $class->setAmount2($amount2);
            $class->setAmount3($amount3);
            $class->setAmount4($amount4);
            $class->setAmount5($amount5);
            $class->setProject($project);
            $class->setBankAccountHolder($bankAccountHolder);
            $class->setBankName($bankName);
            $class->setBankAccountNo($bankAccountNo);
            $class->setFinalAmount($finalAmount);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
            $class->setLoanUid($loanUid);
            $class->setCharges($charges);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}
