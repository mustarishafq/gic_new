<?php
class User {
    /* Member variables */
    var $id,$username,$email,$password,$salt,$phoneNo,$icNo,$fullName,
            $userType,$dateCreated,
                $address,$birthday,$bankName,$bankAccountNo,$upline1,$upline2;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getPhoneNo()
    {
        return $this->contact;
    }

    /**
     * @param mixed $phoneNo
     */
    public function setPhoneNo($phoneNo)
    {
        $this->contact = $phoneNo;
    }

    /**
     * @return mixed
     */
    public function getIcNo()
    {
        return $this->icNo;
    }

    /**
     * @param mixed $icNo
     */
    public function setIcNo($icNo)
    {
        $this->icNo = $icNo;
    }

    /**
     * @return mixed
     */
    public function getUpline1()
    {
        return $this->upline1;
    }

    /**
     * @param mixed $countryId
     */
    public function setUpline1($upline1)
    {
        $this->upline1 = $upline1;
    }

    /**
     * @return mixed
     */
    public function getUpline2()
    {
        return $this->upline2;
    }

    /**
     * @param mixed $countryId
     */
    public function setUpline2($upline2)
    {
        $this->upline2 = $upline2;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    // /**
    //  * @return mixed
    //  */
    // public function getEpin()
    // {
    //     return $this->epin;
    // }
    //
    // /**
    //  * @param mixed $epin
    //  */
    // public function setEpin($epin)
    // {
    //     $this->epin = $epin;
    // }

    // /**
    //  * @return mixed
    //  */
    // public function getSaltEpin()
    // {
    //     return $this->saltEpin;
    // }
    //
    // /**
    //  * @param mixed $saltEpin
    //  */
    // public function setSaltEpin($saltEpin)
    // {
    //     $this->saltEpin = $saltEpin;
    // }

    // /**
    //  * @return mixed
    //  */
    // public function getEmailVerificationCode()
    // {
    //     return $this->emailVerificationCode;
    // }
    //
    // /**
    //  * @param mixed $emailVerificationCode
    //  */
    // public function setEmailVerificationCode($emailVerificationCode)
    // {
    //     $this->emailVerificationCode = $emailVerificationCode;
    // }

    // /**
    //  * @return mixed
    //  */
    // public function getisEmailVerified()
    // {
    //     return $this->isEmailVerified;
    // }
    //
    // /**
    //  * @param mixed $isEmailVerified
    //  */
    // public function setIsEmailVerified($isEmailVerified)
    // {
    //     $this->isEmailVerified = $isEmailVerified;
    // }

    // /**
    //  * @return mixed
    //  */
    // public function getisPhoneVerified()
    // {
    //     return $this->isPhoneVerified;
    // }
    //
    // /**
    //  * @param mixed $isPhoneVerified
    //  */
    // public function setIsPhoneVerified($isPhoneVerified)
    // {
    //     $this->isPhoneVerified = $isPhoneVerified;
    // }

    // /**
    //  * @return mixed
    //  */
    // public function getLoginType()
    // {
    //     return $this->loginType;
    // }
    //
    // /**
    //  * @param mixed $loginType
    //  */
    // public function setLoginType($loginType)
    // {
    //     $this->loginType = $loginType;
    // }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    // /**
    //  * @return mixed
    //  */
    // public function getDateUpdated()
    // {
    //     return $this->dateUpdated;
    // }
    //
    // /**
    //  * @param mixed $dateUpdated
    //  */
    // public function setDateUpdated($dateUpdated)
    // {
    //     $this->dateUpdated = $dateUpdated;
    // }

    // /**
    //  * @return mixed
    //  */
    // public function getDownlineAccumulatedPoints()
    // {
    //     return $this->downlineAccumulatedPoints;
    // }
    //
    // /**
    //  * @param mixed $downlineAccumulatedPoints
    //  */
    // public function setDownlineAccumulatedPoints($downlineAccumulatedPoints)
    // {
    //     $this->downlineAccumulatedPoints = $downlineAccumulatedPoints;
    // }
    //
    // /**
    //  * @return mixed
    //  */
    // public function getisReferred()
    // {
    //     return $this->isReferred;
    // }
    //
    // /**
    //  * @param mixed $isReferred
    //  */
    // public function setIsReferred($isReferred)
    // {
    //     $this->isReferred = $isReferred;
    // }
    //
    // /**
    //  * @return mixed
    //  */
    // public function getCanSendNewsletter()
    // {
    //     return $this->canSendNewsletter;
    // }
    //
    // /**
    //  * @param mixed $canSendNewsletter
    //  */
    // public function setCanSendNewsletter($canSendNewsletter)
    // {
    //     $this->canSendNewsletter = $canSendNewsletter;
    // }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    // /**
    //  * @return mixed
    //  */
    // public function getGender()
    // {
    //     return $this->gender;
    // }
    //
    // /**
    //  * @param mixed $gender
    //  */
    // public function setGender($gender)
    // {
    //     $this->gender = $gender;
    // }

    /**
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param mixed $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

// /**
//      * @return mixed
//      */
//     public function getBankAccountHolder()
//     {
//         return $this->bankAccountHolder;
//     }
//
//     /**
//      * @param mixed $bankAccountHolder
//      */
//     public function setBankAccountHolder($bankAccountHolder)
//     {
//         $this->bankAccountHolder = $bankAccountHolder;
//     }

    /**
     * @return mixed
     */
    public function getBankAccountNo()
    {
        return $this->bankAccountNo;
    }

    /**
     * @param mixed $bankAccountNo
     */
    public function setBankAccountNo($bankAccountNo)
    {
        $this->bankAccountNo = $bankAccountNo;
    }

    // /**
    //  * @return mixed
    //  */
    // public function getCarModel()
    // {
    //     return $this->carModel;
    // }
    //
    // /**
    //  * @param mixed $carModel
    //  */
    // public function setCarModel($carModel)
    // {
    //     $this->carModel = $carModel;
    // }
    //
    // /**
    //  * @return mixed
    //  */
    // public function getCarYear()
    // {
    //     return $this->carYear;
    // }
    //
    // /**
    //  * @param mixed $carYear
    //  */
    // public function setCarYear($carYear)
    // {
    //     $this->carYear = $carYear;
    // }
    //
    // /**
    //  * @return mixed
    //  */
    // public function getPicture()
    // {
    //     return $this->picture;
    // }
    //
    // /**
    //  * @param mixed $picture
    //  */
    // public function setPicture($picture)
    // {
    //     $this->picture = $picture;
    // }
    //
    //     /**
    //  * @return mixed
    //  */
    // public function getRegisterDownlineNo()
    // {
    //     return $this->registerDownlineNo;
    // }
    //
    // /**
    //  * @param mixed $registerDownlineNo
    //  */
    // public function setRegisterDownlineNo($registerDownlineNo)
    // {
    //     $this->registerDownlineNo = $registerDownlineNo;
    // }
    //
    // /**
    //  * @return mixed
    //  */
    // public function getBonus()
    // {
    //     return $this->bonus;
    // }
    //
    // /**
    //  * @param mixed $accumulateCash
    //  */
    // public function setBonus($bonus)
    // {
    //     $this->bonus = $bonus;
    // }
    //
    // /**
    //    * @return mixed
    //    */
    //   public function getWithdrawAmount()
    //   {
    //       return $this->final_amount;
    //   }
    //
    //   /**
    //    * @param mixed $withdrawAmount
    //    */
    //   public function setWithdrawAmount($withdrawAmount)
    //   {
    //       $this->final_amount = $withdrawAmount;
    //   }
    //
    //   /**
    //    * @return mixed
    //    */
    //   public function getUserPoint()
    //   {
    //       return $this->point;
    //   }
    //
    //   /**
    //    * @param mixed $userPoint
    //    */
    //   public function setUserPoint($userPoint)
    //   {
    //       $this->point = $userPoint;
    //   }
    //
    //   /**
    //    * @return mixed
    //    */
    //   public function getCurrentStatus()
    //   {
    //       return $this->current_status;
    //   }
    //
    //   /**
    //    * @param mixed $id
    //    */
    //   public function setCurrentStatus($currentStatus)
    //   {
    //       $this->current_status = $currentStatus;
    //   }



}

function getUser($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","username","email","password","salt","contact","ic","full_name",
        "user_type","date_created","address","birth_month","bank","bank_no","upline1","upline2");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"user");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$username,$email,$password,$salt,$phoneNo,$icNo,$fullName,
                $userType,$dateCreated,$address,$birthday,$bankName,$bankAccountNo,$upline1,$upline2);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new User;
            $user->setId($id);
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setPassword($password);
            $user->setSalt($salt);
            $user->setPhoneNo($phoneNo);
            $user->setIcNo($icNo);
            $user->setFullName($fullName);
            // $user->setEpin($epin);
            // $user->setSaltEpin($saltEpin);
            // $user->setEmailVerificationCode($emailVerificationCode);
            // $user->setIsEmailVerified($isEmailVerified);
            // $user->setIsPhoneVerified($isPhoneVerified);
            // $user->setLoginType($loginType);
            $user->setUserType($userType);
            // $user->setDownlineAccumulatedPoints($downlineAccumulatedPoints);
            // $user->setCanSendNewsletter($canSendNewsletter);
            // $user->setIsReferred($isReferred);
            $user->setDateCreated($dateCreated);
            // $user->setDateUpdated($dateUpdated);
            $user->setAddress($address);
            $user->setBirthday($birthday);
            // $user->setGender($gender);
            $user->setBankName($bankName);
            // $user->setBankAccountHolder($bankAccountHolder);
            $user->setBankAccountNo($bankAccountNo);
            // $user->setCarModel($carModel);
            
            // $user->setPicture($picture);
            // $user->setRegisterDownlineNo($registerDownlineNo);
            // $user->setBonus($bonus);
            // $user->setWithdrawAmount($withdrawAmount);
            // $user->setUserPoint($userPoint);
            // $user->setCurrentStatus($currentStatus);
            $user->setUpline1($upline1);
            $user->setUpline2($upline2);

            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function createTotalWithdrawAmount(){

  $totaedlWithdrawAmount = 0;

      if($withdrawAmount > 0){

          $totaledWithdrawAmount = $withdrawAmount + $userDetails->getWithdrawAmount();

      }else{

      }


}
