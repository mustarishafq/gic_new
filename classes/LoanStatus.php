<?php
// class Product{
class LoanStatus{
    var $id, $projectName, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package,
            $discount, $rebate, $extraRebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus,
            $remark, $bFormCollected, $paymentMethod, $lawyer, $pendingApprovalStatus, $bankApproved, $loSignedDate, $laSignedDate,

            $spaSignedDate, $fullsetCompleted, $cashBuyer, $cancelledBooking, $caseStatus, $eventPersonal, $rate, $agentComm,
            $upline1, $upline2, $plName, $hosName, $listerName, $ulOverride, $uulOverride,
            $plOverride, $hosOverride, $listerOverride, $admin1Override, $admin2Override, $admin3Override, $gicProfit,

            $totalClaimedDevAmt, $totalBalUnclaimAmt, $claimInvoiceNo1st, $claimAmt1st, $claimInvoiceNo2nd, $claimAmt2nd,
            $claimInvoiceNo3rd, $claimAmt3rd, $claimInvoiceNo4th, $claimAmt4th, $claimInvoiceNo5th, $claimAmt5th,
            $claimInvoiceNo6th, $claimAmt6th, $claimInvoiceNo7th, $claimAmt7th, $claimInvoiceNo8th, $claimAmt8th,
            $claimInvoiceNo9th, $claimAmt9th, $claimInvoiceNo10th, $claimAmt10th,
            $dateCreated, $dateUpdated,$loanUid;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    //additional class for loanUid and projectId
    /**
     * @return mixed
     */
    public function getLoanUid()
    {
    return $this->loan_uid;
    }

    /**
     * @param mixed $id
     */
    public function setLoanUid($loanUid)
    {
    $this->loan_uid = $loanUid;
    }

    /**
     * @return mixed
     */
    public function getProjectName()
    {
    return $this->projectName;
    }

    /**
     * @param mixed $projectName
     */
    public function setProjectName($projectName)
    {
    $this->projprojectNameectId = $projectName;
    }

    /**
     * @return mixed
     */
    public function getUnitNo()
    {
        return $this->unitNo;
    }

    /**
     * @param mixed $unitNo
     */
    public function setUnitNo($unitNo)
    {
        $this->unitNo = $unitNo;
    }

    /**
     * @return mixed
     */
    public function getPurchaserName()
    {
        return $this->purchaserName;
    }

    /**
     * @param mixed $purchaserName
     */
    public function setPurchaserName($purchaserName)
    {
        $this->purchaserName = $purchaserName;
    }

    /**
     * @return mixed
     */
    public function getIc()
    {
        return $this->ic;
    }

    /**
     * @param mixed $ic
     */
    public function setIc($ic)
    {
        $this->ic = $ic;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getBookingDate()
    {
        return $this->bookingDate;
    }

    /**
     * @param mixed $bookingDate
     */
    public function setBookingDate($bookingDate)
    {
        $this->bookingDate = $bookingDate;
    }

    /**
     * @return mixed
     */
    public function getSqFt()
    {
        return $this->sqFt;
    }

    /**
     * @param mixed $sqFt
     */
    public function setSqFt($sqFt)
    {
        $this->sqFt = $sqFt;
    }

    /**
     * @return mixed
     */
    public function getSpaPrice()
    {
        return $this->spaPrice;
    }

    /**
     * @param mixed $spaPrice
     */
    public function setSpaPrice($spaPrice)
    {
        $this->spaPrice = $spaPrice;
    }

    /**
     * @return mixed
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * @param mixed $package
     */
    public function setPackage($package)
    {
        $this->package = $package;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getRebate()
    {
        return $this->rebate;
    }

    /**
     * @param mixed $rebate
     */
    public function setRebate($rebate)
    {
        $this->rebate = $rebate;
    }

        /**
     * @return mixed
     */
    public function getExtraRebate()
    {
        return $this->extraRebate;
    }

    /**
     * @param mixed $extraRebate
     */
    public function setExtraRebate($extraRebate)
    {
        $this->extraRebate = $extraRebate;
    }

    /**
     * @return mixed
     */
    public function getNettPrice()
    {
        return $this->nettPrice;
    }

    /**
     * @param mixed $nettPrice
     */
    public function setNettPrice($nettPrice)
    {
        $this->nettPrice = $nettPrice;
    }

    /**
     * @return mixed
     */
    public function getTotalDeveloperComm()
    {
        return $this->totalDeveloperComm;
    }

    /**
     * @param mixed $totalDeveloperComm
     */
    public function setTotalDeveloperComm($totalDeveloperComm)
    {
        $this->totalDeveloperComm = $totalDeveloperComm;
    }

    /**
     * @return mixed
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param mixed $agent
     */
    public function setAgent($agent)
    {
        $this->agent = $agent;
    }

    /**
     * @return mixed
     */
    public function getLoanStatus()
    {
        return $this->loanStatus;
    }

    /**
     * @param mixed $loanStatus
     */
    public function setLoanStatus($loanStatus)
    {
        $this->loanStatus = $loanStatus;
    }

    /**
     * @return mixed
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param mixed $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return mixed
     */
    public function getBFormCollected()
    {
        return $this->bFormCollected;
    }

    /**
     * @param mixed $bFormCollected
     */
    public function setBFormCollected($bFormCollected)
    {
        $this->bFormCollected = $bFormCollected;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param mixed $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return mixed
     */
    public function getLawyer()
    {
        return $this->lawyer;
    }

    /**
     * @param mixed $lawyer
     */
    public function setLawyer($lawyer)
    {
        $this->lawyer = $lawyer;
    }

    /**
     * @return mixed
     */
    public function getPendingApprovalStatus()
    {
        return $this->pendingApprovalStatus;
    }

    /**
     * @param mixed $pendingApprovalStatus
     */
    public function setPendingApprovalStatus($pendingApprovalStatus)
    {
        $this->pendingApprovalStatus = $pendingApprovalStatus;
    }

    /**
     * @return mixed
     */
    public function getBankApproved()
    {
        return $this->bankApproved;
    }

    /**
     * @param mixed $bankApproved
     */
    public function setBankApproved($bankApproved)
    {
        $this->bankApproved = $bankApproved;
    }

    /**
     * @return mixed
     */
    public function getLoSignedDate()
    {
        return $this->loSignedDate;
    }

    /**
     * @param mixed $loSignedDate
     */
    public function setLoSignedDate($loSignedDate)
    {
        $this->loSignedDate = $loSignedDate;
    }

    /**
     * @return mixed
     */
    public function getLaSignedDate()
    {
        return $this->laSignedDate;
    }

    /**
     * @param mixed $laSignedDate
     */
    public function setLaSignedDate($laSignedDate)
    {
        $this->laSignedDate = $laSignedDate;
    }

    /**
     * @return mixed
     */
    public function getSpaSignedDate()
    {
        return $this->spaSignedDate;
    }

    /**
     * @param mixed $spaSignedDate
     */
    public function setSpaSignedDate($spaSignedDate)
    {
        $this->spaSignedDate = $spaSignedDate;
    }

    /**
     * @return mixed
     */
    public function getFullsetCompleted()
    {
        return $this->fullsetCompleted;
    }

    /**
     * @param mixed $fullsetCompleted
     */
    public function setFullsetCompleted($fullsetCompleted)
    {
        $this->fullsetCompleted = $fullsetCompleted;
    }

    /**
     * @return mixed
     */
    public function getCashBuyer()
    {
        return $this->cashBuyer;
    }

    /**
     * @param mixed $cashBuyer
     */
    public function setCashBuyer($cashBuyer)
    {
        $this->cashBuyer = $cashBuyer;
    }

    /**
     * @return mixed
     */
    public function getCancelledBooking()
    {
        return $this->cancelledBooking;
    }

    /**
     * @param mixed $cancelledBooking
     */
    public function setCancelledBooking($cancelledBooking)
    {
        $this->cancelledBooking = $cancelledBooking;
    }

    /**
     * @return mixed
     */
    public function getCaseStatus()
    {
        return $this->caseStatus;
    }

    /**
     * @param mixed $caseStatus
     */
    public function setCaseStatus($caseStatus)
    {
        $this->caseStatus = $caseStatus;
    }

    /**
     * @return mixed
     */
    public function getEventPersonal()
    {
        return $this->eventPersonal;
    }

    /**
     * @param mixed $eventPersonal
     */
    public function setEventPersonal($eventPersonal)
    {
        $this->eventPersonal = $eventPersonal;
    }

    /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param mixed $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return mixed
     */
    public function getAgentComm()
    {
        return $this->agentComm;
    }

    /**
     * @param mixed $agentComm
     */
    public function setAgentComm($agentComm)
    {
        $this->agentComm = $agentComm;
    }

    /**
     * @return mixed
     */
    public function getUpline1()
    {
        return $this->upline1;
    }

    /**
     * @param mixed $upline1
     */
    public function setUpline1($upline1)
    {
        $this->upline1 = $upline1;
    }

    /**
     * @return mixed
     */
    public function getUpline2()
    {
        return $this->upline2;
    }

    /**
     * @param mixed $upline2
     */
    public function setUpline2($upline2)
    {
        $this->upline2 = $upline2;
    }

    /**
     * @return mixed
     */
    public function getPlName()
    {
        return $this->plName;
    }

    /**
     * @param mixed $plName
     */
    public function setPlName($plName)
    {
        $this->plName = $plName;
    }

    /**
     * @return mixed
     */
    public function getHosName()
    {
        return $this->hosName;
    }

    /**
     * @param mixed $hosName
     */
    public function setHosName($hosName)
    {
        $this->hosName = $hosName;
    }

    /**
     * @return mixed
     */
    public function getListerName()
    {
        return $this->listerName;
    }

    /**
     * @param mixed $listerName
     */
    public function setListerName($listerName)
    {
        $this->listerName = $listerName;
    }

    /**
     * @return mixed
     */
    public function getUlOverride()
    {
        return $this->ulOverride;
    }

    /**
     * @param mixed $ulOverride
     */
    public function setUlOverride($ulOverride)
    {
        $this->ulOverride = $ulOverride;
    }

    /**
     * @return mixed
     */
    public function getUulOverride()
    {
        return $this->uulOverride;
    }

    /**
     * @param mixed $uulOverride
     */
    public function setUulOverride($uulOverride)
    {
        $this->uulOverride = $uulOverride;
    }

    /**
     * @return mixed
     */
    public function getPlOverride()
    {
        return $this->plOverride;
    }

    /**
     * @param mixed $plOverride
     */
    public function setPlOverride($plOverride)
    {
        $this->plOverride = $plOverride;
    }

    /**
     * @return mixed
     */
    public function getHosOverride()
    {
        return $this->hosOverride;
    }

    /**
     * @param mixed $hosOverride
     */
    public function setHosOverride($hosOverride)
    {
        $this->hosOverride = $hosOverride;
    }

    /**
     * @return mixed
     */
    public function getListerOverride()
    {
        return $this->listerOverride;
    }

    /**
     * @param mixed $listerOverride
     */
    public function setListerOverride($listerOverride)
    {
        $this->listerOverride = $listerOverride;
    }

    /**
     * @return mixed
     */
    public function getAdmin1Override()
    {
        return $this->admin1Override;
    }

    /**
     * @param mixed $admin1Override
     */
    public function setAdmin1Override($admin1Override)
    {
        $this->admin1Override = $admin1Override;
    }

    /**
     * @return mixed
     */
    public function getAdmin2Override()
    {
        return $this->admin2Override;
    }

    /**
     * @param mixed $admin2Override
     */
    public function setAdmin2Override($admin2Override)
    {
        $this->admin2Override = $admin2Override;
    }

    /**
     * @return mixed
     */
    public function getAdmin3Override()
    {
        return $this->admin3Override;
    }

    /**
     * @param mixed $admin3Override
     */
    public function setAdmin3Override($admin3Override)
    {
        $this->admin3Override = $admin3Override;
    }

    /**
     * @return mixed
     */
    public function getGicProfit()
    {
        return $this->gicProfit;
    }

    /**
     * @param mixed $gicProfit
     */
    public function setGicProfit($gicProfit)
    {
        $this->gicProfit = $gicProfit;
    }

    /**
     * @return mixed
     */
    public function getTotalClaimDevAmt()
    {
        return $this->totalClaimedDevAmt;
    }

    /**
     * @param mixed $totalClaimedDevAmt
     */
    public function setTotalClaimDevAmt($totalClaimedDevAmt)
    {
        $this->totalClaimedDevAmt = $totalClaimedDevAmt;
    }

    /**
     * @return mixed
     */
    public function getTotalBalUnclaimAmt()
    {
        return $this->totalBalUnclaimAmt;
    }

    /**
     * @param mixed $totalBalUnclaimAmt
     */
    public function setTotalBalUnclaimAmt($totalBalUnclaimAmt)
    {
        $this->totalBalUnclaimAmt = $totalBalUnclaimAmt;
    }

    /**
     * @return mixed
     */
    public function getClaimInvoice1st()
    {
        return $this->claimInvoiceNo1st;
    }

    /**
     * @param mixed $claimInvoiceNo1st
     */
    public function setClaimInvoice1st($claimInvoiceNo1st)
    {
        $this->claimInvoiceNo1st = $claimInvoiceNo1st;
    }

    /**
     * @return mixed
     */
    public function getClaimAmt1st()
    {
        return $this->claimAmt1st;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setClaimAmt1st($claimAmt1st)
    {
        $this->claimAmt1st = $claimAmt1st;
    }

        /**
     * @return mixed
     */
    public function getClaimInvoice2nd()
    {
        return $this->claimInvoiceNo2nd;
    }

    /**
     * @param mixed $claimInvoiceNo2nd
     */
    public function setClaimInvoice2nd($claimInvoiceNo2nd)
    {
        $this->claimInvoiceNo2nd = $claimInvoiceNo2nd;
    }

    /**
     * @return mixed
     */
    public function getClaimAmt2nd()
    {
        return $this->claimAmt2nd;
    }

    /**
     * @param mixed $claimAmt2nd
     */
    public function setClaimAmt2nd($claimAmt2nd)
    {
        $this->claimAmt2nd = $claimAmt2nd;
    }

        /**
     * @return mixed
     */
    public function getClaimInvoice3rd()
    {
        return $this->claimInvoiceNo3rd;
    }

    /**
     * @param mixed $claimInvoiceNo3rd
     */
    public function setClaimInvoice3rd($claimInvoiceNo3rd)
    {
        $this->claimInvoiceNo3rd = $claimInvoiceNo3rd;
    }

    /**
     * @return mixed
     */
    public function getClaimAmt3rd()
    {
        return $this->claimAmt3rd;
    }

    /**
     * @param mixed $claimAmt3rd
     */
    public function setClaimAmt3rd($claimAmt3rd)
    {
        $this->claimAmt3rd = $claimAmt3rd;
    }

    /**
     * @return mixed
     */
    public function getClaimInvoice4th()
    {
        return $this->claimInvoiceNo4th;
    }

    /**
     * @param mixed $claimInvoiceNo4th
     */
    public function setClaimInvoice4th($claimInvoiceNo4th)
    {
        $this->claimInvoiceNo4th = $claimInvoiceNo4th;
    }

    /**
     * @return mixed
     */
    public function getClaimAmt4th()
    {
        return $this->claimAmt4th;
    }

    /**
     * @param mixed $claimAmt4th
     */
    public function setClaimAmt4th($claimAmt4th)
    {
        $this->claimAmt4th = $claimAmt4th;
    }

    /**
     * @return mixed
     */
    public function getClaimInvoice5th()
    {
        return $this->claimInvoiceNo5th;
    }

    /**
     * @param mixed $claimInvoiceNo5th
     */
    public function setClaimInvoice5th($claimInvoiceNo5th)
    {
        $this->claimInvoiceNo5th = $claimInvoiceNo5th;
    }

    /**
     * @return mixed
     */
    public function getClaimAmt5th()
    {
        return $this->claimAmt5th;
    }

    /**
     * @param mixed $claimAmt5th
     */
    public function setClaimAmt5th($claimAmt5th)
    {
        $this->claimAmt5th = $claimAmt5th;
    }

    /**
     * @return mixed
     */
    public function getClaimInvoice6th()
    {
        return $this->claimInvoiceNo6th;
    }

    /**
     * @param mixed $claimInvoiceNo6th
     */
    public function setClaimInvoice6th($claimInvoiceNo6th)
    {
        $this->claimInvoiceNo6th = $claimInvoiceNo6th;
    }

    /**
     * @return mixed
     */
    public function getClaimAmt6th()
    {
        return $this->claimAmt6th;
    }

    /**
     * @param mixed $claimAmt6th
     */
    public function setClaimAmt6th($claimAmt6th)
    {
        $this->claimAmt6th = $claimAmt6th;
    }

    /**
     * @return mixed
     */
    public function getClaimInvoice7th()
    {
        return $this->claimInvoiceNo7th;
    }

    /**
     * @param mixed $claimInvoiceNo7th
     */
    public function setClaimInvoice7th($claimInvoiceNo7th)
    {
        $this->claimInvoiceNo7th = $claimInvoiceNo7th;
    }

    /**
     * @return mixed
     */
    public function getClaimAmt7th()
    {
        return $this->claimAmt7th;
    }

    /**
     * @param mixed $claimAmt7th
     */
    public function setClaimAmt7th($claimAmt7th)
    {
        $this->claimAmt7th = $claimAmt7th;
    }

    /**
     * @return mixed
     */
    public function getClaimInvoice8th()
    {
        return $this->claimInvoiceNo8th;
    }

    /**
     * @param mixed $claimInvoiceNo8th
     */
    public function setClaimInvoice8th($claimInvoiceNo8th)
    {
        $this->claimInvoiceNo8th = $claimInvoiceNo8th;
    }

    /**
     * @return mixed
     */
    public function getClaimAmt8th()
    {
        return $this->claimAmt8th;
    }

    /**
     * @param mixed $claimAmt8th
     */
    public function setClaimAmt8th($claimAmt8th)
    {
        $this->claimAmt8th = $claimAmt8th;
    }

    /**
     * @return mixed
     */
    public function getClaimInvoice9th()
    {
        return $this->claimInvoiceNo9th;
    }

    /**
     * @param mixed $claimInvoiceNo9th
     */
    public function setClaimInvoice9th($claimInvoiceNo9th)
    {
        $this->claimInvoiceNo9th = $claimInvoiceNo9th;
    }

    /**
     * @return mixed
     */
    public function getClaimAmt9th()
    {
        return $this->claimAmt9th;
    }

    /**
     * @param mixed $claimAmt9th
     */
    public function setClaimAmt9th($claimAmt9th)
    {
        $this->claimAmt9th = $claimAmt9th;
    }

    /**
     * @return mixed
     */
    public function getClaimInvoice10th()
    {
        return $this->claimInvoiceNo10th;
    }

    /**
     * @param mixed $claimInvoiceNo10th
     */
    public function setClaimInvoice10th($claimInvoiceNo10th)
    {
        $this->claimInvoiceNo10th = $claimInvoiceNo10th;
    }

    /**
     * @return mixed
     */
    public function getClaimAmt10th()
    {
        return $this->claimAmt10th;
    }

    /**
     * @param mixed $claimAmt10th
     */
    public function setClaimAmt10th($claimAmt10th)
    {
        $this->claimAmt10th = $claimAmt10th;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getLoanStatus($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){

    $dbColumnNames = array("id","project_name","unit_no","purchaser_name","ic","contact","email","booking_date","sq_ft","spa_price","package",
                        "discount","rebate","extra_rebate","nettprice","totaldevelopercomm","agent","loanstatus",
                        "remark","bform_Collected","payment_method","lawyer","pending_approval_status","bank_approved","lo_signed_date","la_signed_date",
                        "spa_signed_date","fullset_completed","cash_buyer","cancelled_booking","case_status","event_personal","rate","agent_comm",
                        "upline1","upline2","pl_name","hos_name","lister_name","ul_override","uul_override",
                        "pl_override","hos_override","lister_override","admin1_override","admin2_override","admin3_override","gic_profit",
                        "total_claimed_dev_amt","total_bal_unclaim_amt","1st_claim_invoice_no","1st_claim_amt","2nd_claim_invoice_no","2nd_claim_amt",
                        "3rd_claim_invoice_no","3rd_claim_amt","4th_claim_invoice_no","4th_claim_amt","5th_claim_invoice_no","5th_claim_amt",
                        "6th_claim_invoice_no","6th_claim_amt","7th_claim_invoice_no","7th_claim_amt","8th_claim_invoice_no","8th_claim_amt",
                        "9th_claim_invoice_no","9th_claim_amt","10th_claim_invoice_no","10th_claim_amt","date_created","date_updated","loan_uid");


    // $sql = sqlSelectSimpleBuilder($dbColumnNames,"admin_project");
    $sql = sqlSelectSimpleBuilder($dbColumnNames,"loan_status");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($id, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package,
        //                         $rebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus,
        //                             $remark, $formCollected, $bankApproved, $loSignedDate, $laSignedDate);

        $stmt->bind_result($id, $projectName, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package,
                                $discount, $rebate, $extraRebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus,
                                    $remark, $bFormCollected, $paymentMethod, $lawyer, $pendingApprovalStatus, $bankApproved, $loSignedDate, $laSignedDate,
                                        $spaSignedDate, $fullsetCompleted, $cashBuyer, $cancelledBooking, $caseStatus, $eventPersonal, $rate, $agentComm,
                                            $upline1, $upline2, $plName, $hosName, $listerName, $ulOverride, $uulOverride,
                                                $plOverride, $hosOverride, $listerOverride, $admin1Override, $admin2Override, $admin3Override, $gicProfit,
                                                    $totalClaimedDevAmt, $totalBalUnclaimAmt, $claimInvoiceNo1st, $claimAmt1st, $claimInvoiceNo2nd, $claimAmt2nd,
                                                        $claimInvoiceNo3rd, $claimAmt3rd, $claimInvoiceNo4th, $claimAmt4th, $claimInvoiceNo5th, $claimAmt5th,
                                                            $claimInvoiceNo6th, $claimAmt6th, $claimInvoiceNo7th, $claimAmt7th, $claimInvoiceNo8th, $claimAmt8th,
                                                                $claimInvoiceNo9th, $claimAmt9th, $claimInvoiceNo10th, $claimAmt10th, $dateCreated, $dateUpdated,$loanUid);


        $resultRows = array();
        while ($stmt->fetch()) {
            // $class = new Product();

            $class = new LoanStatus();

            $class->setId($id);
            $class->setProjectName($projectName);
            $class->setUnitNo($unitNo);
            $class->setPurchaserName($purchaserName);
            $class->setIc($ic);
            $class->setContact($contact);
            $class->setEmail($email);
            $class->setBookingDate($bookingDate);
            $class->setSqFt($sqFt);
            $class->setSpaPrice($spaPrice);
            $class->setPackage($package);

            $class->setDiscount($discount);
            $class->setRebate($rebate);
            $class->setExtraRebate($extraRebate);
            $class->setNettPrice($nettPrice);
            $class->setTotalDeveloperComm($totalDeveloperComm);
            $class->setAgent($agent);
            $class->setLoanStatus($loanStatus);
            $class->setRemark($remark);
            $class->setBFormCollected($bFormCollected);
            $class->setPaymentMethod($paymentMethod);

            $class->setLawyer($lawyer);
            $class->setPendingApprovalStatus($pendingApprovalStatus);
            $class->setBankApproved($bankApproved);
            $class->setLoSignedDate($loSignedDate);
            $class->setLaSignedDate($laSignedDate);
            $class->setSpaSignedDate($spaSignedDate);
            $class->setFullsetCompleted($fullsetCompleted);
            $class->setCashBuyer($cashBuyer);
            $class->setCancelledBooking($cancelledBooking);
            $class->setCaseStatus($caseStatus);

            $class->setEventPersonal($eventPersonal);
            $class->setRate($rate);
            $class->setAgentComm($agentComm);
            $class->setUpline1($upline1);
            $class->setUpline2($upline2);
            $class->setPlName($plName);
            $class->setHosName($hosName);
            $class->setListerName($listerName);
            $class->setUlOverride($ulOverride);
            $class->setUulOverride($uulOverride);

            $class->setPlOverride($plOverride);
            $class->setHosOverride($hosOverride);
            $class->setListerOverride($listerOverride);
            $class->setAdmin1Override($admin1Override);
            $class->setAdmin2Override($admin2Override);
            $class->setAdmin3Override($admin3Override);
            $class->setGicProfit($gicProfit);
            $class->setTotalClaimDevAmt($totalClaimedDevAmt);
            $class->setTotalBalUnclaimAmt($totalBalUnclaimAmt);
            $class->setClaimInvoice1st($claimInvoiceNo1st);

            $class->setClaimAmt1st($claimAmt1st);
            $class->setClaimInvoice2nd($claimInvoiceNo2nd);
            $class->setClaimAmt2nd($claimAmt2nd);
            $class->setClaimInvoice3rd($claimInvoiceNo3rd);
            $class->setClaimAmt3rd($claimAmt3rd);
            $class->setClaimInvoice4th($claimInvoiceNo4th);
            $class->setClaimAmt4th($claimAmt4th);
            $class->setClaimInvoice5th($claimInvoiceNo5th);
            $class->setClaimAmt5th($claimAmt5th);
            $class->setClaimInvoice6th($claimInvoiceNo6th);

            $class->setClaimAmt6th($claimAmt6th);
            $class->setClaimInvoice7th($claimInvoiceNo7th);
            $class->setClaimAmt7th($claimAmt7th);
            $class->setClaimInvoice8th($claimInvoiceNo8th);
            $class->setClaimAmt8th($claimAmt8th);
            $class->setClaimInvoice9th($claimInvoiceNo9th);
            $class->setClaimAmt9th($claimAmt9th);
            $class->setClaimInvoice10th($claimInvoiceNo10th);
            $class->setClaimAmt10th($claimAmt10th);

            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
            $class->setLoanUid($loanUid);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
