<?php
class Product{
    //var $id, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice;
    //var $id, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package;

    // var $id, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package, 
    //         $rebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus;

    var $id, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package, 
        $rebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus, 
            $remark, $formCollected, $bankApproved, $loSignedDate, $laSignedDate;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUnitNo()
    {
        return $this->unitNo;
    }

    /**
     * @param mixed $unitNo
     */
    public function setUnitNo($unitNo)
    {
        $this->unitNo = $unitNo;
    }

    /**
     * @return mixed
     */
    public function getPurchaserName()
    {
        return $this->purchaserName;
    }

    /**
     * @param mixed $purchaserName
     */
    public function setPurchaserName($purchaserName)
    {
        $this->purchaserName = $purchaserName;
    }

    /**
     * @return mixed
     */
    public function getIc()
    {
        return $this->ic;
    }

    /**
     * @param mixed $ic
     */
    public function setIc($ic)
    {
        $this->ic = $ic;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getBookingDate()
    {
        return $this->bookingDate;
    }

    /**
     * @param mixed $bookingDate
     */
    public function setBookingDate($bookingDate)
    {
        $this->bookingDate = $bookingDate;
    }

    /**
     * @return mixed
     */
    public function getSqFt()
    {
        return $this->sqFt;
    }

    /**
     * @param mixed $sqFt
     */
    public function setSqFt($sqFt)
    {
        $this->sqFt = $sqFt;
    }

    /**
     * @return mixed
     */
    public function getSpaPrice()
    {
        return $this->spaPrice;
    }

    /**
     * @param mixed $spaPrice
     */
    public function setSpaPrice($spaPrice)
    {
        $this->spaPrice = $spaPrice;
    }

    /**
     * @return mixed
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * @param mixed $package
     */
    public function setPackage($package)
    {
        $this->package = $package;
    }

    /**
     * @return mixed
     */
    public function getRebate()
    {
        return $this->rebate;
    }

    /**
     * @param mixed $rebate
     */
    public function setRebate($rebate)
    {
        $this->rebate = $rebate;
    }

    /**
     * @return mixed
     */
    public function getNettPrice()
    {
        return $this->nettPrice;
    }

    /**
     * @param mixed $nettPrice
     */
    public function setNettPrice($nettPrice)
    {
        $this->nettPrice = $nettPrice;
    }

    /**
     * @return mixed
     */
    public function getTotalDeveloperComm()
    {
        return $this->totalDeveloperComm;
    }

    /**
     * @param mixed $totalDeveloperComm
     */
    public function setTotalDeveloperComm($totalDeveloperComm)
    {
        $this->totalDeveloperComm = $totalDeveloperComm;
    }

    /**
     * @return mixed
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param mixed $agent
     */
    public function setAgent($agent)
    {
        $this->agent = $agent;
    }

    /**
     * @return mixed
     */
    public function getLoanStatus()
    {
        return $this->loanStatus;
    }

    /**
     * @param mixed $loanStatus
     */
    public function setLoanStatus($loanStatus)
    {
        $this->loanStatus = $loanStatus;
    }

    /**
     * @return mixed
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param mixed $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return mixed
     */
    public function getFormCollected()
    {
        return $this->formCollected;
    }

    /**
     * @param mixed $formCollected
     */
    public function setFormCollected($formCollected)
    {
        $this->formCollected = $formCollected;
    }

    /**
     * @return mixed
     */
    public function getBankApproved()
    {
        return $this->bankApproved;
    }

    /**
     * @param mixed $bankApproved
     */
    public function setBankApproved($bankApproved)
    {
        $this->bankApproved = $bankApproved;
    }

    /**
     * @return mixed
     */
    public function getLoSignedDate()
    {
        return $this->loSignedDate;
    }

    /**
     * @param mixed $loSignedDate
     */
    public function setLoSignedDate($loSignedDate)
    {
        $this->loSignedDate = $loSignedDate;
    }

    /**
     * @return mixed
     */
    public function getLaSignedDate()
    {
        return $this->laSignedDate;
    }

    /**
     * @param mixed $laSignedDate
     */
    public function setLaSignedDate($laSignedDate)
    {
        $this->laSignedDate = $laSignedDate;
    }

}

function getProduct($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
// function getAdminProject($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    // $dbColumnNames = array("id","unit_no","purchaser_name","ic","contact","email","booking_date","sq_ft","spa_price");
    // $dbColumnNames = array("id","unit_no","purchaser_name","ic","contact","email","booking_date","sq_ft","spa_price","package");

    $dbColumnNames = array("id","unit_no","purchaser_name","ic","contact","email","booking_date","sq_ft","spa_price","package",
                        "rebate","nettprice","totaldevelopercomm","agent","loanstatus",
                            "remark","form_Collected","bank_approved","lo_signed_date","la_signed_date");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"admin_project");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($id, $name, $price, $type,$description,$dateCreated,$dateUpdated,$stock,$display,$images,$buyStock,$totalPriceBuy);
        
        // $stmt->bind_result($id, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package);

        // $stmt->bind_result($id, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package, 
        //                         $rebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus);

        $stmt->bind_result($id, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package, 
                                $rebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus, 
                                    $remark, $formCollected, $bankApproved, $loSignedDate, $laSignedDate);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Product();
            $class->setId($id);
            $class->setUnitNo($unitNo);
            $class->setPurchaserName($purchaserName);
            $class->setIc($ic);
            $class->setContact($contact);
            $class->setEmail($email);
            $class->setBookingDate($bookingDate);
            $class->setSqFt($sqFt);
            $class->setSpaPrice($spaPrice);
            $class->setPackage($package);

            $class->setRebate($rebate);
            $class->setNettPrice($nettPrice);
            $class->setTotalDeveloperComm($totalDeveloperComm);
            $class->setAgent($agent);
            $class->setLoanStatus($loanStatus);

            $class->setRemark($remark);
            $class->setFormCollected($formCollected);
            $class->setBankApproved($bankApproved);
            $class->setLoSignedDate($loSignedDate);
            $class->setLaSignedDate($laSignedDate);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
