    <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="left-logo-div float-left hidden-logo-padding">
                <a href="index.php"><img src="img/dck-white.png" class="logo-img" alt="DCK®" title="DCK®"></a>
            </div>
            
            <?php 
            if(isset($_SESSION['uid']))
            {
                // echo count($_SESSION);
                ?>

                <div class="middle-menu-div right-menu-div1">
                    <a href="adminDashboard.php" class="menu-padding white-to-yellow">Dashboard</a>
                    <a href="adminPayment.php" class="menu-padding white-to-yellow">Payment</a>
                    <a href="adminShipping.php" class="menu-padding white-to-yellow">Shipping</a>
                    <a href="adminWithdrawal.php" class="menu-padding white-to-yellow">Withdrawal</a>
                    <a href="adminSales.php" class="menu-padding white-to-yellow">Sales</a>
                    <a href="adminProduct.php" class="menu-padding white-to-yellow">Product</a>
                    <a href="adminPayout.php" class="menu-padding white-to-yellow">Payout</a>
                    <a href="adminMember.php" class="menu-padding white-to-yellow">Member</a>
                    <a href="adminAdmin.php" class="menu-padding white-to-yellow">Admin</a>

                    <div class="dropdown hover1">
                            <a  class="menu-padding">
                                <img src="img/settings.png" class="cart-img hover1a" alt="Settings" title="Settings">
                                <img src="img/settings-yellow.png" class="cart-img hover1b" alt="Settings" title="Settings">
                            </a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a> 
                            <div class="dropdown-content ywllo-dropdown-content">
                                <p class="dropdown-p"><a href="adminProfile.php"  class="menu-padding dropdown-a">Edit Profile</a></p>
                                <p class="dropdown-p"><a href="adminPassword.php"  class="menu-padding dropdown-a hide">Edit Password</a></p>
                                <p class="dropdown-p"><a href="adminRate.php" class="menu-padding dropdown-a">Rate</a></p>
                                <p class="dropdown-p"><a href="announcement.php" class="menu-padding dropdown-a">Announcement</a></p>
                                <p class="dropdown-p"><a href="adminReason.php" class="menu-padding dropdown-a">Reason</a></p>
                                <p class="dropdown-p"><a href="logout.php" class="menu-padding dropdown-a">Logout</a></p>
                            </div>  
                    </div>
                    <a class="open-menu-admin"><img src="img/white-menu.png" class="menu-icon"></a>                 
                </div>

                
                <?php
            }
            else 
            {
                ?>
                <div class="right-menu-div float-right">
                    <a href="faq.php" class="menu-padding white-to-yellow faq-padding">FAQ</a>
                    <a  class="menu-padding white-to-yellow div-div open-login">Login</a>
                    <a class="open-menu"><img src="img/white-menu.png" class="menu-icon"></a>
                </div>

                <?php
            }
            ?>

        </div>
    
    </header>