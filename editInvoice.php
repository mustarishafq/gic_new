
<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$invoiceHistory = getInvoice($conn, "WHERE loan_uid = ?", array("loan_uid"), array($_POST['loan_uid']), "s");
$bankName = getBankName($conn);
$loanDetails = getLoanStatus($conn, "WHERE loan_uid=?",array("loan_uid"),array($_POST['loan_uid']), "s");
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Edit Invoice | GIC" />
    <title>Edit Invoice | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

<h1 class="username">Invoice : <?php echo $loanDetails[0]->getPurchaserName() ?></h1>
<h3>Unclaimed Balanced : RM<?php echo $loanDetails[0]->getTotalBalUnclaimAmt()  ?> </h3>



<form  action="utilities/addNewInvoiceFunction.php" method="POST" enctype="multipart/form-data">
<!-- <table class="edit-profile-table"> -->
<div class="dual-input-div">
        <p>Project</p>
        <input required class="dual-input clean" type="text" placeholder="Project" id="product_price" name="project" >

    </div>

  <div class="tempo-two-input-clear"></div>
	<div class="dual-input-div">
            <p>Item</p>
            <input required class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item">
              <!-- <input required class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item" > -->
      </div>
      <div class="dual-input-div second-dual-input">
        <p>Amount (RM)</p>
          <input required class="dual-input clean" type="number" placeholder="Amount (RM)" id="product_name" name="amount" max="<?php echo $loanDetails[0]->getTotalBalUnclaimAmt() ?>">
      </div>
    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
              <p>Item (Optional)</p>
              <input  class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item2">
                <!-- <input required class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item" > -->
        </div>
        <div class="dual-input-div second-dual-input">
          <p>Amount (RM)</p>
            <input  class="dual-input clean" type="number" placeholder="Amount (RM)" id="product_name" name="amount2" max="<?php echo $loanDetails[0]->getTotalBalUnclaimAmt() ?>">
        </div>
      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div">
                <p>Item (Optional)</p>
                <input  class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item3">
                  <!-- <input required class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item" > -->
          </div>
          <div class="dual-input-div second-dual-input">
            <p>Amount (RM)</p>
              <input  class="dual-input clean" type="number" placeholder="Amount (RM)" id="product_name" name="amount3" max="<?php echo $loanDetails[0]->getTotalBalUnclaimAmt() ?>">
          </div>
        <div class="tempo-two-input-clear"></div>
        <div class="dual-input-div">
                  <p>Item (Optional)</p>
                  <input  class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item4">
                    <!-- <input required class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item" > -->
            </div>
            <div class="dual-input-div second-dual-input">
              <p>Amount (RM)</p>
                <input  class="dual-input clean" type="number" placeholder="Amount (RM)" id="product_name" name="amount4" max="<?php echo $loanDetails[0]->getTotalBalUnclaimAmt() ?>">
            </div>
          <div class="tempo-two-input-clear"></div>
          <div class="dual-input-div">
                    <p>Item (Optional)</p>
                    <input  class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item5">
                      <!-- <input required class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item" > -->
              </div>
              <div class="dual-input-div second-dual-input">
                <p>Amount (RM)</p>
                  <input  class="dual-input clean" type="number" placeholder="Amount (RM)" id="product_name" name="amount5" max="<?php echo $loanDetails[0]->getTotalBalUnclaimAmt() ?>">
              </div>
            <!-- <div class="tempo-two-input-clear"></div> -->

              <div class="tempo-two-input-clear"></div>

      <div class="dual-input-div">
              <p>Include Service Tax (6%)</p>
              <input required class="" type="radio" value = "YES" name="charges" >Yes
              <input required class="" type="radio" value = "NO" name="charges" >No

          </div>
      <!-- <div class="dual-input-div">
                <p>Bank Account Holder</p>
                  <input required class="dual-input clean" type="text" placeholder="Bank Account Holder" id="product_name" name="bank_account_holder" >
          </div> -->
        <!-- <div class="dual-input-div second-dual-input"> -->
                 <!-- <p>Bank Name</p> -->
                  <!-- <input required class="dual-input clean" type="text" placeholder="Bank Name" id="product_price" name="bank_name" > -->
                  <!-- <select class="dual-input clean" name="bank_name"> -->
                    <?php //for ($cnt=0; $cnt <count($bankName) ; $cnt++) {
                      ?><option value="<?php //echo $bankName[$cnt]->getBankName(); ?>"><?php //echo $bankName[$cnt]->getBankName();  ?></option> <?php
                    // }  ?>
                  <!-- </select> -->
        <!-- </div> -->
        <div class="tempo-two-input-clear"></div>
        <!-- <div class="dual-input-div">
                  <p>Bank Account No.</p>
                    <input required class="dual-input clean" type="text" placeholder="Bank Account No." id="product_name" name="bank_account_no" >
            </div> -->
          <div class="dual-input-div second-dual-input">
                   <!-- <p>Bank Name</p> -->
                    <!-- <input required class="dual-input clean" type="text" placeholder="Bank Name" id="product_price" name="bank_name" > -->
          </div>
          <div class="tempo-two-input-clear"></div>

<input type="hidden" name="purchaser_name" value="<?php echo $loanDetails[0]->getPurchaserName() ?>">
<input type="hidden" name="loan_uid" value="<?php echo $_POST['loan_uid'] ?>">


      <button input type="submit" name="upload" value="Upload" class="confirm-btn text-center white-text clean black-button">Confirm</button>

</form>

<br>
<?php if ($invoiceHistory) { ?>


<h2>Invoice History</h2>

<table class="shipping-table">
<tr>
    <th>No.</th>
    <th>Name</th>
    <th>Item</th>
    <th>Remark</th>
    <th>Amount</th>
    <th>Date Issue</th>
    <th>Invoice</th>
</tr>
<?php for ($cnt=0; $cnt <count($invoiceHistory) ; $cnt++) { ?>
<tr>
<?php
?>  <td class="td"><?php echo $cnt+1 ?></td>
    <td class="td"><?php echo $invoiceHistory[$cnt]->getPurchaserName() ?></td>
    <td class="td"><?php echo $invoiceHistory[$cnt]->getItem() ?></td>
    <td class="td"><?php echo $invoiceHistory[$cnt]->getRemark() ?></td>
    <td class="td"><?php echo $invoiceHistory[$cnt]->getAmount() ?></td>
    <td class="td"><?php echo date('d-m-Y', strtotime($invoiceHistory[$cnt]->getDateCreated())) ?></td>
    <td class="td">  <form action="invoice.php" method="POST">
          <button class="clean edit-anc-btn hover1" type="submit" name="invoice" value="<?php echo $invoiceHistory[$cnt]->getId();?>">
              <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product">
              <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product">
          </button>
      </form></td>
</tr>
<?php
 } ?>
</table>

<?php } ?>
</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Product Added Successfully";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "There is an error to add the new product";
        }

        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>
