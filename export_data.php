<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$output = '';
if(isset($_POST["export"]))
{
 $query = "SELECT * FROM loan_status";
 $result = mysqli_query($conn, $query);
 if(mysqli_num_rows($result) > 0)
 {
  $output .= '
   <table border = 1 >
                    <tr>
                    <tr>
										<th bgcolor= #C2C2C2 >NO.</th>
										<th bgcolor= #C2C2C2>UNIT NO.</th>
										<th bgcolor= #C2C2C2>NAME</th>
										<!-- <th>STOCK</th> -->
										<th bgcolor= #C2C2C2>IC</th>
										<th bgcolor= #C2C2C2>CONTACT</th>
										<th bgcolor= #C2C2C2>E-MAIL</th>
										<th bgcolor= #C2C2C2>BOOKING DATE</th>
										<th bgcolor= #C2C2C2>SQ FT</th>
										<th bgcolor= #C2C2C2>SPA PRICE</th>
										<th bgcolor= #C2C2C2>PACKAGE</th>
										<th bgcolor= #C2C2C2>DISCOUNT</th>
										<th bgcolor= #C2C2C2>REBATE</th>
										<th bgcolor= #C2C2C2>EXTRA REBATE</th>
										<th bgcolor= #C2C2C2>NETT PRICE</th>
										<th bgcolor= #C2C2C2>TOTAAL DEVELOPER COMMISSION</th>
										<th bgcolor= #C2C2C2>AGENT</th>
										<th bgcolor= #C2C2C2>LOAN STATUS</th>

										<th bgcolor= #C2C2C2>REMARK</th>
										<th bgcolor= #C2C2C2>B FORM COLLECTED</th>
										<th bgcolor= #C2C2C2>PAYMENT METHOD</th>
										<th bgcolor= #C2C2C2>LAWYER</th>
										<th bgcolor= #C2C2C2>PENDING APPROVAL STATUS</th>
										<th bgcolor= #C2C2C2>BANK APPROVED</th>
										<th bgcolor= #C2C2C2>LO SIGNED DATE</th>
										<th bgcolor= #C2C2C2>LA SIGNED DATE</th>
										<th bgcolor= #C2C2C2>SPA SIGNED DATE</th>
										<th bgcolor= #C2C2C2>FULLSET COMPLETED</th>
										<th bgcolor= #C2C2C2>CANCELLED BOOKING</th>
										<th bgcolor= #C2C2C2>CASE STATUS</th>
										<th bgcolor= #C2C2C2>EVENT/PERSONAL</th>
										<th bgcolor= #C2C2C2>RATE</th>
										<th bgcolor= #C2C2C2>AGENT COMMISSION</th>
										<th bgcolor= #C2C2C2>UPLINE</th>
										<th bgcolor= #C2C2C2>UP-UPLINE</th>
										<th bgcolor= #C2C2C2>PL NAME</th>
										<th bgcolor= #C2C2C2>HOS NAME</th>
										<th bgcolor= #C2C2C2>LISTER NAME</th>
										<th bgcolor= #C2C2C2>UL OVERRIDE</th>
										<th bgcolor= #C2C2C2>UUL OVERRIDE</th>
										<th bgcolor= #C2C2C2>PL OVERRIDE</th>
										<th bgcolor= #C2C2C2>HOS OVERRIDE</th>
										<th bgcolor= #C2C2C2>LISTER OVERRIDE</th>

										<th bgcolor= #C2C2C2>ADMIN1 OVERRIDE</th>
										<th bgcolor= #C2C2C2>ADMIN2 OVERRIDE</th>
										<th bgcolor= #C2C2C2>GIC PROFIT</th>
										<th bgcolor= #C2C2C2>TOTAL CLAIMED DEV AMT</th>
										<th bgcolor= #C2C2C2>TOTAL BAL UNCLAIMED DEV AMT</th>


                    </tr>
                    </tr>
  ';
  while($row = mysqli_fetch_array($result))
  {
   $output .= '
										    <tr>
												<td>'.$row["id"].'</td>
												<td>'.$row["unit_no"].'</td>
												<td>'.$row["purchaser_name"].'</td>
												<td>'.$row["ic"].'</td>
												<td>'.$row["contact"].'</td>
												<td>'.$row["email"].'</td>
												<td>'.$row["booking_date"].'</td>
												<td>'.$row["sq_ft"].'</td>
												<td>'.$row["spa_price"].'</td>
												<td>'.$row["package"].'</td>
												<td>'.$row["discount"].'</td>
												<td>'.$row["rebate"].'</td>
												<td>'.$row["extra_rebate"].'</td>
												<td>'.$row["nettprice"].'</td>
												<td>'.$row["totaldevelopercomm"].'</td>
												<td>'.$row["agent"].'</td>
												<td>'.$row["loanstatus"].'</td>
												<td>'.$row["remark"].'</td>
												<td>'.$row["bform_Collected"].'</td>
												<td>'.$row["payment_method"].'</td>
												<td>'.$row["lawyer"].'</td>
												<td>'.$row["pending_approval_status"].'</td>
												<td>'.$row["bank_approved"].'</td>
												<td>'.$row["lo_signed_date"].'</td>
												<td>'.$row["la_signed_date"].'</td>
												<td>'.$row["spa_signed_date"].'</td>
												<td>'.$row["fullset_completed"].'</td>
												<td>'.$row["cancelled_booking"].'</td>
												<td>'.$row["case_status"].'</td>
												<td>'.$row["event_personal"].'</td>
												<td>'.$row["rate"].'</td>
												<td>'.$row["agent_comm"].'</td>
												<td>'.$row["upline1"].'</td>
												<td>'.$row["upline2"].'</td>
												<td>'.$row["pl_name"].'</td>
												<td>'.$row["hos_name"].'</td>
												<td>'.$row["lister_name"].'</td>
												<td>'.$row["ul_override"].'</td>
												<td>'.$row["uul_override"].'</td>
												<td>'.$row["pl_override"].'</td>
												<td>'.$row["hos_override"].'</td>
												<td>'.$row["lister_override"].'</td>
												<td>'.$row["admin1_override"].'</td>
												<td>'.$row["admin2_override"].'</td>
												<td>'.$row["gic_profit"].'</td>
												<td>'.$row["total_claimed_dev_amt"].'</td>
												<td>'.$row["total_bal_unclaim_amt"].'</td>


                    </tr>
   ';
  }
	$date = date('d-m-Y');
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=GIC Loan Status '.$date.'.xls');
  echo $output;
 }
}
?>
