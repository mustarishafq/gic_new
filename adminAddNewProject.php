<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess1.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE id = ? ",array("id"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Add Project | GIC" />
    <title>Add Project | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'admin1Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Add New Project</h1>
    <div class="short-red-border"></div>

    <form method="POST" action="utilities/addProjectFunction.php">
        <label class="labelSize">Project Name :</label>
        <input oninput="this.value = this.value.toUpperCase()" class="inputSize input-pattern" type="text"  placeholder="Project Name" name="project_name" id="project_name"><br>

        <!-- <label class="labelSize">Person Incharge:</label>
        <input oninput="this.value = this.value.toUpperCase()" class="inputSize input-pattern" type="text" placeholder="Person Incharge" name="add_by" id="add_by"><br> -->

        <label class="labelSize">No. of Claim Stage</label>
        <input class="inputSize input-pattern" type="number" placeholder="numbers of claims stages" name="claims_times" id="claims_times"><br>

        <input type="hidden" name="add_by" id="add_by" value="<?php echo $userDetails->getUsername(); ?>">

        <button class="button" type="submit" name="loginButton">Add Project</button><br>
    </form>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
</body>
</html>
