<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$sql = "SELECT * FROM loan_status";
$result = mysqli_query($conn, $sql);
?>

<html>
 <head>
  <title>Export MySQL data to Excel in PHP</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
 </head>
 <body>
  <div class="container">
   <br />
   <br />
   <br />
   <div class="table-responsive">
    <h2 align="center">Export MySQL data to Excel in PHP</h2><br />
    <table class="table table-bordered">
     <tr>
                         <th>Name</th>
                         <th>Unit No.</th>
												 <th>IC</th>
												 <th>Contact No.</th>
												 <th>E-mail</th>
                         <!-- <th>City</th> -->

                    </tr>
     <?php
     while($row = mysqli_fetch_array($result))
     {
        echo '
       <tr>
         <td>'.$row["purchaser_name"].'</td>
         <td>'.$row["unit_no"].'</td>
         <td>'.$row["ic"].'</td>
         <td>'.$row["contact"].'</td>
         <td>'.$row["email"].'</td>
       </tr>
        ';
     }
     ?>
    </table>
    <br />
    <form method="post" action="export_data.php">
     <input type="submit" name="export" class="btn btn-success" value="Export" />
    </form>
   </div>
  </div>
 </body>
</html>
