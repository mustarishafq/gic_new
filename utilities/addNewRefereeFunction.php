
<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

$uid = $_SESSION['uid'];

function registerNewUser($conn,$username,$fullname,$uid,$email,$countryId,$phoneNo,$finalPassword,$salt,$icNo,$referrerUid = null,$topReferrerUid = null,$referralName = null,$currentLevel = null)
{
    $isReferred = 0;
    if($referrerUid && $topReferrerUid)
    {
        $isReferred = 1;
    }

    if(insertDynamicData($conn,"user",array("uid","username","full_name","email","password","salt","phone_no","ic_no","country_id","is_referred"),
    array($uid,$username,$fullname,$email,$finalPassword,$salt,$phoneNo,$icNo,$countryId,$isReferred),"ssssssssii") === null)
    {
         header('Location: ../addReferee.php?promptError=1');
    }
    else
    {
          if($isReferred === 1)
               {
               if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
               array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
                    {
                    header('Location: ../addReferee.php?promptError=2');
                    }
               }
    }
    return true;
}

function registerNewUserWithoutEmail($conn,$username,$fullname,$uid,$countryId,$phoneNo,$finalPassword,$salt,$icNo,$referrerUid = null,$topReferrerUid = null,$referralName = null,$currentLevel = null)
{
     $isReferred = 0;
     if($referrerUid && $topReferrerUid)
          {
          $isReferred = 1;
          }

     if(insertDynamicData($conn,"user",array("uid","username","full_name","password","salt","phone_no","ic_no","country_id","is_referred"),
     array($uid,$username,$fullname,$finalPassword,$salt,$phoneNo,$icNo,$countryId,$isReferred),"sssssssii") === null)
     {
     header('Location: ../addReferee.php?promptError=1');
     }
     else
     {
          if($isReferred === 1)
          {
          if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
          array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
               {
               header('Location: ../addReferee.php?promptError=2');
               }
          }
     }
     return true;
}

 function sendEmailForVerification($uid)
 {
     $conn = connDB();
     $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     $verifyUser_debugMode = 0;
     $verifyUser_host = "mail.dcksupreme.asia";
     $verifyUser_usernameThatSendEmail = "noreply@dcksupreme.asia";                   // Sender Acc Username
     $verifyUser_password = "~sh~1z+kL=;C";                                                      // Sender Acc Password
     $verifyUser_smtpSecure = "ssl";                                                           // SMTP type
     $verifyUser_port = 465;                                                                   // SMTP port no
     $verifyUser_sentFromThisEmailName = "noreply@dcksupreme.asia";                                       // Sender Username
     $verifyUser_sentFromThisEmail = "noreply@dcksupreme.asia";                       // Sender Email
     $verifyUser_sendToThisEmailName = $userRows[0]->getUsername();                                      // Recipient Username
     $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                                   // Recipient Email
     $verifyUser_isHtml = true;                                                                // Set To Html
     $verifyUser_subject = "Confirm Your Registration! ";                                      // Title

          $verifyUser_body = "<p>CONGRATULATIONS !! your registration on DCK Supreme was successfully.</p>"; // Body
          // $verifyUser_body = "<p>Username : ".$$userRows->getUsername()."</p>"; // Body

          // $verifyUser_body = "<p>Please confirm your registration of DCK Supreme by clicking the "; // Body
          // $verifyUser_body .="<a href='http://www.dcksupreme.asia/email-verified.php?getVerified=".$uid."'>link</a>";
          // $verifyUser_body .=" below. </p>";
          // $verifyUser_altBody = "Test Email 2";                                                    // Body

     sendMailTo(
          $verifyUser_debugMode,
          $verifyUser_host,
          $verifyUser_usernameThatSendEmail,
          $verifyUser_password,
          $verifyUser_smtpSecure,
          $verifyUser_port,
          $verifyUser_sentFromThisEmailName,
          $verifyUser_sentFromThisEmail,
          $verifyUser_sendToThisEmailName,
          $verifyUser_sendToThisEmail,
          $verifyUser_isHtml,
          $verifyUser_subject,
          $verifyUser_body,
          null
     );
 }

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $register_uid = md5(uniqid());
     $register_username = rewrite($_POST['register_username']);
     $register_fullname = rewrite($_POST['register_fullname']);
     $register_ic_no = rewrite($_POST['register_ic_no']);

     $register_email_user = null;
     if(isset($_POST['register_email_user']))
     {
          $register_email_user = rewrite($_POST['register_email_user']);
          $register_email_user = filter_var($register_email_user, FILTER_SANITIZE_EMAIL);
     }

     $register_password = $_POST['register_password'];
     $register_password_validation = strlen($register_password);
     $register_retype_password = $_POST['register_retype_password'];

     $register_username_referrer = $_POST['register_username_referrer'];
     $register_product = $_POST['register_product'];

     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     //   FOR DEBUGGING
     //     echo "<br>";
     //     echo $register_uid."<br>";
     //     echo $register_username."<br>";
     //     echo $register_email_user."<br>";
     //     echo $register_password."<br>";
     //     echo $register_retype_password."<br>";
     //     echo $password."<br>";
     //     echo $salt."<br>";
     //     echo $finalPassword."<br>";
     //     echo $register_username_referrer."<br>";
     //     echo $register_product."<br>";

     if(filter_var($register_email_user, FILTER_VALIDATE_EMAIL))
     {
          if($register_password == $register_retype_password)
          {
               if($register_password_validation >= 6)
               {
                    if($register_username_referrer)
                    {
                         $referrerUserRows = getUser($conn," WHERE username = ? ",array("username"),array($register_username_referrer),"s");

                         if($referrerUserRows)
                         {
                              $referrerUid = $referrerUserRows[0]->getUid();
                              $referrerName = $referrerUserRows[0]->getUsername();
                              $referralUid = $referrerUserRows[0]->getUid();
                              $referralName = $register_username;
                              $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
                              $currentLevel = 1;
                              $getUplineCurrentLevel = 1;

                              $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");
                              if($referralHistoryRows)
                              {
                                   $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                                   $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
                              }
                              $referralNewestRows = getReferralHistory($conn,"WHERE referral_name = ?", array("referral_name"),array($register_username_referrer), "s");
                              if($referralNewestRows)
                              {
                              //$topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                              $getUplineCurrentLevel = $referralNewestRows[0]->getCurrentLevel() + 1;
                              }

                              $register_email_user = rewrite($_POST['register_email_user']);

                              $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
                              $usernameDetails = $usernameRows[0];

                              $fullnameRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($_POST['register_fullname']),"s");
                              $fullnameDetails = $fullnameRows[0];

                              $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email_user']),"s");
                              $userEmailDetails = $userEmailRows[0];

                              $icNoRows = getUser($conn," WHERE ic_no = ? ",array("ic_no"),array($_POST['register_ic_no']),"i");
                              $icNoDetails = $icNoRows[0];

                              $userPointRows = getUser($conn," WHERE username = ? ",array("username"),array($register_username_referrer),"s");
                              $userPointDetails = $userPointRows[0];


                              // if (!$usernameDetails && !$userEmailDetails && !$fullnameDetails && !$icNoDetails)
                              if (!$usernameDetails && !$fullnameDetails && !$icNoDetails)
                              {
                                   $userPointReferee = $userPointDetails->getUserPoint();
                                   $dckUid = $userPointDetails->getUid();
                                   //$DCK = $userPointDetails->getUsername();


                                  if ($userPointReferee >= 0)
                                   {
                                     if ($register_username_referrer == "DCK"){
                                       $currentLevel = 1;
                                       $getUplineCurrentLevel = 1;
                                       $topReferrerUid = $dckUid;
                                     }
                                     if ($register_username_referrer == "dck"){
                                       $currentLevel = 1;
                                       $getUplineCurrentLevel = 1;
                                       $topReferrerUid = $dckUid;
                                     }
                                        if(registerNewUser($conn,$register_username,$register_fullname,$register_uid,$register_email_user,null,null,$finalPassword,$salt,$register_ic_no,$referrerUid,$topReferrerUid,$referralName,$currentLevel))
                                        {
                                          $userRows = getUser($conn," WHERE username = ? ",array("username"),array($register_username_referrer),"s");
                                          $userDetails = $userRows[0];

                                          $userPointRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
                                          $userPointDetails = $userPointRows[0];

                                          $referralRows = getUser($conn," WHERE username = ? ",array("username"),array($referralName),"s");
                                          $referralDetails = $referralRows[0];

                                          $referralNewRows = getReferralHistory($conn,"WHERE referral_name = ?", array("referral_name"),array($register_username_referrer), "s");
                                          $referralNewDetails = $referralNewRows[0];

                                          $referrerId = $referralNewDetails -> getReferrerId();//referrer_id

                                          $getUplineRows = getUser($conn, "WHERE uid = ? ", array("uid"), array($referrerId), "s");
                                          $getUplineDetails = $getUplineRows[0];

                                          $getUpline = $getUplineDetails -> getRegisterDownlineNo();
                                          $getUplineName = $getUplineDetails -> getUsername();

                                          $pointReferee = $userPointDetails->getUserPoint();//based on uid
                                          $downlineNumber = $userDetails->getRegisterDownlineNo();
                                          $cash = $userDetails->getWithdrawAmount();
                                          $referralUid = $referralDetails->getUid();
                                          $getUid = $userDetails -> getUid();

                                          // if ($downlineNumber = 5) {
                                          //   $title = "Agent";
                                          //   date_default_timezone_set("Asia/Kuala_Lumpur");
                                          //   $dateMembership =date("Y-m-d h:i:s");
                                          //
                                          //   // if (memberShip($conn,$referrerUid,$referrerName,$referralName,$title)) {
                                          //   //
                                          //   // }
                                          //
                                          //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                          //   if(!$user)
                                          //   {
                                          //        $tableName = array();
                                          //        $tableValue =  array();
                                          //        $stringType =  "";
                                          //        //echo "save to database";
                                          //        if($referralName)
                                          //        {
                                          //             array_push($tableName,"latest_referral");
                                          //             array_push($tableValue,$referralName);
                                          //             $stringType .=  "s";
                                          //        }
                                          //        if ($title) {
                                          //
                                          //          array_push($tableName,"title");
                                          //          array_push($tableValue,$title);
                                          //          $stringType .=  "s";
                                          //        }
                                          //        if ($dateMembership) {
                                          //
                                          //          array_push($tableName,"date_membership");
                                          //          array_push($tableValue,$dateMembership);
                                          //          $stringType .=  "s";
                                          //        }
                                          //
                                          //
                                          //        array_push($tableValue,$register_username_referrer);
                                          //        $stringType .=  "s";
                                          //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                          //        if($passwordUpdated)
                                          //        { }
                                          //        else
                                          //        { }
                                          //   }
                                          //
                                          // }elseif ($downlineNumber > 5) {
                                          //   $title = "Agent";
                                          //   date_default_timezone_set("Asia/Kuala_Lumpur");
                                          //
                                          //
                                          //   // if (memberShip($conn,$referrerUid,$referrerName,$referralName,$title)) {
                                          //   //
                                          //   // }
                                          //
                                          //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                          //   if(!$user)
                                          //   {
                                          //        $tableName = array();
                                          //        $tableValue =  array();
                                          //        $stringType =  "";
                                          //        //echo "save to database";
                                          //        if($referralName)
                                          //        {
                                          //             array_push($tableName,"latest_referral");
                                          //             array_push($tableValue,$referralName);
                                          //             $stringType .=  "s";
                                          //        }
                                          //        if ($title) {
                                          //
                                          //          array_push($tableName,"title");
                                          //          array_push($tableValue,$title);
                                          //          $stringType .=  "s";
                                          //        }
                                          //
                                          //        array_push($tableValue,$register_username_referrer);
                                          //        $stringType .=  "s";
                                          //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                          //        if($passwordUpdated)
                                          //        { }
                                          //        else
                                          //        { }
                                          //   }
                                          //
                                          // }elseif ($downlineNumber = 0) {
                                          //   $title = "Member";
                                          //   date_default_timezone_set("Asia/Kuala_Lumpur");
                                          //   $dateMembership =date("Y-m-d h:i:s");
                                          //
                                          //   // if (memberShip($conn,$referrerUid,$referrerName,$referralName,$title)) {
                                          //   //
                                          //   // }
                                          //
                                          //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                          //   if(!$user)
                                          //   {
                                          //        $tableName = array();
                                          //        $tableValue =  array();
                                          //        $stringType =  "";
                                          //        //echo "save to database";
                                          //        if($referralName)
                                          //        {
                                          //             array_push($tableName,"latest_referral");
                                          //             array_push($tableValue,$referralName);
                                          //             $stringType .=  "s";
                                          //        }
                                          //        if ($title) {
                                          //
                                          //          array_push($tableName,"title");
                                          //          array_push($tableValue,$title);
                                          //          $stringType .=  "s";
                                          //        }
                                          //        if ($dateMembership) {
                                          //
                                          //          array_push($tableName,"date_membership");
                                          //          array_push($tableValue,$dateMembership);
                                          //          $stringType .=  "s";
                                          //        }
                                          //
                                          //
                                          //        array_push($tableValue,$register_username_referrer);
                                          //        $stringType .=  "s";
                                          //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                          //        if($passwordUpdated)
                                          //        { }
                                          //        else
                                          //        { }
                                          //   }
                                          //
                                          // }elseif ($downlineNumber < 5) {
                                          //   $title = "Member";
                                          //   // $dateMembership = date("Y-m-d");
                                          //
                                          //   // if (memberShip($conn,$referrerUid,$referrerName,$referralName,$title)) {
                                          //   //
                                          //   // }
                                          //
                                          //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                          //   if(!$user)
                                          //   {
                                          //        $tableName = array();
                                          //        $tableValue =  array();
                                          //        $stringType =  "";
                                          //        //echo "save to database";
                                          //        if($referralName)
                                          //        {
                                          //             array_push($tableName,"latest_referral");
                                          //             array_push($tableValue,$referralName);
                                          //             $stringType .=  "s";
                                          //        }
                                          //        if ($title) {
                                          //
                                          //          array_push($tableName,"title");
                                          //          array_push($tableValue,$title);
                                          //          $stringType .=  "s";
                                          //        }
                                          //
                                          //        array_push($tableValue,$register_username_referrer);
                                          //        $stringType .=  "s";
                                          //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                          //        if($passwordUpdated)
                                          //        { }
                                          //        else
                                          //        { }
                                          //   }
                                          //
                                          // }


                                             if($downlineNumber < 2 && $getUpline <= 5)//downline for the referer that been input
                                             {
                                                  $bonus = 150;
                                                  $uplineBonus = 0;
                                                  $currentBonus = $userDetails -> getBonus();
                                                  $totalBonus = $currentBonus + $bonus;
                                                  $registerDownline = $userDetails->getRegisterDownlineNo() + 1;
                                                  $totalPoint = $pointReferee -300;
                                                  $totalCash = $cash + $bonus;

                                                  if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer))
                                                  {
                                                       // $_SESSION['messageType'] = 1;
                                                       // header('Location: ../wallet.php?type=1');
                                                       $product = $register_product;
                                                       // if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$product))
                                                       $referralFullname = $register_fullname;
                                                       if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product))
                                                       {
                                                            // echo "success";
                                                       }
                                                       else
                                                       {
                                                            // echo "fail";
                                                       }
                                                  }
                                                  else
                                                  {
                                                       // $_SESSION['messageType'] = 2;
                                                       // header('Location: ../wallet.php?type=1');
                                                  }

                                                  $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                  if(!$user)
                                                  {
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";
                                                       //echo "save to database";
                                                       if($totalCash)
                                                       {
                                                            array_push($tableName,"final_amount");
                                                            array_push($tableValue,$totalCash);
                                                            $stringType .=  "i";
                                                       }
                                                       if($totalBonus)
                                                       {
                                                            array_push($tableName,"bonus");
                                                            array_push($tableValue,$totalBonus);
                                                            $stringType .=  "i";
                                                       }
                                                       if($registerDownline)
                                                       {
                                                            array_push($tableName,"register_downline_no");
                                                            array_push($tableValue,$registerDownline);
                                                            $stringType .=  "i";
                                                       }

                                                       array_push($tableValue,$register_username_referrer);
                                                       $stringType .=  "s";
                                                       $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                       if($passwordUpdated)
                                                       { }
                                                       else
                                                       { }
                                                  }
                                                  if(!$user)
                                                  {
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";

                                                       if($totalPoint)
                                                       {
                                                            array_push($tableName,"point");
                                                            array_push($tableValue,$totalPoint);
                                                            $stringType .=  "i";
                                                       }
                                                       if($totalPoint == 0)
                                                       {  $point = 0;
                                                            array_push($tableName,"point");
                                                            array_push($tableValue,$point);
                                                            $stringType .=  "i";
                                                       }

                                                       array_push($tableValue,$uid);
                                                       $stringType .=  "s";
                                                       $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                       if($passwordUpdated)
                                                       {    }
                                                       else
                                                       {    }
                                                  }
                                                    $currentlyBonus = $getUplineDetails -> getBonus();
                                                    $uplineAmount = $getUplineDetails -> getWithdrawAmount();
                                                    $totalUplinemount = $uplineAmount + $uplineBonus;
                                                    $totallyBonus = $currentlyBonus + $uplineBonus;

                                                    $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                    if(!$user)
                                                    {
                                                        $tableName = array();
                                                        $tableValue =  array();
                                                        $stringType =  "";
                                                        if($totallyBonus)
                                                        {
                                                            array_push($tableName,"bonus");
                                                            array_push($tableValue,$totallyBonus);
                                                            $stringType .=  "i";
                                                         }
                                                         if($totallyBonus)
                                                         {
                                                             array_push($tableName,"final_amount");
                                                             array_push($tableValue,$totalUplinemount);
                                                             $stringType .=  "i";
                                                          }

                                                        array_push($tableValue,$getUplineName);
                                                        $stringType .=  "s";
                                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                        if($passwordUpdated)
                                                        {
                                                            // echo "success";
                                                            // $_SESSION['messageType'] = 1;
                                                            // header('Location: ../wallet.php?type=1');
                                                            // header('Location: ../editprofile.php?type=3');
                                                        }
                                                        else
                                                        {
                                                            // echo "fail";
                                                            // $_SESSION['messageType'] = 3;
                                                            // header('Location: ../wallet.php?type=3');
                                                        }
                                                      }
                                                      if (commission($conn,$referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$register_username_referrer))
                                                      {


                                                      }

                                                      $_SESSION['messageType'] = 1;
                                                      header('Location: ../addReferee.php?type=1');

                                             }elseif($downlineNumber < 2 && $getUpline > 5)//downline for the referer that been input
                                             {
                                                  $bonus = 150;
                                                  // $uplineBonus = 30;
                                                  $uplineBonus = 0;
                                                  $currentBonus = $userDetails -> getBonus();
                                                  $totalBonus = $currentBonus + $bonus;
                                                  $registerDownline = $userDetails->getRegisterDownlineNo() + 1;
                                                  $totalPoint = $pointReferee -300;
                                                  $totalCash = $cash + $bonus;

                                                  if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer))
                                                  {
                                                       // $_SESSION['messageType'] = 1;
                                                       // header('Location: ../wallet.php?type=1');
                                                       $product = $register_product;
                                                       // if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$product))
                                                       $referralFullname = $register_fullname;
                                                       if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product))
                                                       {
                                                            // echo "success";
                                                       }
                                                       else
                                                       {
                                                            // echo "fail";
                                                       }
                                                  }
                                                  else
                                                  {
                                                       // $_SESSION['messageType'] = 2;
                                                       // header('Location: ../wallet.php?type=1');
                                                  }

                                                  $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                  if(!$user)
                                                  {
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";
                                                       //echo "save to database";
                                                       if($totalCash)
                                                       {
                                                            array_push($tableName,"final_amount");
                                                            array_push($tableValue,$totalCash);
                                                            $stringType .=  "i";
                                                       }
                                                       if($totalBonus)
                                                       {
                                                            array_push($tableName,"bonus");
                                                            array_push($tableValue,$totalBonus);
                                                            $stringType .=  "i";
                                                       }
                                                       if($registerDownline)
                                                       {
                                                            array_push($tableName,"register_downline_no");
                                                            array_push($tableValue,$registerDownline);
                                                            $stringType .=  "i";
                                                       }

                                                       array_push($tableValue,$register_username_referrer);
                                                       $stringType .=  "s";
                                                       $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                       if($passwordUpdated)
                                                       { }
                                                       else
                                                       { }
                                                  }
                                                  if(!$user)
                                                  {
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";

                                                       if($totalPoint)
                                                       {
                                                            array_push($tableName,"point");
                                                            array_push($tableValue,$totalPoint);
                                                            $stringType .=  "i";
                                                       }
                                                       if($totalPoint == 0)
                                                       {  $point = 0;
                                                            array_push($tableName,"point");
                                                            array_push($tableValue,$point);
                                                            $stringType .=  "i";
                                                       }

                                                       array_push($tableValue,$uid);
                                                       $stringType .=  "s";
                                                       $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                       if($passwordUpdated)
                                                       {    }
                                                       else
                                                       {    }
                                                  }
                                                  $currentlyBonus = $getUplineDetails -> getBonus();
                                                  $uplineAmount = $getUplineDetails -> getWithdrawAmount();
                                                  $totalUplinemount = $uplineAmount + $uplineBonus;
                                                  $totallyBonus = $currentlyBonus + $uplineBonus;

                                                  $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                  if(!$user)
                                                  {
                                                      $tableName = array();
                                                      $tableValue =  array();
                                                      $stringType =  "";
                                                      if($totallyBonus)
                                                      {
                                                          array_push($tableName,"bonus");
                                                          array_push($tableValue,$totallyBonus);
                                                          $stringType .=  "i";
                                                       }
                                                       if($totallyBonus)
                                                       {
                                                           array_push($tableName,"final_amount");
                                                           array_push($tableValue,$totalUplinemount);
                                                           $stringType .=  "i";
                                                        }
                                                        array_push($tableValue,$getUplineName);
                                                        $stringType .=  "s";
                                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                        if($passwordUpdated)
                                                        {
                                                            // echo "success";
                                                            // $_SESSION['messageType'] = 1;
                                                            // header('Location: ../wallet.php?type=1');
                                                            // header('Location: ../editprofile.php?type=3');
                                                        }
                                                        else
                                                        {
                                                            // echo "fail";
                                                            // $_SESSION['messageType'] = 3;
                                                            // header('Location: ../wallet.php?type=3');
                                                        }
                                                      }
                                                      if (commission($conn,$referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$register_username_referrer))
                                                      {


                                                      }


                                                    }elseif($downlineNumber > 5 && $getUpline <=5)//downline for the referer that been input
                                                    {
                                                         $bonus = 80;
                                                         $uplineBonus = 0;
                                                         $currentBonus = $userDetails -> getBonus();
                                                         $totalBonus = $currentBonus + $bonus;
                                                         $registerDownline = $userDetails->getRegisterDownlineNo() + 1;
                                                         $totalPoint = $pointReferee -300;
                                                         $totalCash = $cash + $bonus;
                                                         $currentStatus = "Agent";

                                                         if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer))
                                                         {
                                                            // $_SESSION['messageType'] = 1;
                                                            // header('Location: ../wallet.php?type=1');
                                                            $product = $register_product;
                                                            //   if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$product))
                                                            $referralFullname = $register_fullname;
                                                            if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product))
                                                              {
                                                                   // echo "success";
                                                              }
                                                              else
                                                              {
                                                                   // echo "fail";
                                                              }
                                                         }
                                                         else
                                                         {
                                                              // $_SESSION['messageType'] = 2;
                                                              // header('Location: ../wallet.php?type=1');
                                                         }

                                                         $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                         if(!$user)
                                                         {
                                                              $currentStatus = "Agent";
                                                              $tableName = array();
                                                              $tableValue =  array();
                                                              $stringType =  "";
                                                              //echo "save to database";
                                                              if($totalCash)
                                                              {
                                                                   array_push($tableName,"final_amount");
                                                                   array_push($tableValue,$totalCash);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($totalBonus)
                                                              {
                                                                   array_push($tableName,"bonus");
                                                                   array_push($tableValue,$totalBonus);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($registerDownline)
                                                              {
                                                                   array_push($tableName,"register_downline_no");
                                                                   array_push($tableValue,$registerDownline);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($currentStatus)
                                                              {
                                                                   array_push($tableName,"current_status");
                                                                   array_push($tableValue,$currentStatus);
                                                                   $stringType .=  "s";
                                                              }

                                                              array_push($tableValue,$register_username_referrer);
                                                              $stringType .=  "s";
                                                              $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                              if($passwordUpdated)
                                                              { }
                                                              else
                                                              { }
                                                         }
                                                         if(!$user)
                                                         {
                                                              $tableName = array();
                                                              $tableValue =  array();
                                                              $stringType =  "";

                                                              if($totalPoint)
                                                              {
                                                                   array_push($tableName,"point");
                                                                   array_push($tableValue,$totalPoint);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($totalPoint == 0)
                                                              {  $point = 0;
                                                                   array_push($tableName,"point");
                                                                   array_push($tableValue,$point);
                                                                   $stringType .=  "i";
                                                              }

                                                              array_push($tableValue,$uid);
                                                              $stringType .=  "s";
                                                              $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                              if($passwordUpdated)
                                                              {    }
                                                              else
                                                              {    }
                                                         }
                                                         $currentlyBonus = $getUplineDetails -> getBonus();
                                                         $uplineAmount = $getUplineDetails -> getWithdrawAmount();
                                                         $totalUplinemount = $uplineAmount + $uplineBonus;
                                                         $totallyBonus = $currentlyBonus + $uplineBonus;

                                                         $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                         if(!$user)
                                                         {
                                                             $tableName = array();
                                                             $tableValue =  array();
                                                             $stringType =  "";
                                                             if($totallyBonus)
                                                             {
                                                                 array_push($tableName,"bonus");
                                                                 array_push($tableValue,$totallyBonus);
                                                                 $stringType .=  "i";
                                                              }
                                                              if($totallyBonus)
                                                              {
                                                                  array_push($tableName,"final_amount");
                                                                  array_push($tableValue,$totalUplinemount);
                                                                  $stringType .=  "i";
                                                               }
                                                               array_push($tableValue,$getUplineName);
                                                               $stringType .=  "s";
                                                               $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                               if($passwordUpdated)
                                                               {
                                                                   // echo "success";
                                                                   // $_SESSION['messageType'] = 1;
                                                                   // header('Location: ../wallet.php?type=1');
                                                                   // header('Location: ../editprofile.php?type=3');
                                                               }
                                                               else
                                                               {
                                                                   // echo "fail";
                                                                   // $_SESSION['messageType'] = 3;
                                                                   // header('Location: ../wallet.php?type=3');
                                                               }
                                                             }
                                                             if (commission($conn,$referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$register_username_referrer))
                                                             {


                                                             }


                                                    }elseif($downlineNumber > 5 && $getUpline > 5)//downline for the referer that been input
                                                    {
                                                         $bonus = 80;
                                                         // $uplineBonus = 10;
                                                         $uplineBonus = 0;
                                                         $currentBonus = $userDetails -> getBonus();
                                                         $totalBonus = $currentBonus + $bonus;
                                                         $registerDownline = $userDetails->getRegisterDownlineNo() + 1;
                                                         $totalPoint = $pointReferee -300;
                                                         $totalCash = $cash + $bonus;
                                                         $currentStatus = "Agent";

                                                         if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer))
                                                         {
                                                              // $_SESSION['messageType'] = 1;
                                                              // header('Location: ../wallet.php?type=1');
                                                            $product = $register_product;
                                                            //   if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$product))
                                                            $referralFullname = $register_fullname;
                                                            if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product))
                                                              {
                                                                   // echo "success";
                                                              }
                                                              else
                                                              {
                                                                   // echo "fail";
                                                              }
                                                         }
                                                         else
                                                         {
                                                              // $_SESSION['messageType'] = 2;
                                                              // header('Location: ../wallet.php?type=1');
                                                         }

                                                         $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                         if(!$user)
                                                         {
                                                              $currentStatus = "Agent";
                                                              $tableName = array();
                                                              $tableValue =  array();
                                                              $stringType =  "";
                                                              //echo "save to database";
                                                              if($totalCash)
                                                              {
                                                                   array_push($tableName,"final_amount");
                                                                   array_push($tableValue,$totalCash);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($totalBonus)
                                                              {
                                                                   array_push($tableName,"bonus");
                                                                   array_push($tableValue,$totalBonus);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($registerDownline)
                                                              {
                                                                   array_push($tableName,"register_downline_no");
                                                                   array_push($tableValue,$registerDownline);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($currentStatus)
                                                              {
                                                                   array_push($tableName,"current_status");
                                                                   array_push($tableValue,$currentStatus);
                                                                   $stringType .=  "s";
                                                              }

                                                              array_push($tableValue,$register_username_referrer);
                                                              $stringType .=  "s";
                                                              $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                              if($passwordUpdated)
                                                              { }
                                                              else
                                                              { }
                                                         }
                                                         if(!$user)
                                                         {
                                                              $tableName = array();
                                                              $tableValue =  array();
                                                              $stringType =  "";

                                                              if($totalPoint)
                                                              {
                                                                   array_push($tableName,"point");
                                                                   array_push($tableValue,$totalPoint);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($totalPoint == 0)
                                                              {  $point = 0;
                                                                   array_push($tableName,"point");
                                                                   array_push($tableValue,$point);
                                                                   $stringType .=  "i";
                                                              }

                                                              array_push($tableValue,$uid);
                                                              $stringType .=  "s";
                                                              $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                              if($passwordUpdated)
                                                              {    }
                                                              else
                                                              {    }
                                                         }
                                                         $currentlyBonus = $getUplineDetails -> getBonus();
                                                         $uplineAmount = $getUplineDetails -> getWithdrawAmount();
                                                         $totalUplinemount = $uplineAmount + $uplineBonus;
                                                         $totallyBonus = $currentlyBonus + $uplineBonus;

                                                         $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                         if(!$user)
                                                         {
                                                             $tableName = array();
                                                             $tableValue =  array();
                                                             $stringType =  "";
                                                             if($totallyBonus)
                                                             {
                                                                 array_push($tableName,"bonus");
                                                                 array_push($tableValue,$totallyBonus);
                                                                 $stringType .=  "i";
                                                              }
                                                              if($totallyBonus)
                                                              {
                                                                  array_push($tableName,"final_amount");
                                                                  array_push($tableValue,$totalUplinemount);
                                                                  $stringType .=  "i";
                                                               }
                                                               array_push($tableValue,$getUplineName);
                                                               $stringType .=  "s";
                                                               $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                               if($passwordUpdated)
                                                               {
                                                                   // echo "success";
                                                                   // $_SESSION['messageType'] = 1;
                                                                   // header('Location: ../wallet.php?type=1');
                                                                   // header('Location: ../editprofile.php?type=3');
                                                               }
                                                               else
                                                               {
                                                                   // echo "fail";
                                                                   // $_SESSION['messageType'] = 3;
                                                                   // header('Location: ../wallet.php?type=3');
                                                               }
                                                             }
                                                             if (commission($conn,$referrerName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$register_username_referrer))
                                                             {


                                                             }


                                                           }elseif($downlineNumber >= 2 && $downlineNumber <=5 && $getUpline <=5)//downline for the referer that been input
                                                           {
                                                                $bonus = 50;
                                                                $uplineBonus = 0;
                                                                $currentBonus = $userDetails -> getBonus();
                                                                $totalBonus = $currentBonus + $bonus;
                                                                $registerDownline = $userDetails->getRegisterDownlineNo() + 1;
                                                                $totalPoint = $pointReferee -300;
                                                                $totalCash = $cash + $bonus;

                                                                if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer))
                                                                {
                                                                     // $_SESSION['messageType'] = 1;
                                                                     // header('Location: ../wallet.php?type=1');
                                                                 $product = $register_product;
                                                                 //     if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$product))
                                                                 $referralFullname = $register_fullname;
                                                                 if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product))
                                                                     {
                                                                          // echo "success";
                                                                     }
                                                                     else
                                                                     {
                                                                          // echo "fail";
                                                                     }
                                                                }
                                                                else
                                                                {
                                                                     // $_SESSION['messageType'] = 2;
                                                                     // header('Location: ../wallet.php?type=1');
                                                                }

                                                                $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                                if(!$user)
                                                                {
                                                                     $tableName = array();
                                                                     $tableValue =  array();
                                                                     $stringType =  "";
                                                                     //echo "save to database";
                                                                     if($totalCash)
                                                                     {
                                                                          array_push($tableName,"final_amount");
                                                                          array_push($tableValue,$totalCash);
                                                                          $stringType .=  "i";
                                                                     }
                                                                     if($totalBonus)
                                                                     {
                                                                          array_push($tableName,"bonus");
                                                                          array_push($tableValue,$totalBonus);
                                                                          $stringType .=  "i";
                                                                     }
                                                                     if($registerDownline)
                                                                     {
                                                                          array_push($tableName,"register_downline_no");
                                                                          array_push($tableValue,$registerDownline);
                                                                          $stringType .=  "i";
                                                                     }

                                                                     array_push($tableValue,$register_username_referrer);
                                                                     $stringType .=  "s";
                                                                     $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                                     if($passwordUpdated)
                                                                     { }
                                                                     else
                                                                     { }
                                                                }
                                                                if(!$user)
                                                                {
                                                                     $tableName = array();
                                                                     $tableValue =  array();
                                                                     $stringType =  "";

                                                                     if($totalPoint)
                                                                     {
                                                                          array_push($tableName,"point");
                                                                          array_push($tableValue,$totalPoint);
                                                                          $stringType .=  "i";
                                                                     }
                                                                     if($totalPoint == 0)
                                                                     {  $point = 0;
                                                                          array_push($tableName,"point");
                                                                          array_push($tableValue,$point);
                                                                          $stringType .=  "i";
                                                                     }

                                                                     array_push($tableValue,$uid);
                                                                     $stringType .=  "s";
                                                                     $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                     if($passwordUpdated)
                                                                     {    }
                                                                     else
                                                                     {    }
                                                                }
                                                                $currentlyBonus = $getUplineDetails -> getBonus();
                                                                $uplineAmount = $getUplineDetails -> getWithdrawAmount();
                                                                $totalUplinemount = $uplineAmount + $uplineBonus;
                                                                $totallyBonus = $currentlyBonus + $uplineBonus;

                                                                $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                                if(!$user)
                                                                {
                                                                    $tableName = array();
                                                                    $tableValue =  array();
                                                                    $stringType =  "";
                                                                    if($totallyBonus)
                                                                    {
                                                                        array_push($tableName,"bonus");
                                                                        array_push($tableValue,$totallyBonus);
                                                                        $stringType .=  "i";
                                                                     }
                                                                     if($totallyBonus)
                                                                     {
                                                                         array_push($tableName,"final_amount");
                                                                         array_push($tableValue,$totalUplinemount);
                                                                         $stringType .=  "i";
                                                                      }
                                                                      array_push($tableValue,$getUplineName);
                                                                      $stringType .=  "s";
                                                                      $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                                      if($passwordUpdated)
                                                                      {
                                                                          // echo "success";
                                                                          // $_SESSION['messageType'] = 1;
                                                                          // header('Location: ../wallet.php?type=1');
                                                                          // header('Location: ../editprofile.php?type=3');
                                                                      }
                                                                      else
                                                                      {
                                                                          // echo "fail";
                                                                          // $_SESSION['messageType'] = 3;
                                                                          // header('Location: ../wallet.php?type=3');
                                                                      }
                                                                    }
                                                                    if (commission($conn,$referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$register_username_referrer))
                                                                    {


                                                                    }


                                                                  }elseif($downlineNumber >= 2 && $downlineNumber <=5 && $getUpline > 5)//downline for the referer that been input
                                                                  {
                                                                       $bonus = 50;
                                                                       // $uplineBonus = 30;
                                                                       $uplineBonus = 0;
                                                                       $currentBonus = $userDetails -> getBonus();
                                                                       $totalBonus = $currentBonus + $bonus;
                                                                       $registerDownline = $userDetails->getRegisterDownlineNo() + 1;
                                                                       $totalPoint = $pointReferee -300;
                                                                       $totalCash = $cash + $bonus;

                                                                       if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer))
                                                                       {
                                                                            // $_SESSION['messageType'] = 1;
                                                                            // header('Location: ../wallet.php?type=1');
                                                                            $product = $register_product;
                                                                           //  if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$product))
                                                                           $referralFullname = $register_fullname;
                                                                           if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product))
                                                                            {
                                                                                 // echo "success";
                                                                            }
                                                                            else
                                                                            {
                                                                                 // echo "fail";
                                                                            }
                                                                       }
                                                                       else
                                                                       {
                                                                            // $_SESSION['messageType'] = 2;
                                                                            // header('Location: ../wallet.php?type=1');
                                                                       }

                                                                       $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                                       if(!$user)
                                                                       {
                                                                            $tableName = array();
                                                                            $tableValue =  array();
                                                                            $stringType =  "";
                                                                            //echo "save to database";
                                                                            if($totalCash)
                                                                            {
                                                                                 array_push($tableName,"final_amount");
                                                                                 array_push($tableValue,$totalCash);
                                                                                 $stringType .=  "i";
                                                                            }
                                                                            if($totalBonus)
                                                                            {
                                                                                 array_push($tableName,"bonus");
                                                                                 array_push($tableValue,$totalBonus);
                                                                                 $stringType .=  "i";
                                                                            }
                                                                            if($registerDownline)
                                                                            {
                                                                                 array_push($tableName,"register_downline_no");
                                                                                 array_push($tableValue,$registerDownline);
                                                                                 $stringType .=  "i";
                                                                            }

                                                                            array_push($tableValue,$register_username_referrer);
                                                                            $stringType .=  "s";
                                                                            $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                                            if($passwordUpdated)
                                                                            { }
                                                                            else
                                                                            { }
                                                                       }
                                                                       if(!$user)
                                                                       {
                                                                            $tableName = array();
                                                                            $tableValue =  array();
                                                                            $stringType =  "";

                                                                            if($totalPoint)
                                                                            {
                                                                                 array_push($tableName,"point");
                                                                                 array_push($tableValue,$totalPoint);
                                                                                 $stringType .=  "i";
                                                                            }
                                                                            if($totalPoint == 0)
                                                                            {  $point = 0;
                                                                                 array_push($tableName,"point");
                                                                                 array_push($tableValue,$point);
                                                                                 $stringType .=  "i";
                                                                            }

                                                                            array_push($tableValue,$uid);
                                                                            $stringType .=  "s";
                                                                            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                            if($passwordUpdated)
                                                                            {    }
                                                                            else
                                                                            {    }
                                                                       }
                                                                       $currentlyBonus = $getUplineDetails -> getBonus();
                                                                       $uplineAmount = $getUplineDetails -> getWithdrawAmount();
                                                                       $totalUplinemount = $uplineAmount + $uplineBonus;
                                                                       $totallyBonus = $currentlyBonus + $uplineBonus;

                                                                       $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                                       if(!$user)
                                                                       {
                                                                           $tableName = array();
                                                                           $tableValue =  array();
                                                                           $stringType =  "";
                                                                           if($totallyBonus)
                                                                           {
                                                                               array_push($tableName,"bonus");
                                                                               array_push($tableValue,$totallyBonus);
                                                                               $stringType .=  "i";
                                                                            }
                                                                            if($totallyBonus)
                                                                            {
                                                                                array_push($tableName,"final_amount");
                                                                                array_push($tableValue,$totalUplinemount);
                                                                                $stringType .=  "i";
                                                                             }
                                                                             array_push($tableValue,$getUplineName);
                                                                             $stringType .=  "s";
                                                                             $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                                             if($passwordUpdated)
                                                                             {
                                                                                 // echo "success";
                                                                                 // $_SESSION['messageType'] = 1;
                                                                                 // header('Location: ../wallet.php?type=1');
                                                                                 // header('Location: ../editprofile.php?type=3');
                                                                             }
                                                                             else
                                                                             {
                                                                                 // echo "fail";
                                                                                 // $_SESSION['messageType'] = 3;
                                                                                 // header('Location: ../wallet.php?type=3');
                                                                             }
                                                                           }
                                                                           if (commission($conn,$referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$register_username_referrer))
                                                                           {


                                                                           }


                                                                  }
                                             sendEmailForVerification($register_uid);
                                             $_SESSION['messageType'] = 1;
                                             header('Location: ../addReferee.php?type=1');
                                        }
                                   }
                                   else
                                   {
                                        $_SESSION['messageType'] = 1;
                                        header('Location: ../addReferee.php?type=4');
                                   }
                              }
                              else
                              {
                                   header('Location: ../addReferee.php?promptError=1');
                              }
                         }
                         else
                         {
                              $_SESSION['messageType'] = 1;
                              header('Location: ../addReferee.php?type=2');
                              //echo "// register error with referral ";
                         }
                    }
                    else
                    { }

               }
               else
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../addReferee.php?type=4');
                    //echo "// password must be more than 6 ";
               }
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../addReferee.php?type=5');
               //echo "// password does not match ";
          }
     }

     else
     {

          if($register_password == $register_retype_password)
          {
               if($register_password_validation >= 6)
               {
                    if($register_username_referrer)
                    {
                         $referrerUserRows = getUser($conn," WHERE username = ? ",array("username"),array($register_username_referrer),"s");

                         if($referrerUserRows)
                         {
                              $referrerUid = $referrerUserRows[0]->getUid();
                              $referrerName = $referrerUserRows[0]->getUsername();
                              $referralName = $register_username;
                              $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
                              $currentLevel = 1;
                              $getUplineCurrentLevel = 1;

                              $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");
                              if($referralHistoryRows)
                              {
                              $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                              $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
                              }
                              $referralNewestRows = getReferralHistory($conn,"WHERE referral_name = ?", array("referral_name"),array($register_username_referrer), "s");
                              if($referralNewestRows)
                              {
                              //$topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                              $getUplineCurrentLevel = $referralNewestRows[0]->getCurrentLevel() + 1;
                              }


                              $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
                              $usernameDetails = $usernameRows[0];

                              $userPointRows = getUser($conn," WHERE username = ? ",array("username"),array($register_username_referrer),"s");
                              $userPointDetails = $userPointRows[0];

                              $fullnameRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($_POST['register_fullname']),"s");
                              $fullnameDetails = $fullnameRows[0];

                              $icNoRows = getUser($conn," WHERE ic_no = ? ",array("ic_no"),array($_POST['register_ic_no']),"i");
                              $icNoDetails = $icNoRows[0];

                              if (!$usernameDetails && !$fullnameDetails && !$icNoDetails)
                              {
                              $userPointReferee = $userPointDetails->getUserPoint();
                              $dckUid = $userPointDetails->getUid();

                                   if ($userPointReferee >= 0)
                                   {
                                     if ($register_username_referrer == "DCK"){
                                       $currentLevel = 1;
                                       $getUplineCurrentLevel = 1;
                                       $topReferrerUid = $dckUid;
                                     }
                                     if ($register_username_referrer == "dck"){
                                       $currentLevel = 1;
                                       $getUplineCurrentLevel = 1;
                                       $topReferrerUid = $dckUid;
                                     }

                                        if(registerNewUserWithoutEmail($conn,$register_username,$register_fullname,$register_uid,null,null,$finalPassword,$salt,$register_ic_no,$referrerUid,$topReferrerUid,$referralName,$currentLevel))
                                        {
                                             $userRows = getUser($conn," WHERE username = ? ",array("username"),array($register_username_referrer),"s");
                                             $userDetails = $userRows[0];

                                             $userPointRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
                                             $userPointDetails = $userPointRows[0];

                                             $referralRows = getUser($conn," WHERE username = ? ",array("username"),array($referralName),"s");
                                             $referralDetails = $referralRows[0];

                                             $referralNewRows = getReferralHistory($conn,"WHERE referral_name = ?", array("referral_name"),array($register_username_referrer), "s");
                                             $referralNewDetails = $referralNewRows[0];

                                             $referrerId = $referralNewDetails -> getReferrerId();//referrer_id

                                             $getUplineRows = getUser($conn, "WHERE uid = ? ", array("uid"), array($referrerId), "s");
                                             $getUplineDetails = $getUplineRows[0];

                                             $getUpline = $getUplineDetails -> getRegisterDownlineNo();
                                             $getUplineName = $getUplineDetails -> getUsername();
                                             $getUplineUid = $getUplineDetails -> getUid();

                                             $pointReferee = $userPointDetails->getUserPoint();//based on uid
                                             $downlineNumber = $userDetails->getRegisterDownlineNo();
                                             $cash = $userDetails->getWithdrawAmount();
                                             $referralUid = $referralDetails->getUid();
                                             $getUid = $userDetails -> getUid();

                                             // if ($downlineNumber = 5) {
                                             //   $title = "Agent";
                                             //   date_default_timezone_set("Asia/Kuala_Lumpur");
                                             //   $dateMembership =date("Y-m-d h:i:s");
                                             //
                                             //   // if (memberShip($conn,$referrerUid,$referrerName,$referralName,$title)) {
                                             //   //
                                             //   // }
                                             //
                                             //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                             //   if(!$user)
                                             //   {
                                             //        $tableName = array();
                                             //        $tableValue =  array();
                                             //        $stringType =  "";
                                             //        //echo "save to database";
                                             //        if($referralName)
                                             //        {
                                             //             array_push($tableName,"latest_referral");
                                             //             array_push($tableValue,$referralName);
                                             //             $stringType .=  "s";
                                             //        }
                                             //        if ($title) {
                                             //
                                             //          array_push($tableName,"title");
                                             //          array_push($tableValue,$title);
                                             //          $stringType .=  "s";
                                             //        }
                                             //        if ($dateMembership) {
                                             //
                                             //          array_push($tableName,"date_membership");
                                             //          array_push($tableValue,$dateMembership);
                                             //          $stringType .=  "s";
                                             //        }
                                             //
                                             //
                                             //        array_push($tableValue,$register_username_referrer);
                                             //        $stringType .=  "s";
                                             //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                             //        if($passwordUpdated)
                                             //        { }
                                             //        else
                                             //        { }
                                             //   }
                                             //
                                             // }elseif ($downlineNumber > 5) {
                                             //   $title = "Agent";
                                             //   date_default_timezone_set("Asia/Kuala_Lumpur");
                                             //
                                             //
                                             //   // if (memberShip($conn,$referrerUid,$referrerName,$referralName,$title)) {
                                             //   //
                                             //   // }
                                             //
                                             //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                             //   if(!$user)
                                             //   {
                                             //        $tableName = array();
                                             //        $tableValue =  array();
                                             //        $stringType =  "";
                                             //        //echo "save to database";
                                             //        if($referralName)
                                             //        {
                                             //             array_push($tableName,"latest_referral");
                                             //             array_push($tableValue,$referralName);
                                             //             $stringType .=  "s";
                                             //        }
                                             //        if ($title) {
                                             //
                                             //          array_push($tableName,"title");
                                             //          array_push($tableValue,$title);
                                             //          $stringType .=  "s";
                                             //        }
                                             //
                                             //        array_push($tableValue,$register_username_referrer);
                                             //        $stringType .=  "s";
                                             //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                             //        if($passwordUpdated)
                                             //        { }
                                             //        else
                                             //        { }
                                             //   }
                                             //
                                             // }elseif ($downlineNumber = 0) {
                                             //   $title = "Member";
                                             //   date_default_timezone_set("Asia/Kuala_Lumpur");
                                             //   $dateMembership =date("Y-m-d h:i:s");
                                             //
                                             //   // if (memberShip($conn,$referrerUid,$referrerName,$referralName,$title)) {
                                             //   //
                                             //   // }
                                             //
                                             //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                             //   if(!$user)
                                             //   {
                                             //        $tableName = array();
                                             //        $tableValue =  array();
                                             //        $stringType =  "";
                                             //        //echo "save to database";
                                             //        if($referralName)
                                             //        {
                                             //             array_push($tableName,"latest_referral");
                                             //             array_push($tableValue,$referralName);
                                             //             $stringType .=  "s";
                                             //        }
                                             //        if ($title) {
                                             //
                                             //          array_push($tableName,"title");
                                             //          array_push($tableValue,$title);
                                             //          $stringType .=  "s";
                                             //        }
                                             //        if ($dateMembership) {
                                             //
                                             //          array_push($tableName,"date_membership");
                                             //          array_push($tableValue,$dateMembership);
                                             //          $stringType .=  "s";
                                             //        }
                                             //
                                             //
                                             //        array_push($tableValue,$register_username_referrer);
                                             //        $stringType .=  "s";
                                             //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                             //        if($passwordUpdated)
                                             //        { }
                                             //        else
                                             //        { }
                                             //   }
                                             //
                                             // }elseif ($downlineNumber < 5) {
                                             //   $title = "Member";
                                             //   // $dateMembership = date("Y-m-d");
                                             //
                                             //   // if (memberShip($conn,$referrerUid,$referrerName,$referralName,$title)) {
                                             //   //
                                             //   // }
                                             //
                                             //   $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                             //   if(!$user)
                                             //   {
                                             //        $tableName = array();
                                             //        $tableValue =  array();
                                             //        $stringType =  "";
                                             //        //echo "save to database";
                                             //        if($referralName)
                                             //        {
                                             //             array_push($tableName,"latest_referral");
                                             //             array_push($tableValue,$referralName);
                                             //             $stringType .=  "s";
                                             //        }
                                             //        if ($title) {
                                             //
                                             //          array_push($tableName,"title");
                                             //          array_push($tableValue,$title);
                                             //          $stringType .=  "s";
                                             //        }
                                             //
                                             //        array_push($tableValue,$register_username_referrer);
                                             //        $stringType .=  "s";
                                             //        $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                             //        if($passwordUpdated)
                                             //        { }
                                             //        else
                                             //        { }
                                             //   }
                                             //
                                             // }



                                             if($downlineNumber < 2 && $getUpline <= 5)//downline for the referer that been input
                                             {



                                                  $bonus = 150;
                                                  $uplineBonus = 0;
                                                  $currentBonus = $userDetails -> getBonus();
                                                  $totalBonus = $currentBonus + $bonus;
                                                  $registerDownline = $userDetails->getRegisterDownlineNo() + 1;
                                                  $totalPoint = $pointReferee -300;
                                                  $totalCash = $cash + $bonus;

                                                  if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer))
                                                  {
                                                       // $_SESSION['messageType'] = 1;
                                                       // header('Location: ../wallet.php?type=1');
                                                       $product = $register_product;
                                                       // if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$product))
                                                       $referralFullname = $register_fullname;
                                                       if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product))
                                                       {
                                                            // echo "success";
                                                       }
                                                       else
                                                       {
                                                            // echo "fail";
                                                       }
                                                  }
                                                  else
                                                  {
                                                       // $_SESSION['messageType'] = 2;
                                                       // header('Location: ../wallet.php?type=1');
                                                  }

                                                  $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                  if(!$user)
                                                  {
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";
                                                       //echo "save to database";
                                                       if($totalCash)
                                                       {
                                                            array_push($tableName,"final_amount");
                                                            array_push($tableValue,$totalCash);
                                                            $stringType .=  "i";
                                                       }
                                                       if($totalBonus)
                                                       {
                                                            array_push($tableName,"bonus");
                                                            array_push($tableValue,$totalBonus);
                                                            $stringType .=  "i";
                                                       }
                                                       if($registerDownline)
                                                       {
                                                            array_push($tableName,"register_downline_no");
                                                            array_push($tableValue,$registerDownline);
                                                            $stringType .=  "i";
                                                       }

                                                       array_push($tableValue,$register_username_referrer);
                                                       $stringType .=  "s";
                                                       $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                       if($passwordUpdated)
                                                       { }
                                                       else
                                                       { }
                                                  }
                                                  if(!$user)
                                                  {
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";

                                                       if($totalPoint)
                                                       {
                                                            array_push($tableName,"point");
                                                            array_push($tableValue,$totalPoint);
                                                            $stringType .=  "i";
                                                       }
                                                       if($totalPoint == 0)
                                                       {  $point = 0;
                                                            array_push($tableName,"point");
                                                            array_push($tableValue,$point);
                                                            $stringType .=  "i";
                                                       }

                                                       array_push($tableValue,$uid);
                                                       $stringType .=  "s";
                                                       $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                       if($passwordUpdated)
                                                       {    }
                                                       else
                                                       {    }
                                                  }
                                                    $currentlyBonus = $getUplineDetails -> getBonus();
                                                    $uplineAmount = $getUplineDetails -> getWithdrawAmount();
                                                    $totalUplinemount = $uplineAmount + $uplineBonus;
                                                    $totallyBonus = $currentlyBonus + $uplineBonus;

                                                    $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                    if(!$user)
                                                    {
                                                        $tableName = array();
                                                        $tableValue =  array();
                                                        $stringType =  "";
                                                        if($totallyBonus)
                                                        {
                                                            array_push($tableName,"bonus");
                                                            array_push($tableValue,$totallyBonus);
                                                            $stringType .=  "i";
                                                         }
                                                         if($totallyBonus)
                                                         {
                                                             array_push($tableName,"final_amount");
                                                             array_push($tableValue,$totalUplinemount);
                                                             $stringType .=  "i";
                                                          }

                                                        array_push($tableValue,$getUplineName);
                                                        $stringType .=  "s";
                                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                        if($passwordUpdated)
                                                        {
                                                            // echo "success";
                                                            // $_SESSION['messageType'] = 1;
                                                            // header('Location: ../wallet.php?type=1');
                                                            // header('Location: ../editprofile.php?type=3');
                                                        }
                                                        else
                                                        {
                                                            // echo "fail";
                                                            // $_SESSION['messageType'] = 3;
                                                            // header('Location: ../wallet.php?type=3');
                                                        }
                                                      }
                                                      if (commission($conn,$referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$register_username_referrer))
                                                      {


                                                      }
                                                      $_SESSION['messageType'] = 1;
                                                      header('Location: ../addReferee.php?type=1');

                                             }elseif($downlineNumber < 2 && $getUpline > 5)//downline for the referer that been input
                                             {
                                                  $bonus = 150;
                                                  // $uplineBonus = 30;
                                                  $uplineBonus = 0;
                                                  $currentBonus = $userDetails -> getBonus();
                                                  $totalBonus = $currentBonus + $bonus;
                                                  $registerDownline = $userDetails->getRegisterDownlineNo() + 1;
                                                  $totalPoint = $pointReferee -300;
                                                  $totalCash = $cash + $bonus;

                                                  if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer))
                                                  {
                                                       // $_SESSION['messageType'] = 1;
                                                       // header('Location: ../wallet.php?type=1');
                                                       $product = $register_product;
                                                       // if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$product))
                                                       $referralFullname = $register_fullname;
                                                       if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product))
                                                       {
                                                            // echo "success";
                                                       }
                                                       else
                                                       {
                                                            // echo "fail";
                                                       }
                                                  }
                                                  else
                                                  {
                                                       // $_SESSION['messageType'] = 2;
                                                       // header('Location: ../wallet.php?type=1');
                                                  }

                                                  $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                  if(!$user)
                                                  {
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";
                                                       //echo "save to database";
                                                       if($totalCash)
                                                       {
                                                            array_push($tableName,"final_amount");
                                                            array_push($tableValue,$totalCash);
                                                            $stringType .=  "i";
                                                       }
                                                       if($totalBonus)
                                                       {
                                                            array_push($tableName,"bonus");
                                                            array_push($tableValue,$totalBonus);
                                                            $stringType .=  "i";
                                                       }
                                                       if($registerDownline)
                                                       {
                                                            array_push($tableName,"register_downline_no");
                                                            array_push($tableValue,$registerDownline);
                                                            $stringType .=  "i";
                                                       }

                                                       array_push($tableValue,$register_username_referrer);
                                                       $stringType .=  "s";
                                                       $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                       if($passwordUpdated)
                                                       { }
                                                       else
                                                       { }
                                                  }
                                                  if(!$user)
                                                  {
                                                       $tableName = array();
                                                       $tableValue =  array();
                                                       $stringType =  "";

                                                       if($totalPoint)
                                                       {
                                                            array_push($tableName,"point");
                                                            array_push($tableValue,$totalPoint);
                                                            $stringType .=  "i";
                                                       }
                                                       if($totalPoint == 0)
                                                       {  $point = 0;
                                                            array_push($tableName,"point");
                                                            array_push($tableValue,$point);
                                                            $stringType .=  "i";
                                                       }

                                                       array_push($tableValue,$uid);
                                                       $stringType .=  "s";
                                                       $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                       if($passwordUpdated)
                                                       {    }
                                                       else
                                                       {    }
                                                  }
                                                  $currentlyBonus = $getUplineDetails -> getBonus();
                                                  $uplineAmount = $getUplineDetails -> getWithdrawAmount();
                                                  $totalUplinemount = $uplineAmount + $uplineBonus;
                                                  $totallyBonus = $currentlyBonus + $uplineBonus;

                                                  $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                  if(!$user)
                                                  {
                                                      $tableName = array();
                                                      $tableValue =  array();
                                                      $stringType =  "";
                                                      if($totallyBonus)
                                                      {
                                                          array_push($tableName,"bonus");
                                                          array_push($tableValue,$totallyBonus);
                                                          $stringType .=  "i";
                                                       }
                                                       if($totallyBonus)
                                                       {
                                                           array_push($tableName,"final_amount");
                                                           array_push($tableValue,$totalUplinemount);
                                                           $stringType .=  "i";
                                                        }
                                                        array_push($tableValue,$getUplineName);
                                                        $stringType .=  "s";
                                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                        if($passwordUpdated)
                                                        {
                                                            // echo "success";
                                                            // $_SESSION['messageType'] = 1;
                                                            // header('Location: ../wallet.php?type=1');
                                                            // header('Location: ../editprofile.php?type=3');
                                                        }
                                                        else
                                                        {
                                                            // echo "fail";
                                                            // $_SESSION['messageType'] = 3;
                                                            // header('Location: ../wallet.php?type=3');
                                                        }
                                                      }
                                                      if (commission($conn,$referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$register_username_referrer))
                                                      {


                                                      }

                                                    }elseif($downlineNumber > 5 && $getUpline <=5)//downline for the referer that been input
                                                    {
                                                         $bonus = 80;
                                                         $uplineBonus = 0;
                                                         $currentBonus = $userDetails -> getBonus();
                                                         $totalBonus = $currentBonus + $bonus;
                                                         $registerDownline = $userDetails->getRegisterDownlineNo() + 1;
                                                         $totalPoint = $pointReferee -300;
                                                         $totalCash = $cash + $bonus;
                                                         $currentStatus = "Agent";

                                                         if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer))
                                                         {
                                                            // $_SESSION['messageType'] = 1;
                                                            // header('Location: ../wallet.php?type=1');
                                                            $product = $register_product;
                                                            //   if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$product))
                                                            $referralFullname = $register_fullname;
                                                            if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product))
                                                              {
                                                                   // echo "success";
                                                              }
                                                              else
                                                              {
                                                                   // echo "fail";
                                                              }
                                                         }
                                                         else
                                                         {
                                                              // $_SESSION['messageType'] = 2;
                                                              // header('Location: ../wallet.php?type=1');
                                                         }

                                                         $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                         if(!$user)
                                                         {
                                                              $currentStatus = "Agent";
                                                              $tableName = array();
                                                              $tableValue =  array();
                                                              $stringType =  "";
                                                              //echo "save to database";
                                                              if($totalCash)
                                                              {
                                                                   array_push($tableName,"final_amount");
                                                                   array_push($tableValue,$totalCash);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($totalBonus)
                                                              {
                                                                   array_push($tableName,"bonus");
                                                                   array_push($tableValue,$totalBonus);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($registerDownline)
                                                              {
                                                                   array_push($tableName,"register_downline_no");
                                                                   array_push($tableValue,$registerDownline);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($currentStatus)
                                                              {
                                                                   array_push($tableName,"current_status");
                                                                   array_push($tableValue,$currentStatus);
                                                                   $stringType .=  "s";
                                                              }

                                                              array_push($tableValue,$register_username_referrer);
                                                              $stringType .=  "s";
                                                              $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                              if($passwordUpdated)
                                                              { }
                                                              else
                                                              { }
                                                         }
                                                         if(!$user)
                                                         {
                                                              $tableName = array();
                                                              $tableValue =  array();
                                                              $stringType =  "";

                                                              if($totalPoint)
                                                              {
                                                                   array_push($tableName,"point");
                                                                   array_push($tableValue,$totalPoint);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($totalPoint == 0)
                                                              {  $point = 0;
                                                                   array_push($tableName,"point");
                                                                   array_push($tableValue,$point);
                                                                   $stringType .=  "i";
                                                              }

                                                              array_push($tableValue,$uid);
                                                              $stringType .=  "s";
                                                              $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                              if($passwordUpdated)
                                                              {    }
                                                              else
                                                              {    }
                                                         }
                                                         $currentlyBonus = $getUplineDetails -> getBonus();
                                                         $uplineAmount = $getUplineDetails -> getWithdrawAmount();
                                                         $totalUplinemount = $uplineAmount + $uplineBonus;
                                                         $totallyBonus = $currentlyBonus + $uplineBonus;

                                                         $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                         if(!$user)
                                                         {
                                                             $tableName = array();
                                                             $tableValue =  array();
                                                             $stringType =  "";
                                                             if($totallyBonus)
                                                             {
                                                                 array_push($tableName,"bonus");
                                                                 array_push($tableValue,$totallyBonus);
                                                                 $stringType .=  "i";
                                                              }
                                                              if($totallyBonus)
                                                              {
                                                                  array_push($tableName,"final_amount");
                                                                  array_push($tableValue,$totalUplinemount);
                                                                  $stringType .=  "i";
                                                               }
                                                               array_push($tableValue,$getUplineName);
                                                               $stringType .=  "s";
                                                               $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                               if($passwordUpdated)
                                                               {
                                                                   // echo "success";
                                                                   // $_SESSION['messageType'] = 1;
                                                                   // header('Location: ../wallet.php?type=1');
                                                                   // header('Location: ../editprofile.php?type=3');
                                                               }
                                                               else
                                                               {
                                                                   // echo "fail";
                                                                   // $_SESSION['messageType'] = 3;
                                                                   // header('Location: ../wallet.php?type=3');
                                                               }
                                                             }
                                                             if (commission($conn,$referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$register_username_referrer))
                                                             {


                                                             }

                                                    }elseif($downlineNumber > 5 && $getUpline > 5)//downline for the referer that been input
                                                    {
                                                         $bonus = 80;
                                                         // $uplineBonus = 10;
                                                         $uplineBonus = 0;
                                                         $currentBonus = $userDetails -> getBonus();
                                                         $totalBonus = $currentBonus + $bonus;
                                                         $registerDownline = $userDetails->getRegisterDownlineNo() + 1;
                                                         $totalPoint = $pointReferee -300;
                                                         $totalCash = $cash + $bonus;
                                                         $currentStatus = "Agent";

                                                         if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer))
                                                         {
                                                            // $_SESSION['messageType'] = 1;
                                                            // header('Location: ../wallet.php?type=1');
                                                            $product = $register_product;
                                                            //   if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$product))
                                                            $referralFullname = $register_fullname;
                                                            if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product))
                                                              {
                                                                   // echo "success";
                                                              }
                                                              else
                                                              {
                                                                   // echo "fail";
                                                              }
                                                         }
                                                         else
                                                         {
                                                              // $_SESSION['messageType'] = 2;
                                                              // header('Location: ../wallet.php?type=1');
                                                         }

                                                         $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                         if(!$user)
                                                         {
                                                              $currentStatus = "Agent";
                                                              $tableName = array();
                                                              $tableValue =  array();
                                                              $stringType =  "";
                                                              //echo "save to database";
                                                              if($totalCash)
                                                              {
                                                                   array_push($tableName,"final_amount");
                                                                   array_push($tableValue,$totalCash);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($totalBonus)
                                                              {
                                                                   array_push($tableName,"bonus");
                                                                   array_push($tableValue,$totalBonus);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($registerDownline)
                                                              {
                                                                   array_push($tableName,"register_downline_no");
                                                                   array_push($tableValue,$registerDownline);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($currentStatus)
                                                              {
                                                                   array_push($tableName,"current_status");
                                                                   array_push($tableValue,$currentStatus);
                                                                   $stringType .=  "s";
                                                              }


                                                              array_push($tableValue,$register_username_referrer);
                                                              $stringType .=  "s";
                                                              $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                              if($passwordUpdated)
                                                              { }
                                                              else
                                                              { }
                                                         }
                                                         if(!$user)
                                                         {
                                                              $tableName = array();
                                                              $tableValue =  array();
                                                              $stringType =  "";

                                                              if($totalPoint)
                                                              {
                                                                   array_push($tableName,"point");
                                                                   array_push($tableValue,$totalPoint);
                                                                   $stringType .=  "i";
                                                              }
                                                              if($totalPoint == 0)
                                                              {  $point = 0;
                                                                   array_push($tableName,"point");
                                                                   array_push($tableValue,$point);
                                                                   $stringType .=  "i";
                                                              }

                                                              array_push($tableValue,$uid);
                                                              $stringType .=  "s";
                                                              $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                              if($passwordUpdated)
                                                              {    }
                                                              else
                                                              {    }
                                                         }
                                                         $currentlyBonus = $getUplineDetails -> getBonus();
                                                         $uplineAmount = $getUplineDetails -> getWithdrawAmount();
                                                         $totalUplinemount = $uplineAmount + $uplineBonus;
                                                         $totallyBonus = $currentlyBonus + $uplineBonus;

                                                         $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                         if(!$user)
                                                         {
                                                             $tableName = array();
                                                             $tableValue =  array();
                                                             $stringType =  "";
                                                             if($totallyBonus)
                                                             {
                                                                 array_push($tableName,"bonus");
                                                                 array_push($tableValue,$totallyBonus);
                                                                 $stringType .=  "i";
                                                              }
                                                              if($totallyBonus)
                                                              {
                                                                  array_push($tableName,"final_amount");
                                                                  array_push($tableValue,$totalUplinemount);
                                                                  $stringType .=  "i";
                                                               }
                                                               array_push($tableValue,$getUplineName);
                                                               $stringType .=  "s";
                                                               $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                               if($passwordUpdated)
                                                               {
                                                                   // echo "success";
                                                                   // $_SESSION['messageType'] = 1;
                                                                   // header('Location: ../wallet.php?type=1');
                                                                   // header('Location: ../editprofile.php?type=3');
                                                               }
                                                               else
                                                               {
                                                                   // echo "fail";
                                                                   // $_SESSION['messageType'] = 3;
                                                                   // header('Location: ../wallet.php?type=3');
                                                               }
                                                             }
                                                             if (commission($conn,$referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$register_username_referrer))
                                                             {


                                                             }

                                                           }elseif($downlineNumber >= 2 && $downlineNumber <=5 && $getUpline <=5)//downline for the referer that been input
                                                           {
                                                                $bonus = 50;
                                                                $uplineBonus = 0;
                                                                $currentBonus = $userDetails -> getBonus();
                                                                $totalBonus = $currentBonus + $bonus;
                                                                $registerDownline = $userDetails->getRegisterDownlineNo() + 1;
                                                                $totalPoint = $pointReferee -300;
                                                                $totalCash = $cash + $bonus;

                                                                if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer))
                                                                {
                                                                 // $_SESSION['messageType'] = 1;
                                                                 // header('Location: ../wallet.php?type=1');
                                                                 $product = $register_product;
                                                                 //     if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$product))
                                                                 $referralFullname = $register_fullname;
                                                                 if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product))
                                                                     {
                                                                          // echo "success";
                                                                     }
                                                                     else
                                                                     {
                                                                          // echo "fail";
                                                                     }
                                                                }
                                                                else
                                                                {
                                                                     // $_SESSION['messageType'] = 2;
                                                                     // header('Location: ../wallet.php?type=1');
                                                                }

                                                                $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                                if(!$user)
                                                                {
                                                                     $tableName = array();
                                                                     $tableValue =  array();
                                                                     $stringType =  "";
                                                                     //echo "save to database";
                                                                     if($totalCash)
                                                                     {
                                                                          array_push($tableName,"final_amount");
                                                                          array_push($tableValue,$totalCash);
                                                                          $stringType .=  "i";
                                                                     }
                                                                     if($totalBonus)
                                                                     {
                                                                          array_push($tableName,"bonus");
                                                                          array_push($tableValue,$totalBonus);
                                                                          $stringType .=  "i";
                                                                     }
                                                                     if($registerDownline)
                                                                     {
                                                                          array_push($tableName,"register_downline_no");
                                                                          array_push($tableValue,$registerDownline);
                                                                          $stringType .=  "i";
                                                                     }

                                                                     array_push($tableValue,$register_username_referrer);
                                                                     $stringType .=  "s";
                                                                     $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                                     if($passwordUpdated)
                                                                     { }
                                                                     else
                                                                     { }
                                                                }
                                                                if(!$user)
                                                                {
                                                                     $tableName = array();
                                                                     $tableValue =  array();
                                                                     $stringType =  "";

                                                                     if($totalPoint)
                                                                     {
                                                                          array_push($tableName,"point");
                                                                          array_push($tableValue,$totalPoint);
                                                                          $stringType .=  "i";
                                                                     }
                                                                     if($totalPoint == 0)
                                                                     {  $point = 0;
                                                                          array_push($tableName,"point");
                                                                          array_push($tableValue,$point);
                                                                          $stringType .=  "i";
                                                                     }

                                                                     array_push($tableValue,$uid);
                                                                     $stringType .=  "s";
                                                                     $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                     if($passwordUpdated)
                                                                     {    }
                                                                     else
                                                                     {    }
                                                                }
                                                                $currentlyBonus = $getUplineDetails -> getBonus();
                                                                $uplineAmount = $getUplineDetails -> getWithdrawAmount();
                                                                $totalUplinemount = $uplineAmount + $uplineBonus;
                                                                $totallyBonus = $currentlyBonus + $uplineBonus;

                                                                $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                                if(!$user)
                                                                {
                                                                    $tableName = array();
                                                                    $tableValue =  array();
                                                                    $stringType =  "";
                                                                    if($totallyBonus)
                                                                    {
                                                                        array_push($tableName,"bonus");
                                                                        array_push($tableValue,$totallyBonus);
                                                                        $stringType .=  "i";
                                                                     }
                                                                     if($totallyBonus)
                                                                     {
                                                                         array_push($tableName,"final_amount");
                                                                         array_push($tableValue,$totalUplinemount);
                                                                         $stringType .=  "i";
                                                                      }
                                                                      array_push($tableValue,$getUplineName);
                                                                      $stringType .=  "s";
                                                                      $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                                      if($passwordUpdated)
                                                                      {
                                                                          // echo "success";
                                                                          // $_SESSION['messageType'] = 1;
                                                                          // header('Location: ../wallet.php?type=1');
                                                                          // header('Location: ../editprofile.php?type=3');
                                                                      }
                                                                      else
                                                                      {
                                                                          // echo "fail";
                                                                          // $_SESSION['messageType'] = 3;
                                                                          // header('Location: ../wallet.php?type=3');
                                                                      }
                                                                    }
                                                                    if (commission($conn,$referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$register_username_referrer))
                                                                    {


                                                                    }

                                                                  }elseif($downlineNumber >= 2 && $downlineNumber <=5 && $getUpline > 5)//downline for the referer that been input
                                                                  {
                                                                       $bonus = 50;
                                                                       // $uplineBonus = 30;
                                                                       $uplineBonus = 0;
                                                                       $currentBonus = $userDetails -> getBonus();
                                                                       $totalBonus = $currentBonus + $bonus;
                                                                       $registerDownline = $userDetails->getRegisterDownlineNo() + 1;
                                                                       $totalPoint = $pointReferee -300;
                                                                       $totalCash = $cash + $bonus;

                                                                       if (NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer))
                                                                       {
                                                                           // $_SESSION['messageType'] = 1;
                                                                           // header('Location: ../wallet.php?type=1');
                                                                           $product = $register_product;
                                                                           //  if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$product))
                                                                           $referralFullname = $register_fullname;
                                                                           if (SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product))
                                                                            {
                                                                                 // echo "success";
                                                                            }
                                                                            else
                                                                            {
                                                                                 // echo "fail";
                                                                            }
                                                                       }
                                                                       else
                                                                       {
                                                                            // $_SESSION['messageType'] = 2;
                                                                            // header('Location: ../wallet.php?type=1');
                                                                       }

                                                                       $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                                       if(!$user)
                                                                       {
                                                                            $tableName = array();
                                                                            $tableValue =  array();
                                                                            $stringType =  "";
                                                                            //echo "save to database";
                                                                            if($totalCash)
                                                                            {
                                                                                 array_push($tableName,"final_amount");
                                                                                 array_push($tableValue,$totalCash);
                                                                                 $stringType .=  "i";
                                                                            }
                                                                            if($totalBonus)
                                                                            {
                                                                                 array_push($tableName,"bonus");
                                                                                 array_push($tableValue,$totalBonus);
                                                                                 $stringType .=  "i";
                                                                            }
                                                                            if($registerDownline)
                                                                            {
                                                                                 array_push($tableName,"register_downline_no");
                                                                                 array_push($tableValue,$registerDownline);
                                                                                 $stringType .=  "i";
                                                                            }

                                                                            array_push($tableValue,$register_username_referrer);
                                                                            $stringType .=  "s";
                                                                            $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                                            if($passwordUpdated)
                                                                            { }
                                                                            else
                                                                            { }
                                                                       }
                                                                       if(!$user)
                                                                       {
                                                                            $tableName = array();
                                                                            $tableValue =  array();
                                                                            $stringType =  "";

                                                                            if($totalPoint)
                                                                            {
                                                                                 array_push($tableName,"point");
                                                                                 array_push($tableValue,$totalPoint);
                                                                                 $stringType .=  "i";
                                                                            }
                                                                            if($totalPoint == 0)
                                                                            {  $point = 0;
                                                                                 array_push($tableName,"point");
                                                                                 array_push($tableValue,$point);
                                                                                 $stringType .=  "i";
                                                                            }

                                                                            array_push($tableValue,$uid);
                                                                            $stringType .=  "s";
                                                                            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                            if($passwordUpdated)
                                                                            {    }
                                                                            else
                                                                            {    }
                                                                       }
                                                                       $currentlyBonus = $getUplineDetails -> getBonus();
                                                                       $uplineAmount = $getUplineDetails -> getWithdrawAmount();
                                                                       $totalUplinemount = $uplineAmount + $uplineBonus;
                                                                       $totallyBonus = $currentlyBonus + $uplineBonus;

                                                                       $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                                                       if(!$user)
                                                                       {
                                                                           $tableName = array();
                                                                           $tableValue =  array();
                                                                           $stringType =  "";
                                                                           if($totallyBonus)
                                                                           {
                                                                               array_push($tableName,"bonus");
                                                                               array_push($tableValue,$totallyBonus);
                                                                               $stringType .=  "i";
                                                                            }
                                                                            if($totallyBonus)
                                                                            {
                                                                                array_push($tableName,"final_amount");
                                                                                array_push($tableValue,$totalUplinemount);
                                                                                $stringType .=  "i";
                                                                             }
                                                                             array_push($tableValue,$getUplineName);
                                                                             $stringType .=  "s";
                                                                             $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                                             if($passwordUpdated)
                                                                             {
                                                                                 // echo "success";
                                                                                 // $_SESSION['messageType'] = 1;
                                                                                 // header('Location: ../wallet.php?type=1');
                                                                                 // header('Location: ../editprofile.php?type=3');
                                                                             }
                                                                             else
                                                                             {
                                                                                 // echo "fail";
                                                                                 // $_SESSION['messageType'] = 3;
                                                                                 // header('Location: ../wallet.php?type=3');
                                                                             }
                                                                           }

                                                                           if (commission($conn,$referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$register_username_referrer))
                                                                           {


                                                                           }

                                                                  }
                                             $_SESSION['messageType'] = 1;
                                             header('Location: ../addReferee.php?type=1');
                                        }
                                   }
                                   else
                                   {
                                        $_SESSION['messageType'] = 1;
                                        header('Location: ../addReferee.php?type=4');
                                   }

                              }
                              else
                              {
                                   header('Location: ../addReferee.php?promptError=1');
                              }
                         }
                         else
                         {
                              $_SESSION['messageType'] = 1;
                              header('Location: ../addReferee.php?type=2');
                              //echo "// register error with referral ";
                         }
                    }
                    else
                    { }
               }
               else
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../addReferee.php?type=4');
                    //echo "// password must be more than 6 ";
               }
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../addReferee.php?type=5');
               //echo "// password does not match ";
          }

     }
}
else
{
     header('Location: ../addReferee.php');
}

function NewWithdraw($conn,$referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer)
{
     if(insertDynamicData($conn,"bonus",array("referral_name","referrer_id","referrer_name","current_level","register_downline_no","top_referrer_id","referral_id","amount","signupby"),
     array($referralName,$referrerUid,$referrerName,$currentLevel,$registerDownline,$topReferrerUid,$referralUid,$bonus,$register_username_referrer),"sssiissis") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
function commission($conn,$referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$register_username_referrer)
{
     if(insertDynamicData($conn,"bonus",array("referral_name","referrer_id","referrer_name","current_level","register_downline_no","top_referrer_id","referral_id","amount","signupby"),
     array($referralName,$getUplineUid,$getUplineName,$getUplineCurrentLevel,$getUpline,$topReferrerUid,$referralUid,$uplineBonus,$register_username_referrer),"sssiissis") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

// function SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$product)
// {
//      if(insertDynamicData($conn,"signup_product",array("referrer_id","referrer_name","referral_id","referral_name","product"),
//      array($referrerUid,$referrerName,$referralUid,$referralName,$product),"sssss") === null)
//      {
//           return false;
//      }
//      else
//      {}
//      return true;
// }

function SignUpProduct($conn,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product)
{
     if(insertDynamicData($conn,"signup_product",array("referrer_id","referrer_name","referral_id","referral_name","referral_fullname","product"),
     array($referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
function memberShip($conn,$referrerUid,$referrerName,$referralName,$title)
{
     if(insertDynamicData($conn,"membership",array("uid","name","latest_referral","title"),
     array($referrerUid,$referrerName,$referralName,$title),"ssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

?>
