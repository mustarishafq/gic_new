<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess1.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addUplineCommission($conn, $id, $purchaserName, $loanUid, $ulOverride,$upline1)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","commission","upline"),
     array($id, $purchaserName, $loanUid, $ulOverride,$upline1),
     "issds") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addUpuplineCommission($conn, $id, $purchaserName, $loanUid, $uulOverride,$upline2)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","commission","upline"),
     array($id, $purchaserName, $loanUid, $uulOverride,$upline2),
     "issds") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}

function addNewProduct($conn, $id, $projectName, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package,
                         $discount, $rebate, $extraRebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus, $remark, $bFormCollected, $paymentMethod,
                         $lawyer, $pendingApprovalStatus, $bankApproved, $loSignedDate, $laSignedDate, $spaSignedDate, $fullsetCompleted, $cashBuyer, $cancelledBooking, $caseStatus,
                         $eventPersonal, $rate, $agentComm, $upline1, $upline2, $plName, $hosName, $listerName, $ulOverride, $uulOverride,
                         $plOverride, $hosOverride, $listerOverride, $admin1Override, $admin2Override, $admin3Override, $gicProfit, $totalClaimedDevAmt, $totalBalUnclaimAmt, $loanUid)
{
     if(insertDynamicData($conn,"loan_status",
     array("id","unit_no","project_name","purchaser_name","ic","contact","email","booking_date","sq_ft","spa_price","package",
     "discount","rebate","extra_rebate","nettprice","totaldevelopercomm","agent","loanstatus","remark","bform_Collected","payment_method",
     "lawyer","pending_approval_status","bank_approved","lo_signed_date","la_signed_date","spa_signed_date","fullset_completed","cash_buyer","cancelled_booking","case_status",
     "event_personal","rate","agent_comm","upline1","upline2","pl_name","hos_name","lister_name","ul_override","uul_override",
     "pl_override","hos_override","lister_override","admin1_override","admin2_override","admin3_override","gic_profit","total_claimed_dev_amt","total_bal_unclaim_amt","loan_uid"),
     array($id, $projectName, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package,
     $discount, $rebate, $extraRebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus, $remark, $bFormCollected, $paymentMethod,
     $lawyer, $pendingApprovalStatus, $bankApproved, $loSignedDate, $laSignedDate, $spaSignedDate, $fullsetCompleted, $cashBuyer, $cancelledBooking, $caseStatus,
     $eventPersonal, $rate, $agentComm, $upline1, $upline2, $plName, $hosName, $listerName, $ulOverride, $uulOverride,
     $plOverride, $hosOverride, $listerOverride, $admin1Override, $admin2Override, $admin3Override, $gicProfit, $totalClaimedDevAmt, $totalBalUnclaimAmt, $loanUid),
     "sssssssssssssssssssssssssssssssssssssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $id = rewrite($_POST["id"]);
     $loanUid = md5(uniqid());
     $projectName = rewrite($_POST["project_name"]);
     $unitNo = rewrite($_POST["unit_no"]);
     $purchaserName = rewrite($_POST["purchaser_name"]);
     $ic = rewrite($_POST["ic"]);
     $contact = rewrite($_POST["contact"]);
     $email = rewrite($_POST["email"]);
     $bookingDate = rewrite($_POST["booking_date"]);
     $sqFt = rewrite($_POST["sq_ft"]);

     // $spaPrice = rewrite($_POST["spa_price"]);

     $str2 = rewrite($_POST["spa_price"]);
     $newSPAPrice = str_replace( ',', '', $str2);
     $spaPrice = $newSPAPrice;


     $package = rewrite($_POST["package"]);

     $discount = rewrite($_POST["discount"]);
     $rebate = rewrite($_POST["rebate"]);
     $extraRebate = rewrite($_POST["extra_rebate"]);

     //remove comma inside value
     // $nettPrice = rewrite($_POST["nettprice"]);

     $str1 = rewrite($_POST["nettprice"]);
     $newNettPrice = str_replace( ',', '', $str1);
     $nettPrice = $newNettPrice;


     $totalDeveloperComm = rewrite($_POST["totaldevelopercomm"]);

     //get upline, up-upline
     $agent = rewrite($_POST["agent"]);

     $getUplineDetails = getUser($conn," WHERE username = ? ",array("username"),array($agent),"s");
     $upline1Name = $getUplineDetails[0]->getUpline1();
     $upline2Name = $getUplineDetails[0]->getUpline2();

     $loanStatus = rewrite($_POST["loanstatus"]);
     $remark = rewrite($_POST["remark"]);
     $bFormCollected = rewrite($_POST["bform_Collected"]);
     $paymentMethod = rewrite($_POST["payment_method"]);

     $lawyer = rewrite($_POST["lawyer"]);
     $pendingApprovalStatus = rewrite($_POST["pending_approval_status"]);
     $bankApproved = rewrite($_POST["bank_approved"]);
     $loSignedDate = rewrite($_POST["lo_signed_date"]);
     $laSignedDate = rewrite($_POST["la_signed_date"]);
     $spaSignedDate = rewrite($_POST["spa_signed_date"]);
     $fullsetCompleted = rewrite($_POST["fullset_completed"]);

     //add cash buyer
     $cashBuyer = rewrite($_POST["cash_buyer"]);

     $cancelledBooking = rewrite($_POST["cancelled_booking"]);
     $caseStatus = rewrite($_POST["case_status"]);

     // if ($caseStatus == 'COMPLETED') {
     //   $upline = rewrite($_POST['upline']);
     //   $upUpline = rewrite($_POST['up_upline']);
     //   $uplineCommission = rewrite($_POST['upline_commission']);
     //   $uulCommission = rewrite($_POST['up_upline_commission']);
     // }

     $eventPersonal = rewrite($_POST["event_personal"]);
     $rate = rewrite($_POST["rate"]);

     //get and update upline and upline
     // $upline1 = $upline1Name;
     // $upline2 = $upline2Name;

     $upline1 = $upline1Name;
     if (!$upline1) {
       $upline1 = "";
     }
     $upline2 = $upline2Name;
     if (!$upline2) {
       $upline2 = "";
     }

     $plName = rewrite($_POST["pl_name"]);
     $hosName = rewrite($_POST["hos_name"]);

     //calculation for commission
     $ratePercentage =( $rate / 100 );
     $agentComm = $ratePercentage * $nettPrice;
     // $agentComm = rewrite($_POST["agent_comm"]);

     // $upline1 = rewrite($_POST["upline1"]);
     // $upline2 = rewrite($_POST["upline2"]);
     // $plName = rewrite($_POST["pl_name"]);
     // $hosName = rewrite($_POST["hos_name"]);

     $listerName = rewrite($_POST["lister_name"]);

     $ulOverride = $agentComm * (5/100);
     $uulOverride = $agentComm * (5/100);
     $plOverride = $agentComm * (15/100);
     $hosOverride = $agentComm * (5/100);

     $listerOverride = rewrite($_POST["lister_override"]);
     $admin1Override = rewrite($_POST["admin1_override"]);
     $admin2Override = rewrite($_POST["admin2_override"]);

     //add admin3override
     $admin3Override = rewrite($_POST["admin3_override"]);

     $gicProfit = rewrite($_POST["gic_profit"]);
     $totalClaimedDevAmt = rewrite($_POST["total_claimed_dev_amt"]);
     $totalBalUnclaimAmt = rewrite($_POST["total_bal_unclaim_amt"]);

     //   FOR DEBUGGING
     // echo $id;
     // echo $unitNo;

if(addNewProduct($conn, $id, $projectName, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package,
                         $discount, $rebate, $extraRebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus, $remark, $bFormCollected, $paymentMethod,
                         $lawyer, $pendingApprovalStatus, $bankApproved, $loSignedDate, $laSignedDate, $spaSignedDate, $fullsetCompleted, $cashBuyer, $cancelledBooking, $caseStatus,
                         $eventPersonal, $rate, $agentComm, $upline1, $upline2, $plName, $hosName, $listerName, $ulOverride, $uulOverride,
                         $plOverride, $hosOverride, $listerOverride, $admin1Override, $admin2Override, $admin3Override, $gicProfit, $totalClaimedDevAmt, $totalBalUnclaimAmt, $loanUid))
     {
          //$_SESSION['messageType'] = 1;
          header('Location: ../admin1Product.php');
          //echo "register success";
     }

     if ($caseStatus == 'COMPLETED') {
       if(addUplineCommission($conn, $id, $purchaserName, $loanUid, $ulOverride,$upline1))
            {
                 //$_SESSION['messageType'] = 1;
                 // header('Location: ../admin1Product.php');
                 //echo "register success";
            }

      if (addUpuplineCommission($conn, $id, $purchaserName, $loanUid, $uulOverride,$upline2)) {
        header('Location: ../admin1Product.php');
      }
     }
}
else
{
    //  header('Location: ../index.php');
}
?>
