<<<<<<< HEAD
    <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="left-logo-div float-left hidden-logo-padding">
                <a href="index.php"><img src="img/logo.png" class="logo-img" alt="GIC" title="GIC"></a>
            </div>
            <?php

            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
                $_SERVER['REQUEST_URI'];

            if(isset($_SESSION['usertype_level']) && $_SESSION['usertype_level'] == 1)
            {
                ?>
                 <div class="middle-menu-div float-left hide">
                    <a href="index.php#about" class="menu-padding white-to-yellow">About</a>
                    <a href="index.php#products" class="menu-padding white-to-yellow">Products</a>
                    <a href="index.php#contact" class="menu-padding white-to-yellow">Contact Us</a>
                   
                </div>

                <?php
                if(isset($_SESSION['uid']))
                {
                    // echo count($_SESSION);
                    ?>
                    <div class="right-menu-div float-right" id="after-login-menu">
                        <a href="cart.php"  class="menu-padding hide"><img src="img/cart.png" class="cart-img" alt="cart" title="cart"><div class="dot-div"></div></a>
                        <a href="announcement.php"  class="menu-padding"><img src="img/notifcation.png" class="cart-img user-notification" alt="Notification" title="Notification"><div class="dot-div hide"></div></a>
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/white-user.png" class="cart-img" alt="profile" title="profile"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>
                            <!-- <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a"><img src="img/user-black.png" class="cart-img dropdown-img2" alt="Profile" title="Profile"> My Profile</a></p>
                                <p class="dropdown-p"><a href="editProfile.php"  class="menu-padding dropdown-a"><img src="img/edit-profile.png" class="cart-img dropdown-img2" alt="Edit Profile" title="Edit Profile"> Edit Profile</a></p>
                                <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a"><img src="img/password-d.png" class="cart-img dropdown-img2" alt="Password" title="Password"> Edit Password</a></p>
                                <p class="dropdown-p"><a href="addePin.php"  class="menu-padding dropdown-a"><img src="img/e-pin.png" class="cart-img dropdown-img2" alt="E-Pin" title="E-Pin"> E-Pin</a></p>
                                <p class="dropdown-p"><a href="referee.php" class="menu-padding dropdown-a"><img src="img/user-group.png" class="cart-img dropdown-img2" alt="My Referee" title="My Referee"> My Referee</a></p>
                                <p class="dropdown-p"><a href="addReferee.php" class="menu-padding dropdown-a"><img src="img/add-referee.png" class="cart-img dropdown-img2" alt="Add Referee" title="Add Referee"> Add Referee</a></p>
                            </div> -->

                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a"><img src="img/user-black.png" class="cart-img dropdown-img2" alt="Profile" title="Profile"> <?php echo _MAINJS_HEADER_MY_PROFILE ?></a></p>
                                <p class="dropdown-p"><a href="editProfile.php"  class="menu-padding dropdown-a"><img src="img/edit-profile.png" class="cart-img dropdown-img2" alt="Edit Profile" title="Edit Profile"> <?php echo _MAINJS_HEADER_EDIT_PROFILE ?></a></p>
                                <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a"><img src="img/password-d.png" class="cart-img dropdown-img2" alt="Password" title="Password"> <?php echo _MAINJS_HEADER_EDIT_PASSWORD ?></a></p>
                                <p class="dropdown-p"><a href="addePin.php"  class="menu-padding dropdown-a"><img src="img/e-pin.png" class="cart-img dropdown-img2" alt="E-Pin" title="E-Pin"> E-Pin</a></p>
                                <p class="dropdown-p"><a href="referee.php" class="menu-padding dropdown-a"><img src="img/user-group.png" class="cart-img dropdown-img2" alt="My Referee" title="My Referee"> <?php echo _MAINJS_HEADER_MY_REFEREE ?></a></p>
                                <p class="dropdown-p"><a href="addReferee.php" class="menu-padding dropdown-a"><img src="img/add-referee.png" class="cart-img dropdown-img2" alt="Add Referee" title="Add Referee"> <?php echo _MAINJS_HEADER_ADD_REFEREE ?></a></p>
                            </div>

                        </div>
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/cart.png" class="cart-img" alt="Purchase" title="Purchase"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                <!-- <p class="dropdown-p"><a href="firstPurchasing.php"  class="menu-padding dropdown-a"><img src="img/verify-payment.png" class="cart-img dropdown-img2" alt="Sign Up Product History" title="Sign Up Product History"> Sign Up Product History</a></p> -->

                                <p class="dropdown-p"><a href="firstPurchasing.php"  class="menu-padding dropdown-a"><img src="img/verify-payment.png" class="cart-img dropdown-img2" alt="Sign Up Product History" title="Sign Up Product History"> <?php echo _MAINJS_HEADER_SIGN_UP_PRO_HISTORY ?></a></p>

                                <p class="dropdown-p"><a href="product.php"  class="menu-padding dropdown-a"><img src="img/bottle.png" class="cart-img dropdown-img2" alt="Purchase Product" title="Purchase Product"> <?php echo _MAINJS_HEADER_PURCHASE_PRO ?></a></p>
                                <p class="dropdown-p"><a href="purchaseHistory.php"  class="menu-padding dropdown-a"><img src="img/verify-payment.png" class="cart-img dropdown-img2" alt="Edit Profile" title="Edit Profile"> <?php echo _MAINJS_HEADER_PURCHASE_HIS ?></a></p>
                            </div>
                        </div>
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/document.png" class="cart-img" alt="Report" title="Report"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>

                            <!-- <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><b>Bonus Report</b></p>
                                <p class="dropdown-p"><a href="bonusReport.php"  class="menu-padding dropdown-a"><img src="img/bonus.png" class="cart-img dropdown-img2" alt="Sponsor Bonus" title="Sponsor Bonus"> Sponsor Bonus</a></p>
                                <p class="dropdown-p"><a href="leadershipBonus.php"  class="menu-padding dropdown-a"><img src="img/leader.png" class="cart-img dropdown-img2" alt="Leadership Matching Bonus" title="Leadership Matching Bonus"> Leadership M. Bonus</a></p>
                            	<p class="dropdown-p"><a href="annualBonus.php"  class="menu-padding dropdown-a"><img src="img/annual-bonus.png" class="cart-img dropdown-img2" alt="Annual Bonus" title="Annual Bonus"> Annual Bonus</a></p>
								<p class="dropdown-p"><b> </b></p>
                                <p class="dropdown-p"><b>Cash Report</b></p>
                                <p class="dropdown-p"><a href="withdrawalReport.php"  class="menu-padding dropdown-a"><img src="img/withdraw1.png" class="cart-img dropdown-img2" alt="Withdrawal Report" title="Withdrawal Report"> Withdrawal Report</a></p>
                                <p class="dropdown-p"><a href="cashToPointReport.php"  class="menu-padding dropdown-a"><img src="img/points.png" class="cart-img dropdown-img2" alt="Convert Cash to Point" title="Convert Cash to Point"> Convert to Point</a></p>
								<p class="dropdown-p"><b> </b></p>
                                <p class="dropdown-p"><b>Point Report</b></p>
                                <p class="dropdown-p"><a href="transferPointReport.php"  class="menu-padding dropdown-a"><img src="img/transfer-report.png" class="cart-img dropdown-img2" alt="Transfer Point Report" title="Transfer Point Report"> Transfer Point Report</a></p>
                                <p class="dropdown-p"><a href="signUpReport.php"  class="menu-padding dropdown-a"><img src="img/register.png" class="cart-img dropdown-img2" alt="Sign Up Report" title="Sign Up Report"> Sign Up Report</a></p>
                            </div> -->

                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><b><?php echo _MAINJS_HEADER_BONUS_RP ?></b></p>
                                <p class="dropdown-p"><a href="bonusReport.php"  class="menu-padding dropdown-a"><img src="img/bonus.png" class="cart-img dropdown-img2" alt="Sponsor Bonus" title="Sponsor Bonus"> <?php echo _MAINJS_HEADER_SPONSOR_BONUS ?></a></p>
                                <p class="dropdown-p"><a href="otherBonus.php"  class="menu-padding dropdown-a"><img src="img/bonus.png" class="cart-img dropdown-img2" alt="Sponsor Bonus" title="Sponsor Bonus"> <?php echo _MAINJS_HEADER_ADMIN_OTHER_BONUS ?></a></p>
								<p class="dropdown-p"><b> </b></p>
                                <p class="dropdown-p"><b><?php echo _MAINJS_HEADER_CASH_RP ?></b></p>
                                <p class="dropdown-p"><a href="withdrawalReport.php"  class="menu-padding dropdown-a"><img src="img/withdraw1.png" class="cart-img dropdown-img2" alt="Withdrawal Report" title="Withdrawal Report"> <?php echo _MAINJS_HEADER_WITHDRAWAL_RP ?></a></p>
                                <p class="dropdown-p"><a href="cashToPointReport.php"  class="menu-padding dropdown-a"><img src="img/points.png" class="cart-img dropdown-img2" alt="Convert Cash to Point" title="Convert Cash to Point"> <?php echo _MAINJS_HEADER_CONVERT_TO_PTS ?></a></p>
								<p class="dropdown-p"><b> </b></p>
                                <p class="dropdown-p"><b><?php echo _MAINJS_HEADER_PTS_RP ?></b></p>
                                <p class="dropdown-p"><a href="transferPointReport.php"  class="menu-padding dropdown-a"><img src="img/transfer-report.png" class="cart-img dropdown-img2" alt="Transfer Point Report" title="Transfer Point Report"> <?php echo _MAINJS_HEADER_XFER_PTS_RP ?></a></p>
                            </div>

                        </div>
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/language-white.png" class="cart-img" alt="Change Language" title="Change Language"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a"><img src="img/english.png" class="cart-img dropdown-img2" alt="English" title="English"> English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a"><img src="img/chinese.png" class="cart-img dropdown-img2" alt="中文" title="中文"> 中文</a></p>
                            </div>
                        </div>
                        <a href="logout.php"  class="menu-padding"><img src="img/logout-white.png" class="cart-img" alt="Logout" title="Logout"></a>

					<div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
								  <li><a href="announcement.php">Announcement</a></li>
                                  <li class="title-li">Profile</li>
                                  <!-- <li><a href="profile.php" class="mini-li">My Profile</a></li>
                                  <li><a href="editProfile.php"  class="mini-li">Edit Profile</a></li>
                                  <li><a href="editPassword.php"  class="mini-li">Edit Password</a></li>
                                  <li><a href="addePin.php" class="mini-li">E-Pin</a></li> -->

                                  <li><a href="profile.php" class="mini-li"><?php echo _MAINJS_HEADER_MY_PROFILE ?></a></li>
                                  <li><a href="editProfile.php"  class="mini-li"><?php echo _MAINJS_HEADER_EDIT_PROFILE ?></a></li>
                                  <li><a href="editPassword.php"  class="mini-li"><?php echo _MAINJS_HEADER_EDIT_PASSWORD ?></a></li>
                                  <li><a href="addePin.php" class="mini-li">E-Pin</a></li>

                                  <li class="title-li">My Team</li>
                                  <!-- <li><a href="referee.php" class="mini-li">My Referee</a></li>
                                  <li><a href="addReferee.php" class="mini-li">Add Referee</a></li> -->

                                  <li><a href="referee.php" class="mini-li"><?php echo _MAINJS_HEADER_MY_REFEREE ?></a></li>
                                  <li><a href="addReferee.php" class="mini-li"><?php echo _MAINJS_HEADER_ADD_REFEREE ?></a></li>

                                  <li class="title-li">Purchase</li>
                                  <li><a href="firstPurchasing.php" class="mini-li"><?php echo _MAINJS_HEADER_SIGN_UP_PRO_HISTORY ?></a></li>
                                  <li><a href="product.php" class="mini-li"><?php echo _MAINJS_HEADER_PURCHASE_PRO ?></a></li>
                                  <li><a href="purchaseHistory.php" class="mini-li"><?php echo _MAINJS_HEADER_PURCHASE_HIS ?></a></li>

                                  <!-- <li class="title-li">Bonus Report</li>
                                  <li><a href="bonusReport.php" class="mini-li">Sponsor Bonus</a></li>
                                  <li><a href="leadershipBonus.php" class="mini-li">Leadership M. Bonus</a></li>
                                  <li><a href="annualBonus.php" class="mini-li">Annual Bonus</a></li>
                                  <li class="title-li">Cash Report</li>
                                  <li><a href="withdrawalReport.php" class="mini-li">Withdrawal Report</a></li>
                                  <li><a href="cashToPointReport.php" class="mini-li">Convert to Point</a></li>
                                  <li class="title-li">Point Report</li>
                                  <li><a href="transferPointReport.php" class="mini-li">Transfer Point Report</a></li>
                                  <li><a href="signUpReport.php" class="mini-li">Sign Up Report</a></li>  -->

                                  <li class="title-li"><?php echo _MAINJS_HEADER_BONUS_RP ?></li>
                                  <li><a href="bonusReport.php" class="mini-li"><?php echo _MAINJS_HEADER_SPONSOR_BONUS ?></a></li>
                                  <li><a href="otherBonus.php" class="mini-li"><?php echo _MAINJS_HEADER_ADMIN_OTHER_BONUS ?></a></li>
                                  <li><a href="withdrawalReport.php" class="mini-li"><?php echo _MAINJS_HEADER_WITHDRAWAL_RP ?></a></li>
                                  <li><a href="cashToPointReport.php" class="mini-li"><?php echo _MAINJS_HEADER_CONVERT_TO_PTS ?></a></li>
                                  <li class="title-li"><?php echo _MAINJS_HEADER_PTS_RP ?></li>
                                  <li><a href="transferPointReport.php" class="mini-li"><?php echo _MAINJS_HEADER_XFER_PTS_RP ?></a></li>


                                  <li class="title-li">Language</li>
                                  <li><a href="<?php $link ?>?lang=en" class="menu-padding white-to-yellow">English</a>
                                  <a href="<?php $link ?>?lang=ch" class="menu-padding white-to-yellow">中文</a></li>
                                  <li  class="last-li"><a href="logout.php"><?php echo _MAINJS_HEADER_LOGOUT ?></a></li>
						</ul>
					</div><!-- /dl-menuwrapper -->

                    </div>
                    <?php
                }
                else
                {
                    ?>
                    <div class="right-menu-div float-right">
                    	
                        <a  class="menu-padding white-to-yellow div-div open-login">Login</a>
                        <a class="open-menu"><img src="img/white-menu.png" class="menu-icon"></a>
                    </div>
                    <?php
                }
            }
            else
            {
                if(isset($_SESSION['uid']))
                {
                ?>
                <div class="middle-menu-div right-menu-div1">


                    <a href="adminProduct.php" class="menu-padding white-to-yellow">Loan Status</a>
                    <div class="dropdown hover1">
                            <a  class="menu-padding">
                                <img src="img/settings.png" class="cart-img hover1a" alt="Settings" title="Settings">
                                <img src="img/settings-yellow.png" class="cart-img hover1b" alt="Settings" title="Settings">
                            </a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>

                            <div class="dropdown-content ywllo-dropdown-content">
                                <p class="dropdown-p"><a href="logout.php" class="menu-padding dropdown-a">Logout</a></p>
                            </div>


                    </div>
                </div>
                    <div class="float-right right-menu-div admin-mobile">
                          	<div id="dl-menu" class="dl-menuwrapper admin-mobile-dl">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                    <li><a href="adminProduct.php">Loan Status</a></li>
                                    <li  class="last-li"><a href="logout.php">Logout</a></li>
                                </ul>
                            </div>

                     </div>
                <?php
                 }
                 else
                 {
                    ?>
                     <div class="right-menu-div float-right">
                        <!-- <a  class="menu-padding white-to-yellow login-a open-login">Login</a>
                        <a  class="menu-padding white-to-yellow div-div open-register">Sign Up</a>             -->
					
                        <a  class="menu-padding white-to-yellow div-div open-login">Login</a>

 					<div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
                             
                                  <li><a class="open-login">Login</a></li>
						</ul>
					</div><!-- /dl-menuwrapper -->
                    </div>
                    <?php
                 }
            }
            ?>
        </div>

    </header>
=======
    <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="left-logo-div float-left hidden-logo-padding">
                <a href="index.php"><img src="img/logo.png" class="logo-img" alt="GIC" title="GIC"></a>
            </div>
            <?php

            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
                $_SERVER['REQUEST_URI'];

            if(isset($_SESSION['usertype_level']) && $_SESSION['usertype_level'] == 1)
            {
                ?>
                 <div class="middle-menu-div float-left hide">
                    <a href="index.php#about" class="menu-padding white-to-yellow">About</a>
                    <a href="index.php#products" class="menu-padding white-to-yellow">Products</a>
                    <a href="index.php#contact" class="menu-padding white-to-yellow">Contact Us</a>
                   
                </div>

                <?php
                if(isset($_SESSION['uid']))
                {
                    // echo count($_SESSION);
                    ?>
                    <div class="right-menu-div float-right" id="after-login-menu">
                        <a href="cart.php"  class="menu-padding hide"><img src="img/cart.png" class="cart-img" alt="cart" title="cart"><div class="dot-div"></div></a>
                        <a href="announcement.php"  class="menu-padding"><img src="img/notifcation.png" class="cart-img user-notification" alt="Notification" title="Notification"><div class="dot-div hide"></div></a>
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/white-user.png" class="cart-img" alt="profile" title="profile"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>
                            <!-- <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a"><img src="img/user-black.png" class="cart-img dropdown-img2" alt="Profile" title="Profile"> My Profile</a></p>
                                <p class="dropdown-p"><a href="editProfile.php"  class="menu-padding dropdown-a"><img src="img/edit-profile.png" class="cart-img dropdown-img2" alt="Edit Profile" title="Edit Profile"> Edit Profile</a></p>
                                <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a"><img src="img/password-d.png" class="cart-img dropdown-img2" alt="Password" title="Password"> Edit Password</a></p>
                                <p class="dropdown-p"><a href="addePin.php"  class="menu-padding dropdown-a"><img src="img/e-pin.png" class="cart-img dropdown-img2" alt="E-Pin" title="E-Pin"> E-Pin</a></p>
                                <p class="dropdown-p"><a href="referee.php" class="menu-padding dropdown-a"><img src="img/user-group.png" class="cart-img dropdown-img2" alt="My Referee" title="My Referee"> My Referee</a></p>
                                <p class="dropdown-p"><a href="addReferee.php" class="menu-padding dropdown-a"><img src="img/add-referee.png" class="cart-img dropdown-img2" alt="Add Referee" title="Add Referee"> Add Referee</a></p>
                            </div> -->

                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a"><img src="img/user-black.png" class="cart-img dropdown-img2" alt="Profile" title="Profile"> <?php echo _MAINJS_HEADER_MY_PROFILE ?></a></p>
                                <p class="dropdown-p"><a href="editProfile.php"  class="menu-padding dropdown-a"><img src="img/edit-profile.png" class="cart-img dropdown-img2" alt="Edit Profile" title="Edit Profile"> <?php echo _MAINJS_HEADER_EDIT_PROFILE ?></a></p>
                                <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a"><img src="img/password-d.png" class="cart-img dropdown-img2" alt="Password" title="Password"> <?php echo _MAINJS_HEADER_EDIT_PASSWORD ?></a></p>
                                <p class="dropdown-p"><a href="addePin.php"  class="menu-padding dropdown-a"><img src="img/e-pin.png" class="cart-img dropdown-img2" alt="E-Pin" title="E-Pin"> E-Pin</a></p>
                                <p class="dropdown-p"><a href="referee.php" class="menu-padding dropdown-a"><img src="img/user-group.png" class="cart-img dropdown-img2" alt="My Referee" title="My Referee"> <?php echo _MAINJS_HEADER_MY_REFEREE ?></a></p>
                                <p class="dropdown-p"><a href="addReferee.php" class="menu-padding dropdown-a"><img src="img/add-referee.png" class="cart-img dropdown-img2" alt="Add Referee" title="Add Referee"> <?php echo _MAINJS_HEADER_ADD_REFEREE ?></a></p>
                            </div>

                        </div>
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/cart.png" class="cart-img" alt="Purchase" title="Purchase"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                <!-- <p class="dropdown-p"><a href="firstPurchasing.php"  class="menu-padding dropdown-a"><img src="img/verify-payment.png" class="cart-img dropdown-img2" alt="Sign Up Product History" title="Sign Up Product History"> Sign Up Product History</a></p> -->

                                <p class="dropdown-p"><a href="firstPurchasing.php"  class="menu-padding dropdown-a"><img src="img/verify-payment.png" class="cart-img dropdown-img2" alt="Sign Up Product History" title="Sign Up Product History"> <?php echo _MAINJS_HEADER_SIGN_UP_PRO_HISTORY ?></a></p>

                                <p class="dropdown-p"><a href="product.php"  class="menu-padding dropdown-a"><img src="img/bottle.png" class="cart-img dropdown-img2" alt="Purchase Product" title="Purchase Product"> <?php echo _MAINJS_HEADER_PURCHASE_PRO ?></a></p>
                                <p class="dropdown-p"><a href="purchaseHistory.php"  class="menu-padding dropdown-a"><img src="img/verify-payment.png" class="cart-img dropdown-img2" alt="Edit Profile" title="Edit Profile"> <?php echo _MAINJS_HEADER_PURCHASE_HIS ?></a></p>
                            </div>
                        </div>
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/document.png" class="cart-img" alt="Report" title="Report"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>

                            <!-- <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><b>Bonus Report</b></p>
                                <p class="dropdown-p"><a href="bonusReport.php"  class="menu-padding dropdown-a"><img src="img/bonus.png" class="cart-img dropdown-img2" alt="Sponsor Bonus" title="Sponsor Bonus"> Sponsor Bonus</a></p>
                                <p class="dropdown-p"><a href="leadershipBonus.php"  class="menu-padding dropdown-a"><img src="img/leader.png" class="cart-img dropdown-img2" alt="Leadership Matching Bonus" title="Leadership Matching Bonus"> Leadership M. Bonus</a></p>
                            	<p class="dropdown-p"><a href="annualBonus.php"  class="menu-padding dropdown-a"><img src="img/annual-bonus.png" class="cart-img dropdown-img2" alt="Annual Bonus" title="Annual Bonus"> Annual Bonus</a></p>
								<p class="dropdown-p"><b> </b></p>
                                <p class="dropdown-p"><b>Cash Report</b></p>
                                <p class="dropdown-p"><a href="withdrawalReport.php"  class="menu-padding dropdown-a"><img src="img/withdraw1.png" class="cart-img dropdown-img2" alt="Withdrawal Report" title="Withdrawal Report"> Withdrawal Report</a></p>
                                <p class="dropdown-p"><a href="cashToPointReport.php"  class="menu-padding dropdown-a"><img src="img/points.png" class="cart-img dropdown-img2" alt="Convert Cash to Point" title="Convert Cash to Point"> Convert to Point</a></p>
								<p class="dropdown-p"><b> </b></p>
                                <p class="dropdown-p"><b>Point Report</b></p>
                                <p class="dropdown-p"><a href="transferPointReport.php"  class="menu-padding dropdown-a"><img src="img/transfer-report.png" class="cart-img dropdown-img2" alt="Transfer Point Report" title="Transfer Point Report"> Transfer Point Report</a></p>
                                <p class="dropdown-p"><a href="signUpReport.php"  class="menu-padding dropdown-a"><img src="img/register.png" class="cart-img dropdown-img2" alt="Sign Up Report" title="Sign Up Report"> Sign Up Report</a></p>
                            </div> -->

                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><b><?php echo _MAINJS_HEADER_BONUS_RP ?></b></p>
                                <p class="dropdown-p"><a href="bonusReport.php"  class="menu-padding dropdown-a"><img src="img/bonus.png" class="cart-img dropdown-img2" alt="Sponsor Bonus" title="Sponsor Bonus"> <?php echo _MAINJS_HEADER_SPONSOR_BONUS ?></a></p>
                                <p class="dropdown-p"><a href="otherBonus.php"  class="menu-padding dropdown-a"><img src="img/bonus.png" class="cart-img dropdown-img2" alt="Sponsor Bonus" title="Sponsor Bonus"> <?php echo _MAINJS_HEADER_ADMIN_OTHER_BONUS ?></a></p>
								<p class="dropdown-p"><b> </b></p>
                                <p class="dropdown-p"><b><?php echo _MAINJS_HEADER_CASH_RP ?></b></p>
                                <p class="dropdown-p"><a href="withdrawalReport.php"  class="menu-padding dropdown-a"><img src="img/withdraw1.png" class="cart-img dropdown-img2" alt="Withdrawal Report" title="Withdrawal Report"> <?php echo _MAINJS_HEADER_WITHDRAWAL_RP ?></a></p>
                                <p class="dropdown-p"><a href="cashToPointReport.php"  class="menu-padding dropdown-a"><img src="img/points.png" class="cart-img dropdown-img2" alt="Convert Cash to Point" title="Convert Cash to Point"> <?php echo _MAINJS_HEADER_CONVERT_TO_PTS ?></a></p>
								<p class="dropdown-p"><b> </b></p>
                                <p class="dropdown-p"><b><?php echo _MAINJS_HEADER_PTS_RP ?></b></p>
                                <p class="dropdown-p"><a href="transferPointReport.php"  class="menu-padding dropdown-a"><img src="img/transfer-report.png" class="cart-img dropdown-img2" alt="Transfer Point Report" title="Transfer Point Report"> <?php echo _MAINJS_HEADER_XFER_PTS_RP ?></a></p>
                            </div>

                        </div>
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/language-white.png" class="cart-img" alt="Change Language" title="Change Language"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a"><img src="img/english.png" class="cart-img dropdown-img2" alt="English" title="English"> English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a"><img src="img/chinese.png" class="cart-img dropdown-img2" alt="中文" title="中文"> 中文</a></p>
                            </div>
                        </div>
                        <a href="logout.php"  class="menu-padding"><img src="img/logout-white.png" class="cart-img" alt="Logout" title="Logout"></a>

					<div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
								  <li><a href="announcement.php">Announcement</a></li>
                                  <li class="title-li">Profile</li>
                                  <!-- <li><a href="profile.php" class="mini-li">My Profile</a></li>
                                  <li><a href="editProfile.php"  class="mini-li">Edit Profile</a></li>
                                  <li><a href="editPassword.php"  class="mini-li">Edit Password</a></li>
                                  <li><a href="addePin.php" class="mini-li">E-Pin</a></li> -->

                                  <li><a href="profile.php" class="mini-li"><?php echo _MAINJS_HEADER_MY_PROFILE ?></a></li>
                                  <li><a href="editProfile.php"  class="mini-li"><?php echo _MAINJS_HEADER_EDIT_PROFILE ?></a></li>
                                  <li><a href="editPassword.php"  class="mini-li"><?php echo _MAINJS_HEADER_EDIT_PASSWORD ?></a></li>
                                  <li><a href="addePin.php" class="mini-li">E-Pin</a></li>

                                  <li class="title-li">My Team</li>
                                  <!-- <li><a href="referee.php" class="mini-li">My Referee</a></li>
                                  <li><a href="addReferee.php" class="mini-li">Add Referee</a></li> -->

                                  <li><a href="referee.php" class="mini-li"><?php echo _MAINJS_HEADER_MY_REFEREE ?></a></li>
                                  <li><a href="addReferee.php" class="mini-li"><?php echo _MAINJS_HEADER_ADD_REFEREE ?></a></li>

                                  <li class="title-li">Purchase</li>
                                  <li><a href="firstPurchasing.php" class="mini-li"><?php echo _MAINJS_HEADER_SIGN_UP_PRO_HISTORY ?></a></li>
                                  <li><a href="product.php" class="mini-li"><?php echo _MAINJS_HEADER_PURCHASE_PRO ?></a></li>
                                  <li><a href="purchaseHistory.php" class="mini-li"><?php echo _MAINJS_HEADER_PURCHASE_HIS ?></a></li>

                                  <!-- <li class="title-li">Bonus Report</li>
                                  <li><a href="bonusReport.php" class="mini-li">Sponsor Bonus</a></li>
                                  <li><a href="leadershipBonus.php" class="mini-li">Leadership M. Bonus</a></li>
                                  <li><a href="annualBonus.php" class="mini-li">Annual Bonus</a></li>
                                  <li class="title-li">Cash Report</li>
                                  <li><a href="withdrawalReport.php" class="mini-li">Withdrawal Report</a></li>
                                  <li><a href="cashToPointReport.php" class="mini-li">Convert to Point</a></li>
                                  <li class="title-li">Point Report</li>
                                  <li><a href="transferPointReport.php" class="mini-li">Transfer Point Report</a></li>
                                  <li><a href="signUpReport.php" class="mini-li">Sign Up Report</a></li>  -->

                                  <li class="title-li"><?php echo _MAINJS_HEADER_BONUS_RP ?></li>
                                  <li><a href="bonusReport.php" class="mini-li"><?php echo _MAINJS_HEADER_SPONSOR_BONUS ?></a></li>
                                  <li><a href="otherBonus.php" class="mini-li"><?php echo _MAINJS_HEADER_ADMIN_OTHER_BONUS ?></a></li>
                                  <li><a href="withdrawalReport.php" class="mini-li"><?php echo _MAINJS_HEADER_WITHDRAWAL_RP ?></a></li>
                                  <li><a href="cashToPointReport.php" class="mini-li"><?php echo _MAINJS_HEADER_CONVERT_TO_PTS ?></a></li>
                                  <li class="title-li"><?php echo _MAINJS_HEADER_PTS_RP ?></li>
                                  <li><a href="transferPointReport.php" class="mini-li"><?php echo _MAINJS_HEADER_XFER_PTS_RP ?></a></li>


                                  <li class="title-li">Language</li>
                                  <li><a href="<?php $link ?>?lang=en" class="menu-padding white-to-yellow">English</a>
                                  <a href="<?php $link ?>?lang=ch" class="menu-padding white-to-yellow">中文</a></li>
                                  <li  class="last-li"><a href="logout.php"><?php echo _MAINJS_HEADER_LOGOUT ?></a></li>
						</ul>
					</div><!-- /dl-menuwrapper -->

                    </div>
                    <?php
                }
                else
                {
                    ?>
                    <div class="right-menu-div float-right">
                    	
                        <a  class="menu-padding white-to-yellow div-div open-login">Login</a>
                        <a class="open-menu"><img src="img/white-menu.png" class="menu-icon"></a>
                    </div>
                    <?php
                }
            }
            else
            {
                if(isset($_SESSION['uid']))
                {
                ?>
                <div class="middle-menu-div right-menu-div1">


                    <a href="adminProduct.php" class="menu-padding white-to-yellow">Loan Status</a>
                    <div class="dropdown hover1">
                            <a  class="menu-padding">
                                <img src="img/settings.png" class="cart-img hover1a" alt="Settings" title="Settings">
                                <img src="img/settings-yellow.png" class="cart-img hover1b" alt="Settings" title="Settings">
                            </a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a>

                            <div class="dropdown-content ywllo-dropdown-content">
                                <p class="dropdown-p"><a href="logout.php" class="menu-padding dropdown-a">Logout</a></p>
                            </div>


                    </div>
                </div>
                    <div class="float-right right-menu-div admin-mobile">
                          	<div id="dl-menu" class="dl-menuwrapper admin-mobile-dl">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                    <li><a href="adminProduct.php">Loan Status</a></li>
                                    <li  class="last-li"><a href="logout.php">Logout</a></li>
                                </ul>
                            </div>

                     </div>
                <?php
                 }
                 else
                 {
                    ?>
                     <div class="right-menu-div float-right">
                        <!-- <a  class="menu-padding white-to-yellow login-a open-login">Login</a>
                        <a  class="menu-padding white-to-yellow div-div open-register">Sign Up</a>             -->
					
                        <a  class="menu-padding white-to-yellow div-div open-login">Login</a>

 					<div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
                             
                                  <li><a class="open-login">Login</a></li>
						</ul>
					</div><!-- /dl-menuwrapper -->
                    </div>
                    <?php
                 }
            }
            ?>
        </div>

    </header>
>>>>>>> master
